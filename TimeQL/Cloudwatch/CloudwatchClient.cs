﻿using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;
using Amazon.Runtime;
using Lispery;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Util;
using TimeQL.Values;

namespace TimeQL.Cloudwatch
{
	internal class CloudwatchClient
	{
		private readonly Config config;
		private readonly QueryCache cache;

		public CloudwatchClient(Config config, QueryCache cache)
		{
			this.config = config;
			this.cache = cache;
		}

		public TimeSeriesList Fetch(
			KeywordTable keywordTable,
			CommonQueryOptions options,
			string @namespace,
			string metricName,
			string statistic,
			string? region)
		{
			if (!config.CloudwatchEndpoints.Any())
			{
				throw new InvalidOperationException("No cloudwatch endpoints configured.");
			}

			CloudwatchConfig endpointConfig = config.CloudwatchEndpoints.First();

			if (options.Endpoint != null)
			{
				endpointConfig = config.CloudwatchEndpoints.Where(d => d.Name == options.Endpoint).First();
			}

			if (region == null)
			{
				region = endpointConfig.DefaultRegion;
			}

			var regionEndpoint = RegionEndpoint.GetBySystemName(region);

			// Create a key for trying to look up the result in the cache.
			string cacheKey = CreateKey(options, @namespace, metricName, statistic, region);

			if (options.Cache && cache.TryLookup(cacheKey, keywordTable, out TimeSeriesList? list))
			{
				return list;
			}

			var credentials = new BasicAWSCredentials(
				endpointConfig.AccessKey,
				endpointConfig.SecretAccessKey);
			
			AmazonCloudWatchClient client = new(credentials, regionEndpoint);

			// First find all metrics that match the request.
			ListMetricsRequest listRequest = new();
			listRequest.Namespace = @namespace;
			listRequest.MetricName = metricName;

			ListMetricsResponse listResponse = client.ListMetricsAsync(listRequest).Result;
			GetMetricDataRequest getRequest = new();

			getRequest.StartTimeUtc = options.WindowStart;
			getRequest.EndTimeUtc = options.WindowStop;

			// Create requests for the actual data.
			Dictionary<string, Amazon.CloudWatch.Model.Metric> metrics = new();
			int id = 0;

			foreach (var metric in listResponse.Metrics)
			{
				MetricDataQuery query = new();
				query.Id = "q" + id.ToString();

				StringBuilder sb = new();
				sb.Append(metric.MetricName);

				foreach (var dimension in metric.Dimensions)
				{
					sb.Append(',');
					sb.Append(dimension.Name);
					sb.Append('=');
					sb.Append(dimension.Value);
				}

				query.Label = sb.ToString();
				metrics[query.Id] = metric;

				MetricStat stat = new();
				stat.Metric = metric;
				stat.Period = ((int)options.Resolution.TotalSeconds + 59) / 60 * 60;
				query.MetricStat = stat;
				stat.Stat = statistic;
				
				getRequest.MetricDataQueries.Add(query);
				id++;
			}

			// Retrieve the actual data.
			var getResponse = client.GetMetricDataAsync(getRequest).Result;
			var listBuilder = ImmutableList.CreateBuilder<TimeSeries>();
			var kwMetric = keywordTable.Get("metric");

			foreach (var result in getResponse.MetricDataResults)
			{
				var metric = metrics[result.Id];
				var metadataBuilder = ImmutableDictionary.CreateBuilder<Keyword, object?>();
				var datapointBuilder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
				metadataBuilder[kwMetric] = metric.MetricName;

				foreach (var dimension in metric.Dimensions)
				{
					metadataBuilder[keywordTable.Get(dimension.Name)] = dimension.Value;
				}

				for (int i = 0; i < result.Timestamps.Count; i++)
				{
					DateTime timestamp = result.Timestamps[i];
					double value = result.Values[i];
					datapointBuilder[timestamp.ToUniversalTime()] = value;
				}

				listBuilder.Add(new TimeSeries(
					result.Label,
					datapointBuilder.ToImmutable(),
					metadataBuilder.ToImmutable()));
			}

			var resultList = new TimeSeriesList(listBuilder.ToImmutable());
			cache.Insert(cacheKey, resultList, options.Resolution);
			return resultList;
		}

		/// <summary>
		/// Creates a key string for the cache.
		/// </summary>
		private static string CreateKey(
			CommonQueryOptions options,
			string @namespace,
			string metricName,
			string statistic,
			string region)
		{
			StringBuilder sb = new();
			sb.Append("cloudwatch|");
			sb.Append(options.Endpoint);
			sb.Append('|');
			sb.Append(options.WindowStart.Ticks);
			sb.Append('|');
			sb.Append(options.WindowStop.Ticks);
			sb.Append('|');
			sb.Append(options.Resolution.Ticks);
			sb.Append('|');
			sb.Append(@namespace);
			sb.Append('|');
			sb.Append(metricName);
			sb.Append('|');
			sb.Append(statistic);
			sb.Append('|');
			sb.Append(region);
			return sb.ToString();
		}
	}
}
