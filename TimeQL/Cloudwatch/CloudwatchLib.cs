﻿using Lispery;
using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Cloudwatch;
using TimeQL.Util;

namespace TimeQL.Cloudwatch
{
	public class CloudwatchLib
	{
		private readonly Keyword kwStatistic;
		private readonly Keyword kwRegion;

		private readonly KnownIdentifiers ki;
		private readonly Lispery.Environment env;
		private readonly CloudwatchClient client;

		public CloudwatchLib(Config config, QueryCache queryCache, KnownIdentifiers ki, Lispery.Environment env)
		{
			this.ki = ki;
			this.env = env;
			this.client = new CloudwatchClient(config, queryCache);

			kwStatistic = env.GetKeyword("statistic");
			kwRegion = env.GetKeyword("region");

			env.DefineFunction<string, string, object>("cloudwatch-query", CloudwatchQuery!);

			env.DefineFunction("cloudwatch-endpoints", () => new Vector(
				config.CloudwatchEndpoints.Select(e => e.Name)));
		}

		private object? CloudwatchQuery(string @namespace, string metric, object[] args)
		{
			var options = new Options(args);
			var commonOptions = new CommonQueryOptions(options, ki, env);
			string statistic = options.Get<string>(kwStatistic, "Average");
			string? region = options.GetNullable<string>(kwRegion, null);

			return commonOptions.Postprocess(client.Fetch(
				env.KeywordTable,
				commonOptions,
				@namespace,
				metric,
				statistic,
				region));
		}
	}
}
