﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TimeQL.Util;

namespace TimeQL.Cloudwatch
{
	public class CloudwatchConfig
	{
		private readonly JsonElement root;

		public CloudwatchConfig(JsonElement root)
		{
			this.root = root;
		}

		/// <summary>
		/// The name of the Cloudwatch database.
		/// </summary>
		public string Name => root.GetString("name");

		/// <summary>
		/// The default region name.
		/// </summary>
		public string DefaultRegion => root.GetString("default-region");

		/// <summary>
		/// The AWS access key.
		/// </summary>
		public string AccessKey => root.GetString("access-key");

		/// <summary>
		/// The secret AWS access key.
		/// </summary>
		public string SecretAccessKey => root.GetString("secret-access-key");
	}
}
