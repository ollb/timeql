﻿using Lispery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Util;
using TimeQL.Values;

namespace TimeQL
{
	/// <summary>
	/// An entry in the query cache.
	/// </summary>
	internal class QueryCacheEntry
	{
		private byte[]? data;
		private CompressionAlgorithm algo;

		/// <summary>
		/// Returns the expiration time of the entry.
		/// </summary>
		public TimeSpan ExpireTime { get; init; }

		/// <summary>
		/// Returns the uncompressed size of the entry.
		/// </summary>
		public long UncompressedSize { get; private set; }

		/// <summary>
		/// Returns the (compressed) size of the entry.
		/// </summary>
		public long Size
		{
			get { return data != null ? data.Length : 0; }
		}

		/// <summary>
		/// Returns the compression ratio.
		/// </summary>
		public double CompressionRatio
		{
			get { return (double)Size / UncompressedSize; }
		}

		/// <summary>
		/// Creates a new empty cache entry with the given expiration time.
		/// </summary>
		public QueryCacheEntry(TimeSpan expireTime)
		{
			ExpireTime = expireTime;
		}

		/// <summary>
		/// Saves a time series list in the entry.
		/// </summary>
		public void Save(TimeSeriesList list, Config config)
		{
			algo = Enum.Parse<CompressionAlgorithm>(config.CacheCompression, true);

			// Write the uncompressed data.
			using MemoryStream stream = new();
			using CompressionStream compressionStream = new(stream, algo, config.CacheCompressionLevel);
			using BinaryWriter writer = new(compressionStream, Encoding.UTF8, true);
			list.Serialize(writer);
			writer.Close();

			UncompressedSize = compressionStream.WrittenBytes;
			data = stream.ToArray();
		}

		/// <summary>
		/// Loads a time series list from the entry.
		/// </summary>
		public TimeSeriesList Load(KeywordTable keywordTable)
		{
			if (data == null)
			{
				throw new InvalidOperationException("Cache entry is empty.");
			}

			using MemoryStream stream = new(data);
			using DecompressionStream decompressionStream = new(stream, algo);
			using BinaryReader reader = new(decompressionStream);
			return TimeSeriesList.Deserialize(keywordTable, reader);
		}
	}
}
