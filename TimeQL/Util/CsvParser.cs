﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Util
{
	/// <summary>
	/// Fast CSV parser implementation.
	/// </summary>
	internal class CsvParser
	{
		private readonly TextReader reader;
		private bool autoTrim = true;

		private readonly List<string> record = new();
		private readonly StringBuilder builder = new();

		/// <summary>
		/// Creates a new CSV parser.
		/// </summary>
		public CsvParser(TextReader reader)
		{
			this.reader = reader;
		}

		/// <summary>
		/// Controls if fields are automatically trimmed.
		/// </summary>
		public bool AutoTrim
		{
			get { return autoTrim; }
			set { autoTrim = value; }
		}

		/// <summary>
		/// Returns the current record.
		/// </summary>
		public ReadOnlyCollection<string> Record
		{
			get { return new ReadOnlyCollection<string>(record); }
		}

		/// <summary>
		/// Reads the next record.
		/// </summary>
		/// <returns><c>true</c> if a record could be read. <c>false</c> on EOF.</returns>
		public bool Read()
		{
			bool result = ReadRecord();
			builder.Clear();
			return result;
		}

		/// <summary>
		/// Reads the next record.
		/// </summary>
		private bool ReadRecord()
		{
			record.Clear();

			while (true)
			{
				record.Add(ReadField());

				switch (reader.Peek())
				{
					case -1:
						return !IsEmpty(record);

					case ',':
						reader.Read();
						break;

					case '\n':
						reader.Read();

						if (!IsEmpty(record))
						{
							return true;
						}

						record.Clear();
						break;

					case '\r':
						reader.Read();
						ReadIf('\n');

						if (!IsEmpty(record))
						{
							return true;
						}

						record.Clear();
						break;
				}
			}
		}

		/// <summary>
		/// Reads the next field in a record.
		/// </summary>
		private string ReadField()
		{
			if (AutoTrim)
			{
				SkipWhitespaceNoNL();
			}

			if (reader.Peek() == '"')
			{
				return ReadQuotedField();
			}

			builder.Clear();

			while (true)
			{
				switch (reader.Peek())
				{
					case ',': goto case -1;
					case '\r': goto case -1;
					case '\n': goto case -1;

					case -1:
						return MaybeTrim(builder.ToString());

					default:
						builder.Append((char)reader.Read());
						break;
				}
			}
		}

		/// <summary>
		/// Reads a field enclosed in quotes.
		/// </summary>
		private string ReadQuotedField()
		{
			int chr = reader.Read();
			Debug.Assert(chr == '"');

			builder.Clear();

			while (true)
			{
				switch (reader.Peek())
				{
					case '"':
						reader.Read();

						if (reader.Peek() == '"')
						{
							builder.Append((char)reader.Read());
						}
						else
						{
							SkipWhitespaceNoNL();

							switch (reader.Peek())
							{
								case ',': goto case -1;
								case '\r': goto case -1;
								case '\n': goto case -1;

								case -1:
									return MaybeTrim(builder.ToString());

								default:
									throw new FormatException("No field separator after quoted field");
							}
						}
						break;

					case -1:
						throw new FormatException("Unterminated quoted field");

					default:
						builder.Append((char)reader.Read());
						break;
				}
			}
		}

		/// <summary>
		/// Checks if the given record is considered empty.
		/// </summary>
		private static bool IsEmpty(List<string> fields)
		{
			return fields.Count == 0 || (fields.Count == 1 && string.IsNullOrWhiteSpace(fields[0]));
		}

		/// <summary>
		/// Consumes the next character if it matches the given one.
		/// </summary>
		private void ReadIf(char chr)
		{
			if (reader.Peek() == (int)chr)
			{
				reader.Read();
			}
		}

		/// <summary>
		/// Trim the given string, if auto-trim is enabled.
		/// </summary>
		private string MaybeTrim(string str)
		{
			return autoTrim ? str.Trim() : str;
		}

		/// <summary>
		/// Skip any whitespace, excluding newlines.
		/// </summary>
		private void SkipWhitespaceNoNL()
		{
			while (
				reader.Peek() != -1 &&
				!"\r\n".Contains((char)reader.Peek()) &&
				char.IsWhiteSpace((char)reader.Peek()))
			{
				reader.Read();
			}
		}
	}
}
