﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Util
{
	internal static class DateTimeExt
	{
		/// <summary>
		/// Returns the RFC 3339 string representation of the datetime.
		/// </summary>
		public static string ToRfc3339(this DateTime dt)
		{
			return dt.ToString("yyyy-MM-dd'T'HH:mm:ss.fffK", DateTimeFormatInfo.InvariantInfo);
		}

		/// <summary>
		/// Aligns the datetime down to the last multiple of <c>span</c>.
		/// </summary>
		public static DateTime AlignDown(this DateTime dt, TimeSpan span)
		{
			return new DateTime(dt.Ticks / span.Ticks * span.Ticks, dt.Kind);
		}

		/// <summary>
		/// Aligns the datetime up to the next multiple of <c>span</c>.
		/// </summary>
		public static DateTime AlignUp(this DateTime dt, TimeSpan span)
		{
			return new DateTime((dt.Ticks + span.Ticks - 1) / span.Ticks * span.Ticks, dt.Kind);
		}

		/// <summary>
		/// Aligns the datetime to the closest multiple of <c>span</c>.
		/// </summary>
		public static DateTime Align(this DateTime dt, TimeSpan span)
		{
			return new DateTime((dt.Ticks + span.Ticks / 2) / span.Ticks * span.Ticks, dt.Kind);
		}
	}
}
