﻿using Lispery.Interfaces;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Interfaces;

namespace TimeQL.Util
{
	internal static class Metadata
	{
		/// <summary>
		/// Joins metadata from multiple annotated values.
		/// </summary>
		public static ImmutableDictionary<Keyword, object?> Join(params IAnnotatedValue[] values)
		{
			return Join((IEnumerable<IAnnotatedValue>)values);
		}

		/// <summary>
		/// Joins metadata from multiple annotated values.
		/// </summary>
		public static ImmutableDictionary<Keyword, object?> Join(IEnumerable<IAnnotatedValue> values)
		{
			return Join(values.Select(v => v.Metadata));
		}

		/// <summary>
		/// Joins metadata from multiple series.
		/// </summary>
		public static ImmutableDictionary<Keyword, object?> Join(
			IEnumerable<ImmutableDictionary<Keyword, object?>> list)
		{
			var builder = ImmutableDictionary.CreateBuilder<Keyword, object?>();
			var comparer = EqualityComparer<object?>.Default;

			if (list.Any())
			{
				ImmutableDictionary<Keyword, object?> first = list.First();

				foreach (var key in first.Keys)
				{
					// Has to exist, because we get the keys from the first series.
					object? value = first[key];
					bool equal = true;

					foreach (var meta in list.Skip(1))
					{
						if (!meta.TryGetValue(key, out var nextValue) || !comparer.Equals(nextValue, value))
						{
							equal = false;
							break;
						}
					}

					// Only add the value if all series agree on it.
					if (equal)
					{
						builder[key] = value;
					}
				}
			}

			return builder.ToImmutable();
		}

		/// <summary>
		/// Selects multiple metadata and/or strings.
		/// </summary>
		public static string Select(ImmutableDictionary<Keyword, object?> metadata, ISequence seq)
		{
			StringBuilder sb = new();

			foreach (var value in seq.GetEntries())
			{
				switch (value)
				{
					case Keyword keyword:
						sb.Append(metadata[keyword]?.ToString() ?? "nil");
						break;

					case string str:
						sb.Append(str);
						break;

					default:
						throw new ArgumentException("Invalid select expression");
				}
			}

			return sb.ToString();
		}
	}
}
