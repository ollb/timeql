﻿using Lispery;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Util
{
	/// <summary>
	/// Just a collection of frequently used identifiers for easier use.
	/// </summary>
	public class KnownIdentifiers
	{
		public Keyword KwWindowStart { get; init; }
		public Keyword KwWindowStop { get; init; }
		public Keyword KwResolution { get; init; }
		public Keyword KwMaxResolution { get; init; }
		public Keyword KwEndpoint { get; init; }
		public Keyword KwName { get; init; }
		public Keyword KwPrefetch { get; init; }
		public Keyword KwShift { get; init; }
		public Keyword KwCache { get; init; }

		public Symbol SymEmWindowStart { get; init; }
		public Symbol SymEmWindowStop { get; init; }
		public Symbol SymEmResolution { get; init; }
		public Symbol SymEmMaxResolution { get; init; }

		public KnownIdentifiers(Lispery.Environment env)
		{
			KwWindowStart = env.GetKeyword("window-start");
			KwWindowStop = env.GetKeyword("window-stop");
			KwResolution = env.GetKeyword("resolution");
			KwMaxResolution = env.GetKeyword("max-resolution");
			KwEndpoint = env.GetKeyword("endpoint");
			KwName = env.GetKeyword("name");
			KwPrefetch = env.GetKeyword("prefetch");
			KwShift = env.GetKeyword("shift");
			KwCache = env.GetKeyword("cache");

			// Earmuffs :-)
			SymEmWindowStart = env.GetSymbol("*window-start*");
			SymEmWindowStop = env.GetSymbol("*window-stop*");
			SymEmResolution = env.GetSymbol("*resolution*");
			SymEmMaxResolution = env.GetSymbol("*max-resolution*");
		}
	}
}
