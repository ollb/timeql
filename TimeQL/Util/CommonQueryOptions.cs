﻿using Lispery;
using Lispery.Interfaces;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Values;

namespace TimeQL.Util
{
	public class CommonQueryOptions
	{
		// TODO: consolidation
		public DateTime WindowStart { get; init; }
		public DateTime WindowStop { get; init; }
		public TimeSpan Resolution { get; init; }
		public TimeSpan Shift { get; init; }
		public TimeSpan Prefetch { get; init; }
		public string? Endpoint { get; init; }
		public object? Name { get; init; }
		public bool Cache { get; init; }

		public int SampleCount
		{
			get { return (int)((WindowStop - WindowStart) / Resolution); }
		}

		public CommonQueryOptions(Options options, KnownIdentifiers ki, Lispery.Environment env)
		{
			WindowStart = options.Get<DateTime>(ki.KwWindowStart, env.Resolve(ki.SymEmWindowStart)!);
			WindowStop = options.Get<DateTime>(ki.KwWindowStop, env.Resolve(ki.SymEmWindowStop)!);
			Shift = options.Get<TimeSpan>(ki.KwShift, TimeSpan.Zero);
			Prefetch = options.Get<TimeSpan>(ki.KwPrefetch, TimeSpan.Zero);
			Endpoint = options.GetNullable<string>(ki.KwEndpoint, null);
			Name = options.GetNullable<object>(ki.KwName, null);
			Cache = options.Get<bool>(ki.KwCache, true);

			Resolution = options.Get<TimeSpan>(ki.KwResolution, env.Resolve(ki.SymEmResolution)!);

			TimeSpan maxResolution = options.Get<TimeSpan>(
				ki.KwMaxResolution,
				env.Resolve(ki.SymEmMaxResolution) ?? TimeSpan.Zero);

			if (maxResolution > Resolution)
			{
				Resolution = maxResolution;
			}

			WindowStart = (WindowStart - Shift - Prefetch).AlignDown(Resolution);
			WindowStop = (WindowStop - Shift).AlignUp(Resolution);
		}

		public TimeSeriesList ApplyName(TimeSeriesList list)
		{
			return Name switch
			{
				null          => list,
				string str    => list.SetName(str),
				Keyword key   => list.SetName(key),
				ISequence seq => list.SetName(seq),
				_             => throw new ArgumentException("Invalid name selector."),
			};
		}

		public TimeSeriesList Align(TimeSeriesList list)
		{
			return list.Align(Resolution);
		}

		public TimeSeriesList Postprocess(TimeSeriesList list)
		{
			return ApplyName(Align(list));
		}
	}
}
