﻿using Lispery;
using Lispery.Interfaces;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Interfaces;

namespace TimeQL.Util
{
	/// <summary>
	/// Wraps a normal aggregator function.
	/// </summary>
	internal class FunSlider : ISlider
	{
		private readonly Func<IEnumerable<double>, double> fun;

		public FunSlider(Func<IEnumerable<double>, double> fun)
		{
			this.fun = fun;
		}

		public void Add(double value)
		{
		}

		public void Remove(double value)
		{
		}

		public double GetValue(IEnumerable<double> values)
		{
			return fun(values);
		}
	}

	internal class GenericSlider : ISlider
	{
		private object? state;
		private readonly IFunction addFun;
		private readonly IFunction remFun;
		private readonly IFunction getFun;

		public GenericSlider(object? state, IFunction addFun, IFunction remFun, IFunction getFun)
		{
			this.state = state;
			this.addFun = addFun;
			this.remFun = remFun;
			this.getFun = getFun;
		}

		public void Add(double value)
		{
			state = addFun.Call(value, state);
		}

		public void Remove(double value)
		{
			state = remFun.Call(value, state);
		}

		public double GetValue(IEnumerable<double> values)
		{
			return Conversion.To<double>(getFun.Call(state));
		}
	}

	internal class SumSlider : ISlider
	{
		double sum;

		public void Add(double value)
		{
			sum += value;
		}

		public void Remove(double value)
		{
			sum -= value;
		}

		public double GetValue(IEnumerable<double> values)
		{
			return sum;
		}
	}

	internal class AverageSlider : ISlider
	{
		double sum;
		int count;

		public void Add(double value)
		{
			sum += value;
			count++;
		}

		public void Remove(double value)
		{
			sum -= value;
			count--;
		}

		public double GetValue(IEnumerable<double> values)
		{
			return sum / count;
		}
	}

	internal class ProductSlider : ISlider
	{
		double prod;

		public void Add(double value)
		{
			prod *= value;
		}

		public void Remove(double value)
		{
			prod /= value;
		}

		public double GetValue(IEnumerable<double> values)
		{
			return prod;
		}
	}

	internal class MedianSlider : ISlider
	{
		private readonly List<double> list = new();

		public void Add(double value)
		{
			int index = list.BinarySearch(value);
			if (index < 0) index = ~index;
			list.Insert(index, value);
		}

		public void Remove(double value)
		{
			list.Remove(value);
		}

		public double GetValue(IEnumerable<double> values)
		{
			if (list.Count % 2 == 0)
			{
				int m = list.Count / 2;
				return (list[m - 1] + list[m]) / 2.0;
			}
			else
			{
				int m = (list.Count - 1) / 2;
				return list[m];
			}
		}
	}

	public static class Slider
	{
		/// <summary>
		/// Creates an optimized aggregator for the given function.
		/// </summary>
		public static ISlider Create(IFunction fun)
		{
			return fun.FunctionType switch
			{
				FunctionType.Sum     => new SumSlider(),
				FunctionType.Average => new AverageSlider(),
				FunctionType.Product => new ProductSlider(),
				FunctionType.Median  => new MedianSlider(),
				_                    => new FunSlider(Optimizer.OptimizeAggregate(fun)),
			};
		}
	}
}
