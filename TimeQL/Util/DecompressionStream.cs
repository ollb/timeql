﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Util
{
	public class DecompressionStream : Stream
	{
		private readonly Stream stream;
		private readonly bool close;

		public DecompressionStream(Stream baseStream, CompressionAlgorithm algo)
		{
			close = algo != CompressionAlgorithm.None;

			stream = algo switch
			{
				CompressionAlgorithm.None => baseStream,
				CompressionAlgorithm.Gzip => new GZipStream(baseStream, CompressionMode.Decompress, true),
				CompressionAlgorithm.Zstd => new ZstdSharp.DecompressionStream(baseStream),
				_ => throw new ArgumentException("Unsupported algorithm.")
			};
		}

		public override bool CanRead => true;
		public override bool CanSeek => false;
		public override bool CanWrite => false;
		public override long Length => stream.Length;

		public override long Position
		{
			get => stream.Position;
			set => throw new NotSupportedException();
		}

		public override void Flush()
		{
			stream.Flush();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			return stream.Read(buffer, offset, count);
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotImplementedException();
		}

		public override void SetLength(long value)
		{
			throw new NotImplementedException();
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotImplementedException();
		}

		public override void Close()
		{
			if (close)
			{
				stream.Close();
			}
		}
	}
}
