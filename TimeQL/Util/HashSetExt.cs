﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Util
{
	internal static class HashSetExt
	{
		public static void AddRange<T>(this HashSet<T> set, IEnumerable<T> range)
		{
			foreach (var item in range)
			{
				set.Add(item);
			}
		}
	}
}
