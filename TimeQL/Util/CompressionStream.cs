﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Util
{
	public enum CompressionAlgorithm
	{
		None,
		Gzip,
		Zstd
	}

	public class CompressionStream : Stream
	{
		private readonly Stream stream;
		private readonly bool close;

		public long WrittenBytes { get; private set; }

		public CompressionStream(Stream baseStream, CompressionAlgorithm algo, int level = 1)
		{
			close = algo != CompressionAlgorithm.None;

			stream = algo switch
			{
				CompressionAlgorithm.None => baseStream,
				CompressionAlgorithm.Gzip => new GZipStream(baseStream, GetGzipCompressionLevel(level), true),
				CompressionAlgorithm.Zstd => new ZstdSharp.CompressionStream(baseStream, level),
				_                         => throw new ArgumentException("Unsupported algorithm.")
			};
		}

		private static CompressionLevel GetGzipCompressionLevel(int level)
		{
			return level switch
			{
				0 => CompressionLevel.NoCompression,
				1 => CompressionLevel.Fastest,
				2 => CompressionLevel.Optimal,
				3 => CompressionLevel.SmallestSize,
				_ => throw new ArgumentException("Compression level not supported.")
			};
		}

		public override bool CanRead => false;
		public override bool CanSeek => false;
		public override bool CanWrite => true;
		public override long Length => stream.Length;

		public override long Position
		{
			get => stream.Position;
			set => throw new NotSupportedException();
		}

		public override void Flush()
		{
			stream.Flush();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		public override void SetLength(long value)
		{
			stream.SetLength(value);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			stream.Write(buffer, offset, count);
			WrittenBytes += count;
		}

		public override void Close()
		{
			if (close)
			{
				stream.Close();
			}
		}
	}
}
