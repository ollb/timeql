﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace TimeQL.Util
{
	public static class JsonElementExt
	{
		public static bool HasProperty(this JsonElement element, string propertyName)
		{
			return element.TryGetProperty(propertyName, out var _);
		}

		public static string GetString(this JsonElement element, string propertyName)
		{
			string? value = element.GetProperty(propertyName).GetString();
			ArgumentNullException.ThrowIfNull(value);
			return value;
		}

		public static string GetString(this JsonElement element, string propertyName, string fallback)
		{
			if (!element.TryGetProperty(propertyName, out JsonElement property))
			{
				return fallback;
			}

			return property.GetString() ?? fallback;
		}

		public static int GetInt32(this JsonElement element, string propertyName)
		{
			return element.GetProperty(propertyName).GetInt32();
		}

		public static int GetInt32(this JsonElement element, string propertyName, int fallback)
		{
			if (!element.TryGetProperty(propertyName, out JsonElement property))
			{
				return fallback;
			}

			return property.GetInt32();
		}

		public static long GetInt64(this JsonElement element, string propertyName)
		{
			return element.GetProperty(propertyName).GetInt64();
		}

		public static long GetInt64(this JsonElement element, string propertyName, int fallback)
		{
			if (!element.TryGetProperty(propertyName, out JsonElement property))
			{
				return fallback;
			}

			return property.GetInt64();
		}

		public static bool GetBoolean(this JsonElement element, string propertyName)
		{
			return element.GetProperty(propertyName).GetBoolean();
		}

		public static bool GetBoolean(this JsonElement element, string propertyName, bool fallback)
		{
			if (!element.TryGetProperty(propertyName, out JsonElement property))
			{
				return fallback;
			}

			return property.GetBoolean();
		}
	}
}
