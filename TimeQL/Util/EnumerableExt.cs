﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Util
{
	internal static class EnumerableExt
	{
		public static IEnumerable<KeyValuePair<object, object?>> Generalize<T, U>(this IEnumerable<KeyValuePair<T, U>> enumerable)
		{
			return enumerable.Select(e => KeyValuePair.Create<object, object?>(e.Key!, e.Value));
		}

		/// <summary>
		/// Check if all elements of the series are the same.
		/// </summary>
		public static bool IsSameForAll<T>(this IEnumerable<T> enumerable)
		{
			return IsSameForAll(enumerable, v => v);
		}

		/// <summary>
		/// Check if all selected elements of the series are the same.
		/// </summary>
		public static bool IsSameForAll<T, U>(this IEnumerable<T> enumerable, Func<T, U> selector)
		{
			var enumerator = enumerable.GetEnumerator();

			if (!enumerator.MoveNext())
			{
				return true;
			}

			var comparer = EqualityComparer<U>.Default;
			U reference = selector(enumerator.Current);

			while (enumerator.MoveNext())
			{
				if (!comparer.Equals(reference, selector(enumerator.Current)))
				{
					return false;
				}
			}

			return true;
		}
	}
}
