﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Util
{
	public static class StopwatchExt
	{
		public static TimeSpan GetElapsedAndRestart(this Stopwatch sw)
		{
			var result = sw.Elapsed;
			sw.Restart();
			return result;
		}
	}
}
