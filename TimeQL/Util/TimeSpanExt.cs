﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Util
{
	internal static class TimeSpanExt
	{
		public static string ToInflux(this TimeSpan timeSpan)
		{
			StringBuilder sb = new();

			if (timeSpan.Days != 0)
			{
				sb.AppendFormat("{0}d", timeSpan.Days);
			}

			if (timeSpan.Hours != 0)
			{
				sb.AppendFormat("{0}h", timeSpan.Hours);
			}

			if (timeSpan.Minutes != 0)
			{
				sb.AppendFormat("{0}m", timeSpan.Minutes);
			}

			if (timeSpan.Seconds != 0)
			{
				sb.AppendFormat("{0}s", timeSpan.Seconds);
			}

			if (timeSpan.Milliseconds != 0)
			{
				sb.AppendFormat("{0}ms", timeSpan.Milliseconds);
			}

			return sb.ToString();
		}
	}
}
