﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TimeQL.Cloudwatch;
using TimeQL.Graphite;
using TimeQL.Influx;
using TimeQL.Prometheus;
using TimeQL.Util;
using TimeQL.Values;

namespace TimeQL.Libraries
{
	public class TimeQLLib
	{
		private readonly QueryCache queryCache;

		public InfluxLib InfluxLib { get; init; }
		public GraphiteLib GraphiteLib { get; init; }
		public CloudwatchLib CloudwatchLib { get; init; }
		public PrometheusLib PrometheusLib { get; init; }
		public SeriesLib SeriesLib { get; init; }

		public TimeQLLib(Config config, QueryCache queryCache, Lispery.Environment env)
		{
			this.queryCache = queryCache;

			KnownIdentifiers ki = new(env);
			InfluxLib = new InfluxLib(config, queryCache, ki, env);
			GraphiteLib = new GraphiteLib(config, queryCache, ki, env);
			CloudwatchLib = new CloudwatchLib(config, queryCache, ki, env);
			PrometheusLib = new PrometheusLib(config, queryCache, ki, env);
			SeriesLib = new SeriesLib(ki, env);

			Conversion.RegisterConverter<TimeSpan>(TimeSpanConverter);
			Conversion.RegisterConverter<DateTime>(DateTimeConverter);

			env.DefineFunction<string, DateTime>("date-trunc", DateTrunc!);
			env.DefineFunction<string>("interval", s => ParseTimeSpan(s!));
			env.DefineFunction<string>("timestamp", s => ParseDateTime(s!));
			env.DefineFunction<string>("statistics", Statistics!);
		}

		private object? DateTrunc(string component, DateTime dt)
		{
			return component switch
			{
				"y"  => new DateTime(dt.Year, 1, 1, 0, 0, 0, dt.Kind),
				"mo" => new DateTime(dt.Year, dt.Month, 1, 0, 0, 0, dt.Kind),
				"d"  => new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, dt.Kind),
				"h"  => new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, 0, 0, dt.Kind),
				"m"  => new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, 0, dt.Kind),
				"s"  => new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, dt.Kind),
				_    => throw new ArgumentException("Invalid component on truncate"),
			};
		}

		/// <summary>
		/// Converts an arbitrary value into a timespan.
		/// </summary>
		private static object? TimeSpanConverter(object? value)
		{
			return value switch
			{
				string str  => ParseTimeSpan(str),
				TimeSpan ts => ts,
				_           => throw Conversion.GetTypeException<TimeSpan>(value),
			};
		}

		/// <summary>
		/// Converts an arbitrary value into a datetime.
		/// </summary>
		private static object? DateTimeConverter(object? value)
		{
			return value switch
			{
				string str  => ParseDateTime(str),
				DateTime dt => dt,
				_           => throw Conversion.GetTypeException<DateTime>(value),
			};
		}

		/// <summary>
		/// Converts a string into a timespan.
		/// </summary>
		public static TimeSpan ParseTimeSpan(string interval)
		{
			Match match = Regex.Match(interval, @"(?<minus>-)?((?<days>\d+)d)?((?<hours>\d+)h)?((?<minutes>\d+)m)?((?<seconds>\d+)s)?");

			if (match.Success)
			{
				bool success = false;
				int seconds = 0;

				if (match.Groups["days"].Success)
				{
					success = true;
					seconds += int.Parse(match.Groups["days"].Value, CultureInfo.InvariantCulture) * 86400;
				}

				if (match.Groups["hours"].Success)
				{
					success = true;
					seconds += int.Parse(match.Groups["hours"].Value, CultureInfo.InvariantCulture) * 3600;
				}

				if (match.Groups["minutes"].Success)
				{
					success = true;
					seconds += int.Parse(match.Groups["minutes"].Value, CultureInfo.InvariantCulture) * 60;
				}

				if (match.Groups["seconds"].Success)
				{
					success = true;
					seconds += int.Parse(match.Groups["seconds"].Value, CultureInfo.InvariantCulture);
				}

				if (!success)
				{
					throw new FormatException();
				}

				TimeSpan result = TimeSpan.FromSeconds(seconds);

				if (match.Groups["minus"].Success)
				{
					result = -result;
				}

				return result;
			}
			else
			{
				throw new FormatException();
			}
		}

		/// <summary>
		/// Converts a string into a datetime.
		/// </summary>
		public static DateTime ParseDateTime(string time)
		{
			if (DateTime.TryParseExact(
				time,
				"yyyy-MM-dd HH:mm:ss",
				CultureInfo.InvariantCulture,
				DateTimeStyles.AssumeLocal,
				out var result))
			{
				return result.ToUniversalTime();
			}

			if (DateTime.TryParseExact(
				time,
				"yyyy-MM-dd HH:mm",
				CultureInfo.InvariantCulture,
				DateTimeStyles.AssumeLocal,
				out result))
			{
				return result.ToUniversalTime();
			}

			result = DateTime.ParseExact(
				time,
				"yyyy-MM-dd",
				CultureInfo.InvariantCulture,
				DateTimeStyles.AssumeLocal).ToUniversalTime();

			return result.ToUniversalTime();
		}

		public object? Statistics(string query)
		{
			return query switch
			{
				"cache" => GetCacheStatistics(),
				_       => throw new ArgumentException("No such statistic."),
			};
		}

		private NamedValueList GetCacheStatistics()
		{
			var builder = ImmutableList.CreateBuilder<NamedValue>();

			builder.Add(new NamedValue("entry-count", queryCache.EntryCount));
			builder.Add(new NamedValue("compressed-bytes", queryCache.Size));
			builder.Add(new NamedValue("uncompressed-bytes", queryCache.UncompressedSize));
			builder.Add(new NamedValue("average-entry-size", queryCache.AverageEntrySize));
			builder.Add(new NamedValue("compression-ratio", queryCache.CompressionRatio));

			return new NamedValueList(builder.ToImmutable());
		}
	}
}
