﻿using Lispery;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Sequences;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TimeQL.Interfaces;
using TimeQL.Util;
using TimeQL.Values;

namespace TimeQL.Libraries
{
	public class SeriesLib
	{
		private static readonly Random random = new();
		private readonly KnownIdentifiers ki;
		private readonly Lispery.Environment env;
		private readonly Keyword kwInput;
		private readonly Keyword kwDays;
		private readonly Keyword kwHours;
		private readonly Keyword kwMinutes;
		private readonly Keyword kwSeconds;
		private readonly Keyword kwSteps;
		private readonly Keyword kwFraction;
		private readonly Keyword kwPi;
		private readonly Keyword kwPi2;
		private readonly Keyword kwMeta;
		private readonly FilterExpressionParser filterParser;

		public SeriesLib(KnownIdentifiers ki, Lispery.Environment env)
		{
			this.ki = ki;
			this.env = env;

			kwInput = env.GetKeyword("input");
			kwDays = env.GetKeyword("days");
			kwHours = env.GetKeyword("hours");
			kwMinutes = env.GetKeyword("minutes");
			kwSeconds = env.GetKeyword("seconds");
			kwSteps = env.GetKeyword("steps");
			kwFraction = env.GetKeyword("fraction");
			kwPi = env.GetKeyword("pi");
			kwPi2 = env.GetKeyword("pi2");
			kwMeta = env.GetKeyword("meta");
			
			filterParser = new FilterExpressionParser(env.KeywordTable);

			// Constructors.
			env.DefineFunction<string, Dictionary>("series", Series!);
			env.DefineFunction<string, Dictionary, Dictionary>("series", Series!);
			env.DefineFunction<object>("series-list", SeriesList!);
			env.DefineFunction<string, double>("named-value", NamedValue!);
			env.DefineFunction<string, double, Dictionary>("named-value", NamedValue!);
			env.DefineFunction<object>("named-value-list", NamedValueList!);
			env.DefineFunction<IFunction, object>("gen-series", GenSeries!);
			env.DefineFunction<string, IFunction, object>("gen-series", GenSeries!);

			// Type checks.
			env.DefineFunction<object?>("is-series?", v => v is TimeSeries);
			env.DefineFunction<object?>("is-series-list?", v => v is TimeSeriesList);
			env.DefineFunction<object?>("is-named-value?", v => v is NamedValue);
			env.DefineFunction<object?>("is-named-value-list?", v => v is NamedValueList);
			env.DefineFunction<object?>("is-annotated-value?", v => v is IAnnotatedValue);

			// Metadata manipulation.
			env.DefineFunction<IAnnotatedValue>("meta", s => new Dictionary(s!.Metadata.Generalize()));
			env.DefineFunction<IEnumerable<IAnnotatedValue>>("meta", l => new Dictionary(Metadata.Join(l!).Generalize()));
			env.DefineFunction<Keyword, IAnnotatedValue>("meta", (k, s) => s!.Metadata[k!]);
			env.DefineFunction<Keyword, IEnumerable<IAnnotatedValue>>("meta", (k, l) => Metadata.Join(l!)[k!]);
			env.DefineFunction<ISequence, IAnnotatedValue>("meta", (s, v) => Metadata.Select(v!.Metadata, s!));
			env.DefineFunction<ISequence, IEnumerable<IAnnotatedValue>>("meta", (s, v) => Metadata.Select(Metadata.Join(v!), s!));
			env.DefineFunction<Keyword, object?, TimeSeries>("set-meta", (k, v, s) => s!.SetMeta(k!, v));
			env.DefineFunction<Keyword, object?, NamedValue>("set-meta", (k, v, n) => n!.SetMeta(k!, v));
			env.DefineFunction<Keyword, object?, TimeSeriesList>("set-meta", (k, v, l) => l!.SetMeta(k!, v));
			env.DefineFunction<Keyword, object?, NamedValueList>("set-meta", (k, v, l) => l!.SetMeta(k!, v));
			env.DefineFunction<IAnnotatedValue>("meta-keys", s => new Set(s!.Metadata.Keys));
			env.DefineFunction<IEnumerable<IAnnotatedValue>>("meta-keys", l => new Set(Metadata.Join(l!).Keys));
			env.DefineFunction<IEnumerable<IAnnotatedValue>>("meta-keys-all", l => new Set(l!.SelectMany(v => v.Metadata.Keys).Distinct()));

			// Name manipulation.
			env.DefineFunction<IAnnotatedValue>("name", n => n!.Name);
			env.DefineFunction<string, TimeSeries>("set-name", (n, s) => s!.SetName(n!));
			env.DefineFunction<string, NamedValue>("set-name", (n, s) => s!.SetName(n!));
			env.DefineFunction<Keyword, TimeSeries>("set-name", (k, s) => s!.SetName(k!));
			env.DefineFunction<Keyword, NamedValue>("set-name", (k, s) => s!.SetName(k!));
			env.DefineFunction<ISequence, TimeSeries>("set-name", (seq, s) => s!.SetName(seq!));
			env.DefineFunction<ISequence, NamedValue>("set-name", (seq, s) => s!.SetName(seq!));
			env.DefineFunction<string, TimeSeriesList>("set-name", (n, l) => l!.SetName(n!));
			env.DefineFunction<string, NamedValueList>("set-name", (n, l) => l!.SetName(n!));
			env.DefineFunction<Keyword, TimeSeriesList>("set-name", (k, l) => l!.SetName(k!));
			env.DefineFunction<Keyword, NamedValueList>("set-name", (k, l) => l!.SetName(k!));
			env.DefineFunction<ISequence, TimeSeriesList>("set-name", (seq, l) => l!.SetName(seq!));
			env.DefineFunction<ISequence, NamedValueList>("set-name", (seq, l) => l!.SetName(seq!));

			// Series manipulation.
			env.DefineFunction<IFunction, TimeSeries>("map-values", MapValues!);
			env.DefineFunction<IFunction, TimeSeriesList>("map-values", MapValues!);
			env.DefineFunction<IFunction, TimeSeries>("scan-values", ScanValues!);
			env.DefineFunction<IFunction, double, TimeSeries>("scan-values", ScanValues!);
			env.DefineFunction<IFunction, TimeSeriesList>("scan-values", ScanValues!);
			env.DefineFunction<IFunction, double, TimeSeriesList>("scan-values", ScanValues!);
			env.DefineFunction<IFunction, TimeSeries>("filter-values", FilterValues!);
			env.DefineFunction<IFunction, TimeSeriesList>("filter-values", FilterValues!);
			env.DefineFunction<IFunction, TimeSeries>("fold-values", FoldValues!);
			env.DefineFunction<IFunction, double, TimeSeries>("fold-values", FoldValues!);
			env.DefineFunction<IFunction, TimeSeriesList>("fold-values", FoldValues!);
			env.DefineFunction<IFunction, double, TimeSeriesList>("fold-values", FoldValues!);
			env.DefineFunction<IFunction, TimeSeriesList>("reduce-values", ReduceValues!);
			env.DefineFunction<long, IFunction, TimeSeries>("slide-map", SlideMap!);
			env.DefineFunction<long, IFunction, TimeSeriesList>("slide-map", SlideMap!);
			env.DefineFunction<long, ISlider, TimeSeries>("slide-map", SlideMap!);
			env.DefineFunction<long, ISlider, TimeSeriesList>("slide-map", SlideMap!);
			env.DefineFunction<TimeSpan, IFunction, TimeSeries>("slide-map", SlideMap!);
			env.DefineFunction<TimeSpan, IFunction, TimeSeriesList>("slide-map", SlideMap!);
			env.DefineFunction<TimeSpan, ISlider, TimeSeries>("slide-map", SlideMap!);
			env.DefineFunction<TimeSpan, ISlider, TimeSeriesList>("slide-map", SlideMap!);
			env.DefineFunction<string, IFunction, TimeSeries>("slide-map", SlideMap!);
			env.DefineFunction<string, IFunction, TimeSeriesList>("slide-map", SlideMap!);
			env.DefineFunction<string, ISlider, TimeSeries>("slide-map", SlideMap!);
			env.DefineFunction<string, ISlider, TimeSeriesList>("slide-map", SlideMap!);
			env.DefineFunction<TimeSpan, IFunction, TimeSeries>("window-map", WindowMap!);
			env.DefineFunction<TimeSpan, IFunction, TimeSeriesList>("window-map", WindowMap!);
			env.DefineFunction<string, IFunction, TimeSeries>("window-map", WindowMap!);
			env.DefineFunction<string, IFunction, TimeSeriesList>("window-map", WindowMap!);
			env.DefineFunction<Keyword, ISequence, TimeSeries>("combine-meta", CombineMeta!);
			env.DefineFunction<Keyword, ISequence, TimeSeriesList>("combine-meta", CombineMeta!);
			env.DefineFunction<object, NamedValueList>("filter-meta", FilterMeta!);
			env.DefineFunction<object, TimeSeriesList>("filter-meta", FilterMeta!);
			env.DefineFunction<TimeSpan, TimeSeries>("time-shift", TimeShift!);
			env.DefineFunction<TimeSpan, TimeSeriesList>("time-shift", TimeShift!);
			env.DefineFunction<string, TimeSeries>("time-shift", TimeShift!);
			env.DefineFunction<string, TimeSeriesList>("time-shift", TimeShift!);
			env.DefineFunction<TimeSeries>("first-value", s => s!.GetFirstValue());
			env.DefineFunction<TimeSeries>("last-value", s => s!.GetLastValue());
			env.DefineFunction<TimeSeriesList>("first-value-set", l => l!.GetFirstValueSet());
			env.DefineFunction<TimeSeriesList>("last-value-set", l => l!.GetLastValueSet());
			env.DefineFunction<TimeSpan, TimeSeries>("align", (i, s) => s!.Align(i!));
			env.DefineFunction<TimeSpan, TimeSeriesList>("align", (i, l) => l!.Align(i!));
			env.DefineFunction<string, TimeSeries>("align", (i, s) => s!.Align(TimeQLLib.ParseTimeSpan(i!)));
			env.DefineFunction<string, TimeSeriesList>("align", (i, l) => l!.Align(TimeQLLib.ParseTimeSpan(i!)));
			env.DefineFunction<TimeSeries>("resolution", s => s!.Resolution);
			env.DefineFunction<TimeSeriesList>("resolution", s => s!.Resolution);
			env.DefineFunction<TimeSeries>("derive", s => s!.Derive());
			env.DefineFunction<TimeSeriesList>("derive", l => l!.Derive());
			env.DefineFunction<TimeSeries>("integrate", s => s!.Integrate());
			env.DefineFunction<TimeSeriesList>("integrate", l => l!.Integrate());
			env.DefineFunction<TimeSeries>("values", s => new LazySequence(s!.DataPoints.Values.Cast<object?>()));
			env.DefineFunction<TimeSpan, TimeSeries>("rand-times", RandTimes!);
			env.DefineFunction<object, IFunction, IFunction, IFunction>("slider", (s, a, r, g) => new GenericSlider(s, a!, r!, g!));

			// Series joins.
			env.DefineFunction<IFunction, TimeSeries>("join", Join!);
			env.DefineFunction<IFunction, TimeSeriesList>("join", Join!);
			env.DefineFunction<IFunction, double, TimeSeries>("join", Join!);
			env.DefineFunction<IFunction, double, TimeSeriesList>("join", Join!);
			env.DefineFunction<object, IFunction, TimeSeriesList>("group", Group!);
			env.DefineFunction<IFunction, TimeSeriesList, TimeSeriesList>("match", Match!);

			// Load example data.
			env.DefineFunction("load-example-data", LoadExampleData);

			// LISP definitions.
			env.Interpreter.Evaluate(@"
				(defn group-join
					([sel fun     list] (group sel #(apply join fun     %) list))
					([sel fun def list] (group sel #(apply join fun def %) list)))

				;(defn group-fold
				;	([sel fun      list] (group sel #(fold fun      %) list))
				;	([sel fun seed list] (group sel #(fold fun seed %) list)))

				(defn exp-smooth [f series]
					(scan-values #(+ (* f %1) (* (- 1.0 f) %2)) series))

				(defn per-second [series] (/ (derive series) 3600))
				(defn per-minute [series] (/ (derive series) 60))
				(def per-hour derive)
			");
		}

		/// <summary>
		/// Creates a new series.
		/// </summary>
		private object? Series(string name, Dictionary dataPoints)
		{
			return Series(name, dataPoints, Dictionary.Empty);
		}

		/// <summary>
		/// Creates a new series.
		/// </summary>
		private object? Series(string name, Dictionary dataPoints, Dictionary metadata)
		{
			var dataPointBuilder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			var metadataBuilder = ImmutableDictionary.CreateBuilder<Keyword, object?>();

			foreach (var entry in dataPoints)
			{
				var time = Conversion.To<DateTime>(entry.Key);
				var value = Conversion.To<double>(entry.Value);
				dataPointBuilder[time] = value;
			}

			foreach (var entry in metadata)
			{
				var key = Conversion.To<Keyword>(entry.Key);
				metadataBuilder[key] = entry.Value;
			}

			return new TimeSeries(
				name,
				dataPointBuilder.ToImmutable(),
				metadataBuilder.ToImmutable());
		}

		/// <summary>
		/// Creates a new series list.
		/// </summary>
		private object? SeriesList(object[] objects)
		{
			var builder = ImmutableList.CreateBuilder<TimeSeries>();

			foreach (var obj in objects)
			{
				switch (obj)
				{
					case TimeSeries series:
						builder.Add(series);
						break;

					case ISequence seq:
						builder.AddRange(seq.GetEntries().Cast<TimeSeries>());
						break;

					default:
						throw new InvalidOperationException("Value has invalid type.");
				}
			}

			return new TimeSeriesList(builder.ToImmutable());
		}

		private object? NamedValue(string name, double value)
		{
			return NamedValue(name, value, Dictionary.Empty);
		}

		private object? NamedValue(string name, double value, Dictionary metadata)
		{
			var metadataBuilder = ImmutableDictionary.CreateBuilder<Keyword, object?>();

			foreach (var entry in metadata)
			{
				var key = Conversion.To<Keyword>(entry.Key);
				metadataBuilder[key] = entry.Value;
			}

			return new NamedValue(
				name,
				value,
				metadataBuilder.ToImmutable());
		}

		/// <summary>
		/// Creates a new named value list.
		/// </summary>
		private object? NamedValueList(object[] objects)
		{
			var builder = ImmutableList.CreateBuilder<NamedValue>();

			foreach (var obj in objects)
			{
				switch (obj)
				{
					case NamedValue value:
						builder.Add(value);
						break;

					case ISequence seq:
						builder.AddRange(seq.GetEntries().Cast<NamedValue>());
						break;

					default:
						throw new InvalidOperationException("Value has invalid type.");
				}
			}

			return new NamedValueList(builder.ToImmutable());
		}

		/// <summary>
		/// Generates a time series from a function.
		/// </summary>
		private object? GenSeries(IFunction fun, object[] args)
		{
			return GenSeries(fun.Name ?? "result", fun, args);
		}

		/// <summary>
		/// Generates a time series from a function.
		/// </summary>
		private object? GenSeries(string name, IFunction fun, object[] args)
		{
			var options = new Options(args);

			TimeSpan shift = options.Get<TimeSpan>(ki.KwShift, TimeSpan.Zero);
			TimeSpan resolution = options.Get<TimeSpan>(ki.KwResolution, env.Resolve(ki.SymEmResolution)!);
			DateTime windowStart = options.Get<DateTime>(ki.KwWindowStart, env.Resolve(ki.SymEmWindowStart)!);
			DateTime windowStop = options.Get<DateTime>(ki.KwWindowStop, env.Resolve(ki.SymEmWindowStop)!);
			Keyword input = options.Get<Keyword>(kwInput, kwFraction);
			Dictionary metadata = options.Get<Dictionary>(kwMeta, Dictionary.Empty);

			windowStart = (windowStart - shift).AlignDown(resolution);
			windowStop = (windowStop - shift).AlignDown(resolution);
			TimeSpan window = windowStop - windowStart;

			Func<TimeSpan, long, double> convFun;

			if      (input == kwDays)    convFun = (d, _) => d.TotalDays;
			else if (input == kwHours)   convFun = (d, _) => d.TotalHours;
			else if (input == kwMinutes) convFun = (d, _) => d.TotalMinutes;
			else if (input == kwSeconds) convFun = (d, _) => d.TotalSeconds;
			else if (input == kwSteps)   convFun = (_, s) => s;
			else if (input == kwPi)      convFun = (d, _) => d / window * Math.PI;
			else if (input == kwPi2)     convFun = (d, _) => d / window * Math.PI * 2.0;
			else                         convFun = (d, _) => d / window;

			var datapointBuilder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			long step = 0;

			for (DateTime time = windowStart; time <= windowStop; time += resolution)
			{
				TimeSpan delta = time - windowStart;
				double value = Conversion.To<double>(fun.Call(convFun(delta, step)));
				datapointBuilder[time] = value;
				step++;
			}

			var metadataBuilder = ImmutableDictionary.CreateBuilder<Keyword, object?>();

			foreach (var entry in metadata)
			{
				metadataBuilder[Conversion.To<Keyword>(entry.Key)] = entry.Value;
			}

			return new TimeSeries(
				name!,
				datapointBuilder.ToImmutable(),
				metadataBuilder.ToImmutable(),
				resolution);
		}

		/// <summary>
		/// Maps a function on the values of the given series.
		/// </summary>
		private static TimeSeries MapValues(IFunction fun, TimeSeries series)
		{
			return series.MapValues(v => Conversion.To<double>(fun.Call(v)));
		}

		/// <summary>
		/// Maps a function on the values of the given series list.
		/// </summary>
		private static TimeSeriesList MapValues(IFunction fun, TimeSeriesList list)
		{
			return list.Map(s => MapValues(fun, s));
		}

		/// <summary>
		/// Folds a function on the values of the given series. Records every accumulator value.
		/// </summary>
		private static TimeSeries ScanValues(IFunction fun, TimeSeries series)
		{
			var foldFun = Optimizer.OptimizeCombineDouble(fun);
			return series.ScanValues(foldFun);
		}

		/// <summary>
		/// Folds a function on the values of the given series. Records every accumulator value.
		/// </summary>
		private static TimeSeries ScanValues(IFunction fun, double seed, TimeSeries series)
		{
			var foldFun = Optimizer.OptimizeCombineDouble(fun);
			return series.ScanValues(foldFun, seed);
		}

		/// <summary>
		/// Folds a function on the values of the given series list. Records every accumulator value.
		/// </summary>
		private static TimeSeriesList ScanValues(IFunction fun, TimeSeriesList list)
		{
			return list.Map(s => ScanValues(fun, s));
		}

		/// <summary>
		/// Folds a function on the values of the given series list. Records every accumulator value.
		/// </summary>
		private static TimeSeriesList ScanValues(IFunction fun, double seed, TimeSeriesList list)
		{
			return list.Map(s => ScanValues(fun, seed, s));
		}

		/// <summary>
		/// Slides a window through the series. Applies the function on each window.
		/// </summary>
		private static TimeSeries SlideMap(long count, IFunction fun, TimeSeries series)
		{
			Check.IsNonNegativeInt(count);
			return series.SlideMap((int)count, Slider.Create(fun));
		}

		/// <summary>
		/// Slides a window through the series list. Applies the function on each window.
		/// </summary>
		private static TimeSeriesList SlideMap(long count, IFunction fun, TimeSeriesList list)
		{
			return list.Map(s => SlideMap(count, fun, s));
		}

		/// <summary>
		/// Slides a window through the series. Applies the function on each window.
		/// </summary>
		private static TimeSeries SlideMap(long count, ISlider aggregator, TimeSeries series)
		{
			Check.IsNonNegativeInt(count);
			return series.SlideMap((int)count, aggregator);
		}

		/// <summary>
		/// Slides a window through the series list. Applies the function on each window.
		/// </summary>
		private static TimeSeriesList SlideMap(long count, ISlider aggregator, TimeSeriesList list)
		{
			return list.Map(s => SlideMap(count, aggregator, s));
		}

		/// <summary>
		/// Slides a window through the series. Applies the function on each window.
		/// </summary>
		private static TimeSeries SlideMap(TimeSpan interval, IFunction fun, TimeSeries series)
		{
			return series.SlideMap(interval, Slider.Create(fun));
		}

		/// <summary>
		/// Slides a window through the series list. Applies the function on each window.
		/// </summary>
		private static TimeSeriesList SlideMap(TimeSpan interval, IFunction fun, TimeSeriesList list)
		{
			return list.Map(s => SlideMap(interval, fun, s));
		}

		/// <summary>
		/// Slides a window through the series. Applies the function on each window.
		/// </summary>
		private static TimeSeries SlideMap(TimeSpan interval, ISlider aggregator, TimeSeries series)
		{
			return series.SlideMap(interval, aggregator);
		}

		/// <summary>
		/// Slides a window through the series list. Applies the function on each window.
		/// </summary>
		private static TimeSeriesList SlideMap(TimeSpan interval, ISlider aggregator, TimeSeriesList list)
		{
			return list.Map(s => SlideMap(interval, aggregator, s));
		}

		/// <summary>
		/// Slides a window through the series. Applies the function on each window.
		/// </summary>
		private static TimeSeries SlideMap(string interval, IFunction fun, TimeSeries series)
		{
			return SlideMap(TimeQLLib.ParseTimeSpan(interval), fun, series);
		}

		/// <summary>
		/// Slides a window through the series list. Applies the function on each window.
		/// </summary>
		private static TimeSeriesList SlideMap(string interval, IFunction fun, TimeSeriesList list)
		{
			return SlideMap(TimeQLLib.ParseTimeSpan(interval), fun, list);
		}

		/// <summary>
		/// Slides a window through the series. Applies the function on each window.
		/// </summary>
		private static TimeSeries SlideMap(string interval, ISlider aggregator, TimeSeries series)
		{
			return SlideMap(TimeQLLib.ParseTimeSpan(interval), aggregator, series);
		}

		/// <summary>
		/// Slides a window through the series list. Applies the function on each window.
		/// </summary>
		private static TimeSeriesList SlideMap(string interval, ISlider aggregator, TimeSeriesList list)
		{
			return SlideMap(TimeQLLib.ParseTimeSpan(interval), aggregator, list);
		}

		/// <summary>
		/// Groups values into windows. Applies the function on each window.
		/// </summary>
		private static TimeSeries WindowMap(TimeSpan interval, IFunction fun, TimeSeries series)
		{
			var windowFun = Optimizer.OptimizeAggregate(fun);
			return series.WindowMap(interval, windowFun);
		}

		/// <summary>
		/// Groups values into windows. Applies the function on each window.
		/// </summary>
		private static TimeSeriesList WindowMap(TimeSpan interval, IFunction fun, TimeSeriesList list)
		{
			return list.Map(s => WindowMap(interval, fun, s));
		}

		/// <summary>
		/// Groups values into windows. Applies the function on each window.
		/// </summary>
		private static TimeSeries WindowMap(string interval, IFunction fun, TimeSeries series)
		{
			return WindowMap(TimeQLLib.ParseTimeSpan(interval), fun, series);
		}

		/// <summary>
		/// Groups values into windows. Applies the function on each window.
		/// </summary>
		private static TimeSeriesList WindowMap(string interval, IFunction fun, TimeSeriesList list)
		{
			return WindowMap(TimeQLLib.ParseTimeSpan(interval), fun, list);
		}

		/// <summary>
		/// Concatenates metadata into new metadata.
		/// </summary>
		private static TimeSeries CombineMeta(Keyword key, ISequence seq, TimeSeries series)
		{
			return series.SetMeta(key, Metadata.Select(series.Metadata, seq));
		}

		/// <summary>
		/// Concatenates metadata into new metadata.
		/// </summary>
		private static TimeSeriesList CombineMeta(Keyword key, ISequence seq, TimeSeriesList list)
		{
			return list.Map(s => CombineMeta(key, seq, s));
		}

		/// <summary>
		/// Filters the values of the given series.
		/// </summary>
		private static TimeSeries FilterValues(IFunction fun, TimeSeries series)
		{
			return series.FilterValues(v => Conversion.IsTruthy(fun.Call(v)));
		}

		/// <summary>
		/// Filters the values of the given series list.
		/// </summary>
		private static TimeSeriesList FilterValues(IFunction fun, TimeSeriesList list)
		{
			return list.Map(s => FilterValues(fun, s));
		}

		/// <summary>
		/// Folds the values of the given series.
		/// </summary>
		private static object? FoldValues(IFunction fun, TimeSeries series)
		{
			var foldFun = Optimizer.OptimizeCombineDouble(fun);
			return series.FoldValues(foldFun);
		}

		/// <summary>
		/// Folds the values of the given series.
		/// </summary>
		private static object? FoldValues(IFunction fun, double seed, TimeSeries series)
		{
			var foldFun = Optimizer.OptimizeCombineDouble(fun);
			return series.FoldValues(foldFun, seed);
		}

		/// <summary>
		/// Folds the values of the given series sequence.
		/// </summary>
		private static object? FoldValues(IFunction fun, TimeSeriesList list)
		{
			return list.Map(s => new NamedValue(
				s.Name,
				Conversion.To<double>(FoldValues(fun, s)),
				s.Metadata));
		}

		/// <summary>
		/// Folds the values of the given series sequence.
		/// </summary>
		private static object? FoldValues(IFunction fun, double seed, TimeSeriesList list)
		{
			return list.Map(s => new NamedValue(
				s.Name,
				Conversion.To<double>(FoldValues(fun, seed, s)),
				s.Metadata));
		}

		/// <summary>
		/// Maps a function on the series list and returns named values.
		/// </summary>
		private static NamedValueList ReduceValues(IFunction fun, TimeSeriesList list)
		{
			return list.Map(s => new NamedValue(s.Name, Conversion.To<double>(fun.Call(new LazySequence(s.DataPoints.Values.Cast<object?>()))), s.Metadata));
		}

		/// <summary>
		/// Filters a named value list by metadata.
		/// </summary>
		private object? FilterMeta(object filter, NamedValueList list)
		{
			return list.Filter(filterParser.Parse(filter));
		}

		/// <summary>
		/// Filters a time series list by metadata.
		/// </summary>
		private object? FilterMeta(object filter, TimeSeriesList list)
		{
			return list.Filter(filterParser.Parse(filter));
		}

		/// <summary>
		/// Time-shifts the given time series.
		/// </summary>
		private object? TimeShift(TimeSpan delta, TimeSeries series)
		{
			return series.TimeShift(delta);
		}

		/// <summary>
		/// Time-shifts the given time series.
		/// </summary>
		private object? TimeShift(TimeSpan delta, TimeSeriesList list)
		{
			return list.TimeShift(delta);
		}

		/// <summary>
		/// Time-shifts the given time series.
		/// </summary>
		private object? TimeShift(string delta, TimeSeries series)
		{
			return TimeShift(TimeQLLib.ParseTimeSpan(delta), series);
		}

		/// <summary>
		/// Time-shifts the given time series.
		/// </summary>
		private object? TimeShift(string delta, TimeSeriesList list)
		{
			return TimeShift(TimeQLLib.ParseTimeSpan(delta), list);
		}

		/// <summary>
		/// Randomizes timestamps in a series.
		/// </summary>
		private static object? RandTimes(TimeSpan interval, TimeSeries series)
		{
			return series.RandomizeTimes(random, interval);
		}

		/// <summary>
		/// Performs an inner join between multiple series.
		/// </summary>
		private static TimeSeries Join(IFunction fun, TimeSeries[] list)
		{
			var joinFun = Optimizer.OptimizeAggregate(fun);
			return TimeSeries.Join(joinFun, list);
		}

		/// <summary>
		/// Performs an inner join between multiple series.
		/// </summary>
		private static TimeSeries Join(IFunction fun, TimeSeriesList list)
		{
			var joinFun = Optimizer.OptimizeAggregate(fun);
			return TimeSeries.Join(joinFun, list);
		}

		/// <summary>
		/// Performs an outer join between multiple series.
		/// </summary>
		private static TimeSeries Join(IFunction fun, double fallback, TimeSeries[] list)
		{
			var joinFun = Optimizer.OptimizeAggregate(fun);
			return TimeSeries.Join(joinFun, fallback, list);
		}

		/// <summary>
		/// Performs an outer join between multiple series.
		/// </summary>
		private static TimeSeries Join(IFunction fun, double fallback, TimeSeriesList list)
		{
			var joinFun = Optimizer.OptimizeAggregate(fun);
			return TimeSeries.Join(joinFun, fallback, list);
		}

		/// <summary>
		/// Groups series and maps a function over the groups.
		/// </summary>
		private static TimeSeriesList Group(object selExpr, IFunction fun, TimeSeriesList list)
		{
			var selFun = CreateSelector(selExpr);
			var groups = list.Entries.GroupBy<TimeSeries, object?>(selFun);
			var builder = ImmutableList.CreateBuilder<TimeSeries>();

			foreach (var group in groups.OrderBy(g => g.Key?.ToString() ?? "nil"))
			{
				var name = group.Key?.ToString() ?? "nil";
				var seq = new TimeSeriesList(group);
				builder.Add(Conversion.To<TimeSeries>(fun.Call(seq)).SetName(name));
			}

			return new TimeSeriesList(builder.ToImmutableList());
		}

		/// <summary>
		/// Groups series and maps a function over the groups.
		/// </summary>
		private static TimeSeriesList Match(IFunction fun, TimeSeriesList lhs, TimeSeriesList rhs)
		{
			return lhs.Match((a, b) => Conversion.To<TimeSeries>(fun.Call(a, b)), rhs);
		}

		/// <summary>
		/// Loads some example data (for the documentation).
		/// </summary>
		private object? LoadExampleData()
		{
			return env.Interpreter.Evaluate(@"
				(def sin-series (gen-series sin :input :pi2 :meta { :type :trigonometric :function ""f(x) = sin(x)"" :name ""sin"" }))
				(def cos-series (gen-series cos :input :pi2 :meta { :type :trigonometric :function ""f(x) = cos(x)"" :name ""cos"" }))
				(def rand-series (gen-series ""rand"" (fn [_] (rand)) :meta { :type :random :function ""f(x) = random()"" :name ""rand"" }))
				(def zero-series (gen-series ""zero"" (fn [_] 0) :meta { :type :constant :function ""f(x) = 0"" :name ""zero"" :even true :odd false :value 0 }))
				(def one-series (gen-series ""one"" (fn [_] 1) :meta { :type :constant :function ""f(x) = 1"" :name ""one"" :even false :odd true :value 1 }))
				(def two-series (gen-series ""two"" (fn [_] 2) :meta { :type :constant :function ""f(x) = 2"" :name ""two"" :even true :odd false :value 2 }))
				(def three-series (gen-series ""three"" (fn [_] 2) :meta { :type :constant :function ""f(x) = 3"" :name ""three"" :even false :odd true :value 3 }))
				(def linear-series (gen-series ""linear"" identity :meta { :type :polynomial :function ""f(x) = x"" :name ""linear"" }))
				(def square-series (gen-series ""square"" #(* % %) :meta { :type :polynomial :function ""f(x) = x^2"" :name ""square"" }))
				(def cube-series (gen-series ""cube"" #(* % % %) :meta { :type :polynomial :function ""f(x) = x^3"" :name ""cube"" }))
				(def step-series (gen-series ""step"" #(if (<= % 0.5) 0 1) :meta { :type :other :function ""f(x) = x <= 0.5 ? 0 : 1"" :name ""step"" }))

				(def trig-list (series-list sin-series cos-series))
				(def const-list (series-list zero-series one-series two-series three-series))
				(def poly-list (series-list linear-series square-series cube-series))
				(def example-list (series-list sin-series cos-series rand-series zero-series one-series two-series linear-series square-series cube-series step-series))

				'[sin-series cos-series rand-series zero-series one-series two-series three-series linear-series square-series cube-series step-series trig-list const-list poly-list example-list]
			");
		}

		/// <summary>
		/// Creates a selector function.
		/// </summary>
		private static Func<IAnnotatedValue, object?> CreateSelector(object expression)
		{
			return expression switch
			{
				Keyword key   => s => s.Metadata[key],
				IFunction fun => s => fun.Call(s),
				ISequence seq => s => Metadata.Select(s.Metadata, seq),
				_             => throw new ArgumentException("Invalid select expression"),
			};
		}
	}
}
