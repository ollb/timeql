# Functions

There are various functions that can be run on a time series, mostly to modify, aggregate or
filter the values in the time series datapoints.
