# Metadata

Each time series can have arbitrary metadata attached to it. It can be queried and manipulated
with the `meta`, `meta-keys` and `set-meta` functions:

```clojure
>> (meta sin-series)
{:type :trigonometric :name "sin" :function "f(x) = sin(x)"}
>> (meta :type sin-series)
:trigonometric
>> (meta-keys sin-series)
#{:type :name :function}
```

We can also modify the metadata. Please note that this will return a new time series, as the type
is immutable:

```clojure
>> (def a (set-meta :test 9 sin-series))
Timestamp                sin
2022-03-16 18:00:00     0.00
2022-03-16 20:00:00     0.50
2022-03-16 22:00:00     0.87
2022-03-17 00:00:00     1.00
2022-03-17 02:00:00     0.87
2022-03-17 04:00:00     0.50
2022-03-17 06:00:00     0.00
2022-03-17 08:00:00    -0.50
2022-03-17 10:00:00    -0.87
2022-03-17 12:00:00    -1.00
2022-03-17 14:00:00    -0.87
2022-03-17 16:00:00    -0.50
2022-03-17 18:00:00    -0.00
>> (meta a)
{:type :trigonometric :test 9 :name "sin" :function "f(x) = sin(x)"}
```

Metadata is very useful for grouping and filtering time series later.

## Combining Metadata

The `meta` function also allows for the easy combination of metadata and strings:

```clojure
>> (meta [:name :function] sin-series)
"sinf(x) = sin(x)"
>> (meta [:name " " :function] sin-series)
"sin f(x) = sin(x)"
>> (meta ["[" :name "] " :function] sin-series)
"[sin] f(x) = sin(x)"
```