# Resolution

Time series can be _aligned_ or _unaligned_. On an aligned time series, the timestamps of all data
points are evenly spaced apart. The time interval between the datapoints in this case is called the
_resolution_ of the series.

For example with a _resolution_ of `"1h"`, there will be a new data point on exactly every hour.
An unaligned time series however can have data points at arbitrary points in time. Such a series will
have a resolution of `nil` instead.

When combining multiple series the _resolution_ has to be identical (in that case the time series are
said to be _aligned_). TimeQL will make no attempt to interpolate data points or reduce resolution in
order to match data points. Instead when combining two series with different resolution, the resulting
series will usually be empty, because the timestamps don't match.

Please note that the series in the example data set are all aligned to the configured `*resolution*`.
When querying data sources, the result will usually be aligned to `*resolution*` as well. However in
general TimeQL can also represent _unaligned_ data.

As an example, we can randomize the times from one of the time series and then re-align them:

```clojure
>> (def unaligned-sin (rand-times "30m" sin-series))
Timestamp                sin
2022-03-16 18:09:29     0.00
2022-03-16 20:13:32     0.50
2022-03-16 22:02:05     0.87
2022-03-17 00:05:45     1.00
2022-03-17 02:13:22     0.87
2022-03-17 03:45:39     0.50
2022-03-17 05:50:09     0.00
2022-03-17 07:48:04    -0.50
2022-03-17 10:02:11    -0.87
2022-03-17 11:59:17    -1.00
2022-03-17 13:59:06    -0.87
2022-03-17 15:48:57    -0.50
2022-03-17 17:48:15    -0.00
>> (resolution unaligned-sin)
nil
>> (def realigned-sin (align "2h" unaligned-sin))
Timestamp                sin
2022-03-16 18:00:00     0.00
2022-03-16 20:00:00     0.50
2022-03-16 22:00:00     0.87
2022-03-17 00:00:00     1.00
2022-03-17 02:00:00     0.50
2022-03-17 04:00:00     0.00
2022-03-17 06:00:00    -0.50
2022-03-17 10:00:00    -1.00
2022-03-17 12:00:00    -0.87
2022-03-17 14:00:00    -0.50
2022-03-17 16:00:00    -0.00
>> (resolution realigned-sin)
<02:00:00>
```

Please note that this can lose data, if two datapoints fall into the same time window.
