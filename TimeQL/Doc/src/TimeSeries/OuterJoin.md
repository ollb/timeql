# Outer Join

```clojure
(join fun def series)
```

The outer join works similar to the [inner join](./InnerJoin.md), but it will treat missing values
differently. Whenever a value is missing from one of the series, it will be replaced by `def`:

```clojure
(def cos-filtered (filter-values #(>= % 0.2) cos-series))
; ...
>> [sin-series cos-filtered (join sum 0 sin-series cos-filtered)]
Timestamp                sin      cos   result
2022-03-16 20:00:00     0.00     1.00     1.00
2022-03-16 22:00:00     0.50     0.87     1.37
2022-03-17 00:00:00     0.87     0.50     1.37
2022-03-17 02:00:00     1.00              1.00
2022-03-17 04:00:00     0.87              0.87
2022-03-17 06:00:00     0.50              0.50
2022-03-17 08:00:00     0.00              0.00
2022-03-17 10:00:00    -0.50             -0.50
2022-03-17 12:00:00    -0.87             -0.87
2022-03-17 14:00:00    -1.00             -1.00
2022-03-17 16:00:00    -0.87     0.50    -0.37
2022-03-17 18:00:00    -0.50     0.87     0.37
2022-03-17 20:00:00    -0.00     1.00     1.00
>> [sin-series cos-filtered (join sum 1 sin-series cos-filtered)]
Timestamp                sin      cos   result
2022-03-16 20:00:00     0.00     1.00     1.00
2022-03-16 22:00:00     0.50     0.87     1.37
2022-03-17 00:00:00     0.87     0.50     1.37
2022-03-17 02:00:00     1.00              2.00
2022-03-17 04:00:00     0.87              1.87
2022-03-17 06:00:00     0.50              1.50
2022-03-17 08:00:00     0.00              1.00
2022-03-17 10:00:00    -0.50              0.50
2022-03-17 12:00:00    -0.87              0.13
2022-03-17 14:00:00    -1.00              0.00
2022-03-17 16:00:00    -0.87     0.50    -0.37
2022-03-17 18:00:00    -0.50     0.87     0.37
2022-03-17 20:00:00    -0.00     1.00     1.00
```
