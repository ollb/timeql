# Inner Join

```clojure
(join fun series...)
```

An inner join will combine one or more time series based on matching timestamps. The join will
find all corresponding datapoints in the `series` and combine their values using the function
`fun`. The aggregator `fun` will receive the values from all `series` as a sequence.

_Note_: if the resulting series is empty, make sure that the input series are aligned, so that
the input timestamps match.

```clojure
>> [sin-series cos-series
    (join sum sin-series cos-series)]
Timestamp                sin      cos   result
2022-03-16 20:00:00     0.00     1.00     1.00
2022-03-16 22:00:00     0.50     0.87     1.37
2022-03-17 00:00:00     0.87     0.50     1.37
2022-03-17 02:00:00     1.00     0.00     1.00
2022-03-17 04:00:00     0.87    -0.50     0.37
2022-03-17 06:00:00     0.50    -0.87    -0.37
2022-03-17 08:00:00     0.00    -1.00    -1.00
2022-03-17 10:00:00    -0.50    -0.87    -1.37
2022-03-17 12:00:00    -0.87    -0.50    -1.37
2022-03-17 14:00:00    -1.00    -0.00    -1.00
2022-03-17 16:00:00    -0.87     0.50    -0.37
2022-03-17 18:00:00    -0.50     0.87     0.37
2022-03-17 20:00:00    -0.00     1.00     1.00
```

This extends to more than one series as well:

```clojure
>> [sin-series cos-series step-series
    (join sum sin-series cos-series step-series)]
Timestamp                sin      cos     step   result
2022-03-16 20:00:00     0.00     1.00     0.00     1.00
2022-03-16 22:00:00     0.50     0.87     0.00     1.37
2022-03-17 00:00:00     0.87     0.50     0.00     1.37
2022-03-17 02:00:00     1.00     0.00     0.00     1.00
2022-03-17 04:00:00     0.87    -0.50     0.00     0.37
2022-03-17 06:00:00     0.50    -0.87     0.00    -0.37
2022-03-17 08:00:00     0.00    -1.00     0.00    -1.00
2022-03-17 10:00:00    -0.50    -0.87     1.00    -0.37
2022-03-17 12:00:00    -0.87    -0.50     1.00    -0.37
2022-03-17 14:00:00    -1.00    -0.00     1.00    -0.00
2022-03-17 16:00:00    -0.87     0.50     1.00     0.63
2022-03-17 18:00:00    -0.50     0.87     1.00     1.37
2022-03-17 20:00:00    -0.00     1.00     1.00     2.00
```

## Arithmetical operators

Please note that the arithmetical operators will also automatically perform an inner join if
two time series are used as arguments:

```clojure
>> [sin-series cos-series
    (+ sin-series cos-series)
    (* sin-series cos-series)]
Timestamp                sin      cos   result   result
2022-03-16 20:00:00     0.00     1.00     1.00     0.00
2022-03-16 22:00:00     0.50     0.87     1.37     0.43
2022-03-17 00:00:00     0.87     0.50     1.37     0.43
2022-03-17 02:00:00     1.00     0.00     1.00     0.00
2022-03-17 04:00:00     0.87    -0.50     0.37    -0.43
2022-03-17 06:00:00     0.50    -0.87    -0.37    -0.43
2022-03-17 08:00:00     0.00    -1.00    -1.00    -0.00
2022-03-17 10:00:00    -0.50    -0.87    -1.37     0.43
2022-03-17 12:00:00    -0.87    -0.50    -1.37     0.43
2022-03-17 14:00:00    -1.00    -0.00    -1.00     0.00
2022-03-17 16:00:00    -0.87     0.50    -0.37    -0.43
2022-03-17 18:00:00    -0.50     0.87     0.37    -0.43
2022-03-17 20:00:00    -0.00     1.00     1.00    -0.00
```

## Missing values

A datapoint will only be present in the result, if a datapoint with the same time was present in
all input series. This can be demonstrated by filtering:

```clojure
>> (def cos-filtered (filter-values #(>= % 0.2) cos-series))
; ...
>> [sin-series cos-filtered (+ sin-series cos-filtered)]
Timestamp                sin      cos   result
2022-03-16 20:00:00     0.00     1.00     1.00
2022-03-16 22:00:00     0.50     0.87     1.37
2022-03-17 00:00:00     0.87     0.50     1.37
2022-03-17 02:00:00     1.00
2022-03-17 04:00:00     0.87
2022-03-17 06:00:00     0.50
2022-03-17 08:00:00     0.00
2022-03-17 10:00:00    -0.50
2022-03-17 12:00:00    -0.87
2022-03-17 14:00:00    -1.00
2022-03-17 16:00:00    -0.87     0.50    -0.37
2022-03-17 18:00:00    -0.50     0.87     0.37
2022-03-17 20:00:00    -0.00     1.00     1.00
```

