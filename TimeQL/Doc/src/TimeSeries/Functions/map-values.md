# `map-values`

```clojure
(map-values fun series)
```

The `map-values` function applies the provided function to all values of the time series.
It works similar to the regular `map` function, but produces a new time series instead:

```clojure
>> [sin-series
    (map-values #(* 2 %) sin-series)
    (map-values abs sin-series)]
Timestamp                sin      sin      sin
2022-03-17 00:00:00     0.00     0.00     0.00
2022-03-17 02:00:00     0.50     1.00     0.50
2022-03-17 04:00:00     0.87     1.73     0.87
2022-03-17 06:00:00     1.00     2.00     1.00
2022-03-17 08:00:00     0.87     1.73     0.87
2022-03-17 10:00:00     0.50     1.00     0.50
2022-03-17 12:00:00     0.00     0.00     0.00
2022-03-17 14:00:00    -0.50    -1.00     0.50
2022-03-17 16:00:00    -0.87    -1.73     0.87
2022-03-17 18:00:00    -1.00    -2.00     1.00
2022-03-17 20:00:00    -0.87    -1.73     0.87
2022-03-17 22:00:00    -0.50    -1.00     0.50
2022-03-18 00:00:00    -0.00    -0.00     0.00
```
