# `fold-values`

```clojure
(fold-values fun series)
(fold-values fun init series)
```

This is the equivalent of the normal `fold` function for time series. It will run the fold function
`fun` on all values in the time series. For each call, the result of the previous call is given to
`fun` as the second argument (the _accumulator_ value).

The initial _accumulator_ value `init` can be provided to the `fold-values` function itself:

```clojure
>> (fold-values + 0.5 linear-series)
7
```

If no _accumulator_ is provided, the first function call will receive the values of the first and
second datapoints in the time series instead.

```clojure
>> (fold-values + linear-series)
6.5
```

You can also use the `values` function, to get all the values from the time series and then fold
that using the regular `fold` function. It will achieve the same effect, but is less efficient:

```clojure
>> (fold + (values linear-series))
6.5
>> (fold + 0.5 (values linear-series))
7
```


