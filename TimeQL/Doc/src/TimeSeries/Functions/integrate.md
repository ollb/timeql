# `integrate`

```clojure
(integrate series)
```

The `integrate` function creates the integral of a time series. It is the reverse operation to
[`derive`](./derive.md).

```clojure
>> [one-series
    (integrate one-series)
    (derive (integrate one-series))]
Timestamp                one      one      one
2022-03-16 22:00:00              0.00
2022-03-17 00:00:00     1.00     2.00     1.00
2022-03-17 02:00:00     1.00     4.00     1.00
2022-03-17 04:00:00     1.00     6.00     1.00
2022-03-17 06:00:00     1.00     8.00     1.00
2022-03-17 08:00:00     1.00    10.00     1.00
2022-03-17 10:00:00     1.00    12.00     1.00
2022-03-17 12:00:00     1.00    14.00     1.00
2022-03-17 14:00:00     1.00    16.00     1.00
2022-03-17 16:00:00     1.00    18.00     1.00
2022-03-17 18:00:00     1.00    20.00     1.00
2022-03-17 20:00:00     1.00    22.00     1.00
2022-03-17 22:00:00     1.00    24.00     1.00
2022-03-18 00:00:00     1.00    26.00     1.00
```

Note that `integrate` requires an aligned series. It will use the resolution to properly place the
starting value `0.00` and to determine the result values in accordance to `derive`. For example the
`2.00` value at `00:00` will be assumed to be an increase over two hours because of the resolution.
Therefore the result at `00:00` will be `1.00`.
