# `window-map`

```clojure
(window-map interval fun series)
```

The `window-map` function will group adjacent values that are within the time range `interval`
into different windows. It will then call the function `fun` for each window and pass in the
sequence of values as the argument. A new time series with the result values will be returned:

```clojure
>> [step-series
    (window-map "4h" sum step-series)
    (window-map "6h" average step-series)]
Timestamp               step     step     step
2022-03-17 00:00:00     0.00     0.00     0.00
2022-03-17 02:00:00     0.00
2022-03-17 04:00:00     0.00     0.00
2022-03-17 06:00:00     0.00              0.00
2022-03-17 08:00:00     0.00     0.00
2022-03-17 10:00:00     0.00
2022-03-17 12:00:00     0.00     1.00     0.67
2022-03-17 14:00:00     1.00
2022-03-17 16:00:00     1.00     2.00
2022-03-17 18:00:00     1.00              1.00
2022-03-17 20:00:00     1.00     2.00
2022-03-17 22:00:00     1.00
2022-03-18 00:00:00     1.00     1.00     1.00
```

Please note that this will usually reduce the amount of datapoints and will therefore also
affect the resolution of the time series.
