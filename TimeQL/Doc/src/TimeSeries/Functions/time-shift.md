﻿# `time-shift`

```clojure
(time-shift interval series)
```

Time-shifts the series by the given time interval. Please note that the function will restore the
series resolution, so time-shifting by an interval below the resolution value will have no effect.

```clojure
>> [step-series
    (time-shift "4h" step-series)
    (time-shift "-4h" step-series)]
Timestamp               step     step     step
2022-03-19 04:00:00                       0.00
2022-03-19 06:00:00                       0.00
2022-03-19 08:00:00     0.00              0.00
2022-03-19 10:00:00     0.00              0.00
2022-03-19 12:00:00     0.00     0.00     0.00
2022-03-19 14:00:00     0.00     0.00     0.00
2022-03-19 16:00:00     0.00     0.00     0.00
2022-03-19 18:00:00     0.00     0.00     1.00
2022-03-19 20:00:00     0.00     0.00     1.00
2022-03-19 22:00:00     1.00     0.00     1.00
2022-03-20 00:00:00     1.00     0.00     1.00
2022-03-20 02:00:00     1.00     1.00     1.00
2022-03-20 04:00:00     1.00     1.00     1.00
2022-03-20 06:00:00     1.00     1.00
2022-03-20 08:00:00     1.00     1.00
2022-03-20 10:00:00              1.00
2022-03-20 12:00:00              1.00
```

_Note_: this will not change the time window that has been queried, so it can lead to missing data
at the start or end of a series. Therefore it is usually recommended, to use the `:shift` option
on the data source, when querying the data instead. See [Data Sources](./../../DataSources.md)
for more details.