# `filter-values`

```clojure
(filter-values fun series)
```

The `filter-values` function can be used to filter out datapoints based on their value. It will
execute the provided function `fun` for every datapoint and only include that datapoint in the
resulting series, if the returned value is truthy.

```clojure
>> [linear-series
    (filter-values #(>= % 0.5) linear-series)]
Timestamp             linear   linear
2022-03-17 00:00:00     0.00
2022-03-17 02:00:00     0.08
2022-03-17 04:00:00     0.17
2022-03-17 06:00:00     0.25
2022-03-17 08:00:00     0.33
2022-03-17 10:00:00     0.42
2022-03-17 12:00:00     0.50     0.50
2022-03-17 14:00:00     0.58     0.58
2022-03-17 16:00:00     0.67     0.67
2022-03-17 18:00:00     0.75     0.75
2022-03-17 20:00:00     0.83     0.83
2022-03-17 22:00:00     0.92     0.92
2022-03-18 00:00:00     1.00     1.00
```
