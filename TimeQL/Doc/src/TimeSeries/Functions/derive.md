# `derive`

```clojure
(derive series)
```

The `derive` function creates the derivative of a time series. It is the reverse operation to
[`integrate`](./integrate.md).

```clojure
>> [linear-series
    (derive linear-series)
    (integrate (derive linear-series))]
Timestamp             linear   linear   linear
2022-03-16 18:00:00     0.00              0.00
2022-03-16 20:00:00     0.08     0.04     0.08
2022-03-16 22:00:00     0.17     0.04     0.17
2022-03-17 00:00:00     0.25     0.04     0.25
2022-03-17 02:00:00     0.33     0.04     0.33
2022-03-17 04:00:00     0.42     0.04     0.42
2022-03-17 06:00:00     0.50     0.04     0.50
2022-03-17 08:00:00     0.58     0.04     0.58
2022-03-17 10:00:00     0.67     0.04     0.67
2022-03-17 12:00:00     0.75     0.04     0.75
2022-03-17 14:00:00     0.83     0.04     0.83
2022-03-17 16:00:00     0.92     0.04     0.92
2022-03-17 18:00:00     1.00     0.04     1.00
```

Note that `derive` requires an aligned series. It will use the resolution to determine where
the time series is continuous and where data is missing. It will also normalize the derivative
to contain values "per-hour". For example the change from `0.00` to `0.08` over two hours
corresponds to `0.04` per hour.

## `per-second`, `per-minute`, `per-hour`

There are convenience functions `per-second`, `per-minute` and `per-hour`, that scale the result
values differently and that can be used to obtain the value change for a different time unit.

They are simply defined as:

```clojure
(defn per-second [series] (/ (derive series) 3600))
(defn per-minute [series] (/ (derive series) 60))
(def per-hour derive)
```