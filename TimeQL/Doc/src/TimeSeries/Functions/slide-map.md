# `slide-map`

```clojure
(slide-map count fun series)
(slide-map interval fun series)
```

The `slide-map` function will map a function `fun` on a _sliding window_. The usual application
is to create a moving average of the last `count` values.

For every datapoint `fun` will receive a sequence of the last `count` values as argument and should
return a single `double` value. This return value will be the value of datapoint in the resulting
time series.

```clojure
>> (average [0 0 1 1 1])
0.6
>> (sum [0 0 1 1 1])
3
>> [step-series
    (slide-map 5 average step-series)
    (slide-map 5 sum step-series)]
Timestamp               step     step     step
2022-03-16 18:00:00     0.00     0.00     0.00
2022-03-16 20:00:00     0.00     0.00     0.00
2022-03-16 22:00:00     0.00     0.00     0.00
2022-03-17 00:00:00     0.00     0.00     0.00
2022-03-17 02:00:00     0.00     0.00     0.00
2022-03-17 04:00:00     0.00     0.00     0.00
2022-03-17 06:00:00     0.00     0.00     0.00
2022-03-17 08:00:00     1.00     0.20     1.00
2022-03-17 10:00:00     1.00     0.40     2.00
2022-03-17 12:00:00     1.00     0.60     3.00
2022-03-17 14:00:00     1.00     0.80     4.00
2022-03-17 16:00:00     1.00     1.00     5.00
2022-03-17 18:00:00     1.00     1.00     5.00
```

Instead of providing the number of datapoints in the _sliding window_, you can also provide a time
interval using the `interval` argument. This is a bit more flexible:

```clojure
>> (slide-map "8h" average step-series)
Timestamp               step
2022-03-16 18:00:00     0.00
2022-03-16 20:00:00     0.00
2022-03-16 22:00:00     0.00
2022-03-17 00:00:00     0.00
2022-03-17 02:00:00     0.00
2022-03-17 04:00:00     0.00
2022-03-17 06:00:00     0.00
2022-03-17 08:00:00     0.20
2022-03-17 10:00:00     0.40
2022-03-17 12:00:00     0.60
2022-03-17 14:00:00     0.80
2022-03-17 16:00:00     1.00
2022-03-17 18:00:00     1.00
```

_Note_: this will not change the time window that has been queried, so values at the beginning
of the time window will have less values in the sliding window. If you want to avoid this, it
is best to use the `:prefetch` option on the data source when querying data, in order to have
some data in front of the actual time window available. See [Data Sources](./../../DataSources.md)
for more details.

## Sliders and performance optimization

```clojure
(slide-map count slider series)
(slide-map interval slider series)
```

The `slide-map` function can be very expensive, if the window contains a lot of datapoints. It is
implemented efficiently for the standard `average`, `median`, `sum` and `product` functions. However
in the generic case it can be inefficient.

One way to solve this is by utilizing a _slider_. The slider is an object that encapsulates an
iterative algorithm for updating the aggregated value. It takes three functions that update a
common state.

- The `add` function adds a value to the current state.
- The `rem` function removes a value from the current state.
- The `get` function produces a value from the current state.

For example the `average` function can be implemented using a slider like this:

```clojure
>> (def avg-slider (slider
    [0 0]
    (fn add [value [sum count]] [(+ sum value) (inc count)])
    (fn rem [value [sum count]] [(- sum value) (dec count)])
    (fn get [[sum count]] (/ sum count))))
<TimeQL.Util.GenericSlider>
>> (slide-map "4h" avg-slider step-series)
Timestamp               step
2022-03-20 12:00:00     0.00
2022-03-20 14:00:00     0.00
2022-03-20 16:00:00     0.00
2022-03-20 18:00:00     0.00
2022-03-20 20:00:00     0.00
2022-03-20 22:00:00     0.00
2022-03-21 00:00:00     0.00
2022-03-21 02:00:00     0.33
2022-03-21 04:00:00     0.67
2022-03-21 06:00:00     1.00
2022-03-21 08:00:00     1.00
2022-03-21 10:00:00     1.00
2022-03-21 12:00:00     1.00
```

The `slide-map` function will accept a slider instead of a function.