# `exp-smooth`

```clojure
(exp-smooth alpha series)
```

The `exp-smooth` function will average the values in the time series using _exponential smoothing_.
For every value it will interpolate between the value and the current "smoothed" value, according to
the following formula:

\\[
\begin{aligned}
s_0 &= v_0 \\\\
s_n &= \alpha * v_n + (1 - \alpha) * s_{n-1}
\end{aligned}
\\]

```clojure
>> [step-series
    (exp-smooth 0.5 step-series)]
Timestamp               step     step
2022-03-17 00:00:00     0.00     0.00
2022-03-17 02:00:00     0.00     0.00
2022-03-17 04:00:00     0.00     0.00
2022-03-17 06:00:00     0.00     0.00
2022-03-17 08:00:00     0.00     0.00
2022-03-17 10:00:00     0.00     0.00
2022-03-17 12:00:00     0.00     0.00
2022-03-17 14:00:00     1.00     0.50
2022-03-17 16:00:00     1.00     0.75
2022-03-17 18:00:00     1.00     0.88
2022-03-17 20:00:00     1.00     0.94
2022-03-17 22:00:00     1.00     0.97
2022-03-18 00:00:00     1.00     0.98
```
