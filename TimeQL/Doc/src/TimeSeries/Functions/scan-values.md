# `scan-values`

```clojure
(scan-values fun series)
```

This will do the same as `fold-values`, but it will return the accumulator value from all function
calls as datapoints in a new time series. This is similar to the `scan` function from Lispery.

The function is very useful to create running totals:

```clojure
>> [linear-series
    (scan-values + linear-series)
    (scan-values + 0.5 linear-series)]
Timestamp             linear   linear   linear
2022-03-16 18:00:00     0.00     0.00     0.50
2022-03-16 20:00:00     0.08     0.08     0.58
2022-03-16 22:00:00     0.17     0.25     0.75
2022-03-17 00:00:00     0.25     0.50     1.00
2022-03-17 02:00:00     0.33     0.83     1.33
2022-03-17 04:00:00     0.42     1.25     1.75
2022-03-17 06:00:00     0.50     1.75     2.25
2022-03-17 08:00:00     0.58     2.33     2.83
2022-03-17 10:00:00     0.67     3.00     3.50
2022-03-17 12:00:00     0.75     3.75     4.25
2022-03-17 14:00:00     0.83     4.58     5.08
2022-03-17 16:00:00     0.92     5.50     6.00
2022-03-17 18:00:00     1.00     6.50     7.00
```

