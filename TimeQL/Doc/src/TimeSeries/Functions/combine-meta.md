﻿# `combine-meta`

```clojure
(combine-meta key seq series)
```

Combines metadata in the same way as the [`meta`](../Metadata.md) selector function and then
stores the result with a new matadata key.

```clojure
>> (meta sin-series)
{:type :trigonometric :function "f(x) = sin(x)" :name "sin"}
>> (meta (combine-meta :combined ["[" :name "] " :function] sin-series))
{:function "f(x) = sin(x)" :type :trigonometric :combined "[sin] f(x) = sin(x)" :name "sin"}
```
