﻿# Generating Series

TimeQL provides a few methods to create or generate time series.

## `gen-series`

```clojure
(gen-series fun opts...)
(gen-series name fun opts...)
```

The `gen-series` function is very useful, to quickly generate time series data from a function.
The time range and number of samples created by the `gen-series` function are derived from the
environment. See [REPL](../REPL.md) for more details on this.

The basic usage looks something like this:

```clojure
>> (gen-series identity)
Timestamp           identity
2022-03-10 22:00:00     0.00
2022-03-11 00:00:00     0.08
2022-03-11 02:00:00     0.17
2022-03-11 04:00:00     0.25
2022-03-11 06:00:00     0.33
2022-03-11 08:00:00     0.42
2022-03-11 10:00:00     0.50
2022-03-11 12:00:00     0.58
2022-03-11 14:00:00     0.67
2022-03-11 16:00:00     0.75
2022-03-11 18:00:00     0.83
2022-03-11 20:00:00     0.92
2022-03-11 22:00:00     1.00
```

The name of the time series is automatically derived from the passed-in function, if possible.
It can also be provided as the first argument, if necessary. The input values to the function
range from `0.00` to `1.00`. The `identity` function will just return whatever is passed in,
so the values of the datapoints will also increase from `0.00` to `1.00`.

### Input option

The argument to the generator function is based on the position in the time window. The behavior
can be changed by setting the `:input` option. Supported options are:

| Input value | Description |
|:----------- |:----------- |
| `:fraction` | A value between `0.00` and `1.00` (default). |
| `:pi`       | A value between `0.00` and `math.pi`. |
| `:pi2`      | A value between `0.00` and `(* math.pi 2)`. |
| `:days`     | The amount of days from the start of the window. |
| `:hours`    | The amount of hours from the start of the window. |
| `:minutes`  | The amount of minutes from the start of the window. |
| `:seconds`  | The amount of seconds from the start of the window. |

For example `:pi2` can be very useful for trigonometric functions:

```clojure
>> [(gen-series sin :input :pi2) (gen-series cos :input :pi2)]
Timestamp                sin      cos
2022-03-11 02:00:00     0.00     1.00
2022-03-11 04:00:00     0.50     0.87
2022-03-11 06:00:00     0.87     0.50
2022-03-11 08:00:00     1.00     0.00
2022-03-11 10:00:00     0.87    -0.50
2022-03-11 12:00:00     0.50    -0.87
2022-03-11 14:00:00     0.00    -1.00
2022-03-11 16:00:00    -0.50    -0.87
2022-03-11 18:00:00    -0.87    -0.50
2022-03-11 20:00:00    -1.00    -0.00
2022-03-11 22:00:00    -0.87     0.50
2022-03-12 00:00:00    -0.50     0.87
2022-03-12 02:00:00    -0.00     1.00
```

## `series`

```clojure
(series name data)
(series name data meta)
```

You can also create series from explicit datapoints using the `series` function:

```clojure
>> (def data {*window-start* 0 *window-stop* 1})
{<03/16/2022 20:08:33> 0 <03/17/2022 20:08:33> 1}
>> (series "test" data)
Timestamp               test
2022-03-16 20:08:33     0.00
2022-03-17 20:08:33     1.00
```

Metadata can be given using the third argument:

```clojure
>> (def test-series (series "test" data {:test 5}))
Timestamp               test
2022-03-16 20:08:33     0.00
2022-03-17 20:08:33     1.00
>> (meta test-series)
{:test 5}
```
