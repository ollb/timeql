# Arithmetics

Time series natively support a variety of arithmetical operations with `double` values. For
example, you can add or subtract values from time series:

```clojure
>> [linear-series
    (set-name "a" (+ linear-series 5))
    (set-name "b" (- 1 linear-series))
    (set-name "c" (- linear-series))
    (set-name "d" (- (* 2 linear-series) 5))]
Timestamp             linear        a        b        c        d
2022-03-16 18:00:00     0.00     5.00     1.00    -0.00    -5.00
2022-03-16 20:00:00     0.08     5.08     0.92    -0.08    -4.83
2022-03-16 22:00:00     0.17     5.17     0.83    -0.17    -4.67
2022-03-17 00:00:00     0.25     5.25     0.75    -0.25    -4.50
2022-03-17 02:00:00     0.33     5.33     0.67    -0.33    -4.33
2022-03-17 04:00:00     0.42     5.42     0.58    -0.42    -4.17
2022-03-17 06:00:00     0.50     5.50     0.50    -0.50    -4.00
2022-03-17 08:00:00     0.58     5.58     0.42    -0.58    -3.83
2022-03-17 10:00:00     0.67     5.67     0.33    -0.67    -3.67
2022-03-17 12:00:00     0.75     5.75     0.25    -0.75    -3.50
2022-03-17 14:00:00     0.83     5.83     0.17    -0.83    -3.33
2022-03-17 16:00:00     0.92     5.92     0.08    -0.92    -3.17
2022-03-17 18:00:00     1.00     6.00     0.00    -1.00    -3.00
```

Please note that the time series will retain their metadata and the name:

```clojure
>> (name (+ linear-series 10))
"linear"
>> (meta (+ linear-series 10))
{:type :polynomial :function "f(x) = x" :name "linear"}
```
