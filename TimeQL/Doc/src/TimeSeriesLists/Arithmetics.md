﻿# Arithmetics

Arithmetical operators are extended to work on time series lists. Operations between time series
lists and `double` values will just apply the operation to all series and values:

```clojure
>> (* 2 trig-list)
Timestamp                sin      cos
2022-03-16 20:00:00     0.00     2.00
2022-03-16 22:00:00     1.00     1.73
2022-03-17 00:00:00     1.73     1.00
2022-03-17 02:00:00     2.00     0.00
2022-03-17 04:00:00     1.73    -1.00
2022-03-17 06:00:00     1.00    -1.73
2022-03-17 08:00:00     0.00    -2.00
2022-03-17 10:00:00    -1.00    -1.73
2022-03-17 12:00:00    -1.73    -1.00
2022-03-17 14:00:00    -2.00    -0.00
2022-03-17 16:00:00    -1.73     1.00
2022-03-17 18:00:00    -1.00     1.73
2022-03-17 20:00:00    -0.00     2.00
```

Operations between time series lists and regular time series will perform an inner join between
the time series and all time series in the list:

```clojure
>> (* step-series trig-list)
Timestamp             result   result
2022-03-17 06:00:00     0.00     0.00
2022-03-17 08:00:00     0.00     0.00
2022-03-17 10:00:00     0.00     0.00
2022-03-17 12:00:00     0.00     0.00
2022-03-17 14:00:00     0.00    -0.00
2022-03-17 16:00:00     0.00    -0.00
2022-03-17 18:00:00     0.00    -0.00
2022-03-17 20:00:00    -0.50    -0.87
2022-03-17 22:00:00    -0.87    -0.50
2022-03-18 00:00:00    -1.00    -0.00
2022-03-18 02:00:00    -0.87     0.50
2022-03-18 04:00:00    -0.50     0.87
2022-03-18 06:00:00    -0.00     1.00
```
