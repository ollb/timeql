﻿# Grouping

Time series in a time series list can be grouped and processed with a few utility functions.

## `group`

```clojure
(group key fun list)
```

The `group` function will group together all time series that have the same value for the metadata
selected by `key`. It will then call `fun` for each group, with group passed in as the argument to
the function. `fun` needs to collapse the group into a single time series that is then returned as
a "representative" of the group and renamed to the value of the selected metadata.

The series in the `const-list` series list have some extended metadata to demonstrate this:

```clojure
>> (meta zero-series)
{:even true :odd false :value 0 :type :constant :name "zero" :function "f(x) = 0"}
```

For example to select the first series in the list with even or odd value, you can run:

```clojure
>> (group :even first const-list)
Timestamp              False     True
2022-03-22 08:00:00     1.00     0.00
2022-03-22 10:00:00     1.00     0.00
2022-03-22 12:00:00     1.00     0.00
2022-03-22 14:00:00     1.00     0.00
2022-03-22 16:00:00     1.00     0.00
2022-03-22 18:00:00     1.00     0.00
2022-03-22 20:00:00     1.00     0.00
2022-03-22 22:00:00     1.00     0.00
2022-03-23 00:00:00     1.00     0.00
2022-03-23 02:00:00     1.00     0.00
2022-03-23 04:00:00     1.00     0.00
2022-03-23 06:00:00     1.00     0.00
2022-03-23 08:00:00     1.00     0.00
```

## `group-join`

```clojure
(group-join key fun list)
```

The usual application is to perform a `join` on all time series in a group. For example we can add
the even and odd series in the list together:

```clojure
>> (group-join :even sum const-list)
Timestamp              False     True
2022-03-22 08:00:00     3.00     2.00
2022-03-22 10:00:00     3.00     2.00
2022-03-22 12:00:00     3.00     2.00
2022-03-22 14:00:00     3.00     2.00
2022-03-22 16:00:00     3.00     2.00
2022-03-22 18:00:00     3.00     2.00
2022-03-22 20:00:00     3.00     2.00
2022-03-22 22:00:00     3.00     2.00
2022-03-23 00:00:00     3.00     2.00
2022-03-23 02:00:00     3.00     2.00
2022-03-23 04:00:00     3.00     2.00
2022-03-23 06:00:00     3.00     2.00
2022-03-23 08:00:00     3.00     2.00
```

This is very useful for data that can be grouped on data at different resolutions. For example imagine
that you have some geo-related data that you would like to group on different scales (continent, country,
region, city etc.).

The `group-join` can also perform an outer join, by providing an additional argument.

## Function selector

```clojure
(group sel-fun fun list)
(group-join sel-fun fun list)
```

It is also possible to group series using a selector function. All series for which the function
returns the same value are placed into the same group.

## Combined metadata selector

```clojure
(group seq fun list)
(group-join seq fun list)
```

Similar to the [`meta`](./../TimeSeries/Metadata.md) function that allows for combining metadata,
the same mechanism can be used on grouping:

```clojure
>> (group [:value " (" :even ")"] first const-list)
Timestamp           0 (True) 1 (False) 2 (True) 3 (False)
2022-03-22 08:00:00     0.00      1.00     2.00      2.00
2022-03-22 10:00:00     0.00      1.00     2.00      2.00
2022-03-22 12:00:00     0.00      1.00     2.00      2.00
2022-03-22 14:00:00     0.00      1.00     2.00      2.00
2022-03-22 16:00:00     0.00      1.00     2.00      2.00
2022-03-22 18:00:00     0.00      1.00     2.00      2.00
2022-03-22 20:00:00     0.00      1.00     2.00      2.00
2022-03-22 22:00:00     0.00      1.00     2.00      2.00
2022-03-23 00:00:00     0.00      1.00     2.00      2.00
2022-03-23 02:00:00     0.00      1.00     2.00      2.00
2022-03-23 04:00:00     0.00      1.00     2.00      2.00
2022-03-23 06:00:00     0.00      1.00     2.00      2.00
2022-03-23 08:00:00     0.00      1.00     2.00      2.00
```
