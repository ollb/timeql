﻿# Functions

Most functions that work on time series will also work on time series lists. They will just apply
to all time series in the time series list individually. For example:

```clojure
>> (derive poly-list)
Timestamp             linear   square     cube
2022-03-17 08:00:00     0.04     0.00     0.00
2022-03-17 10:00:00     0.04     0.01     0.00
2022-03-17 12:00:00     0.04     0.02     0.01
2022-03-17 14:00:00     0.04     0.02     0.01
2022-03-17 16:00:00     0.04     0.03     0.02
2022-03-17 18:00:00     0.04     0.04     0.03
2022-03-17 20:00:00     0.04     0.05     0.04
2022-03-17 22:00:00     0.04     0.05     0.05
2022-03-18 00:00:00     0.04     0.06     0.06
2022-03-18 02:00:00     0.04     0.07     0.08
2022-03-18 04:00:00     0.04     0.07     0.10
2022-03-18 06:00:00     0.04     0.08     0.11
```

This is even true for functions that would regularly just produce single values:

```clojure
>> (fold-values + poly-list)
<NamedValueList ["linear" 6.50] ["square" 4.51] ["cube" 3.52]>
```

Please note that in those cases, a _named value list_ is returned. This will retain the name and
metadata information.