﻿# Filtering

The time series in a series list can be filtered based on their metadata using the
`filter-meta` function. The filter expression can be quite extensive. Here are some
examples:

```clojure
; Return the series with the name "cos".
>> (filter-meta "cos" example-list)

; Return the series with type :trigonometric.
>> (filter-meta [:type :trigonometric] example-list)

; Return the series that do not have the type :trigonometric.
>> (filter-meta [:not :type :trigonometric] example-list)

; Return series based on the name matching a regular expression.
>> (filter-meta #"sin|cos" example-list)

; Return series based on a selector function.
>> (filter-meta #(= (name %) "cos") example-list)

; Return series with type :trigonometric that don't match the name "sin".
>> (filter-meta [:and [:not "sin"] [:type :trigonometric]] example-list)
```

## Filter expressions

| Expression   | Description |
|:----------   |:----------- |
| `(fn ...)`   | Calls the function with the series as argument. Keeps the series on truthy results. |
| `#"regex"`   | Matches the _current value_ against the given regular expression. |
| `value`      | Matches the _current value_ against the given value. |
| `[:key ...]` | Sets the _current value_ to the metadata for `:key`. |
| `[:not ...]` | Negates any following filter expression. |
| `[:and ...]` | Combines the following filter expressions with a logical and. |
| `[:or ...]`  | Combines the following filter expressions with a logical or. |

If the _current value_ has not been changed, it refers to the name of the series.
