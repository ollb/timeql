﻿# Metadata

Accessing metadata with the `meta` function will also work on time series lists. However
the function will only return metadata that is the same for all series contained in the
time series list. For example for `example-list` it will be empty, because all series have
different metadata.

```clojure
>> (meta a)
{}
```

However for the trigonometric series list `trig-list`, it will return the type:

```clojure
>> (meta trig-list)
{:type :trigonometric}
>> (meta :type trig-list)
:trigonometric
```
