# Time Series

A _time series_ is a series of datapoints with a _name_, _metadata_ and a _resolution_. Each
datapoint consists of a _timestamp_ and a `double` _value_. When the REPL prints a time series,
it will show the values for each datapoint in a table:

```clojure
>> (load-example-data)
[sin-series cos-series rand-series zero-series one-series two-series three-series
linear-series square-series cube-series step-series trig-list const-list poly-list
example-list]
>> sin-series
Timestamp                sin
2022-03-16 18:00:00     0.00
2022-03-16 20:00:00     0.50
2022-03-16 22:00:00     0.87
2022-03-17 00:00:00     1.00
2022-03-17 02:00:00     0.87
2022-03-17 04:00:00     0.50
2022-03-17 06:00:00     0.00
2022-03-17 08:00:00    -0.50
2022-03-17 10:00:00    -0.87
2022-03-17 12:00:00    -1.00
2022-03-17 14:00:00    -0.87
2022-03-17 16:00:00    -0.50
2022-03-17 18:00:00    -0.00
```

The name of the time series will also be shown as the heading of the column. When querying data
from Grafana, the name will also be in the Graph legend.

You can query the additional properties using the `name`, `meta` and `resolution` functions:

```clojure
>> (name sin-series)
"sin"
>> (meta sin-series)
{:type :trigonometric :name "sin" :function "f(x) = sin(x)"}
>> (resolution sin-series)
<02:00:00>
```
