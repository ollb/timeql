# Installation

TimeQL can consists of a server backend providing an API that is for example queried by the Grafana
plugin and a REPL command-line utility for running queries interactively on the console.

At the moment you can either download the binary packages and run them using `dotnet` (this requires
the installation of [.NET Core 6.0](https://docs.microsoft.com/en-us/dotnet/core/install/linux)) or
run the application using docker.

## Using the binary packages

Running the REPL and server is currently the same procedure. There is no systemd service at the
moment, but a unit file should be easy to create. Apart from that, just should be able to just
download, unpack and run the application (check the [releases page](https://gitlab.com/ollb/timeql/-/releases)
for the download links):

```bash
wget https://gitlab.com/api/v4/projects/34177257/packages/generic/TimeQLREPL/[[:VERSION:]]/TimeQLREPL-[[:VERSION:]].tar.gz
tar xzf TimeQLREPL-[[:VERSION:]].tar.gz
cd TimeQLREPL-[[:VERSION:]]
dotnet TimeQLREPL.dll
```

The procedure is the same for the TimeQLServer application.

## Using docker

To start a container with the REPL you can run:

```bash
docker run -it registry.gitlab.com/ollb/timeql/timeql-repl:[[:VERSION:]]
```

To start a detached container with the server on the specified port:

```bash
docker run -p [PORT]:5000 -d registry.gitlab.com/ollb/timeql/timeql-server:[[:VERSION:]]
```

## Configuration

Both the REPL and the server will start without any configuration. However in that state you will
not be able to query any data sources. The programs will search for a configuration file in the
following locations (in order):

- `~/.timeql/config.json`
- `/etc/timeql/config.json`

The first config file found will be used. If you are using docker, you can override the configuration
file using `-v`, like so:

```bash
docker run -p [PORT]:5000 -v [CFGFILE]:/etc/timeql/config.json -d registry.gitlab.com/ollb/timeql/timeql-server:[[:VERSION:]]
```

_Note_: if you run TimeQL on Windows, the configuration is searched for in:

- `%APPDATA%\TimeQL\config.json`
- `%PROGRAMDATA%\TimeQL\config.json`

## Example configuration

This is an example configuration that shows the most important settings:

```json
{
	// The hostname and port for the server. With docker, the host needs to be "*", or the docker
	// proxy will be unable to forward the port to the server.
	"host": "*",
	"port": 5000,

	// The maximum number of worker processes.
	"worker-limit": 4,

	// The maximum number of evaluation steps that are allowed for a query in the server. This is
	// intended to interrupt non-terminating queries and prevent them from overloading the server.
	"evaluation-step-limit": 1000000,

	// The compression algorithm to use for cached values. Can be "zstd", "gzip" or "none".
	"cache-compression": "zstd",

	// The compression level to use for the cache.
	"cache-compression-level": 1,

	// The maximum time in minutes that values can be cached for.
	"cache-max-ttl-interval": 60,

	// The minimum time between cache cleanups in minutes.
	"cache-cleanup-interval": 15,

	// The endpoint configurations for different data sources. Note that the names should be unique
	// and that the _first_ endpoint for each source will be used by default when querying that data
	// source. If you don't need a data source, just omit its configuration.
	"influx-endpoints": [
		{
			"name":           "influx",
			"uri":            "http://localhost:8086",
			"organization":   "...",
			"default-bucket": "...",
			"api-token":      "..."
		}
	],

	"graphite-endpoints": [
		{
			"name": "graphite",
			"uri":  "http://localhost:8542"
		}
	],

	"cloudwatch-endpoints": [
		{
			"name":              "cloudwatch",
			"default-region":    "eu-central-1",
			"access-key":        "...",
			"secret-access-key": "..."
		}
	],

	// Include files are loaded prior to every query that is being run. They can be used to define
	// additional functions or symbols that you always want to have available. Please note that
	// execution fails, if one of the files is not available. Paths should be absolute. There is
	// no expansion of `~` or variables.
	"includes": [
		"/etc/timeql/init.tql"
	]
}
```

