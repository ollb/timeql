# TimeQL

TimeQL is a query language for querying, combining and filtering time series data. It is based
on [Lispery](https://gitlab.com/ollb/lispery), which is an interpreted LISP-like programming
language.

For using TimeQL you mainly need to know the function call syntax and basic types of Lispery.
For reference you can always check the documentation at
[https://ollb.gitlab.io/lispery/](https://ollb.gitlab.io/lispery/).

It is intended to run on top of an existing time series database, such as InfluxDB or Graphite
and below a dashboard or visualization application such as Grafana.

There is also a [Plugin for Grafana](https://gitlab.com/ollb/timeql-grafana-plugin), that will
add TimeQL as a data source.
