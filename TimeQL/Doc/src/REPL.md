# REPL

TimeQL comes with a command-line REPL utility (read, eval, print loop) that can be used to test out
the language or to experiment. It allows the user to directly enter queries on the command line and
immediately see the resulting values.

Examples in this documentation will be based on the REPL, but can usally also be executed directly
from Grafana or from the Server API. However this is only the case, when the query returns a value
that can be visualized as a graph.

## The environment

Queries in TimeQL are executed in an environment that always has a few special symbols defined.
Those symbols are:

| Symbol           | Description |
|:---------------- |:----------- |
| `*window-start*` | The start time of the _time window_ that is being queried. |
| `*window-stop*`  | The stop time of the _time window_ that is being queried. |
| `*window-size*`  | The size of the _time window_ (stop minus start). |
| `*resolution*`   | The requested time between individual _samples_. |

You can have a look at these symbols on the REPL:

```clojure
>> *resolution*
<02:00:00>
>> *window-start*
<03/10/2022 21:24:06>
```

When running queries from Grafana, those variables are set based on the currently selected time
window and the number of data points that have been selected. The REPL will automatically set 
`*window-start*`, `*window-stop*` and `*resolution*` for each query, based on the current values
of the `*window-size*` and `*sample-count*` symbols.

By default `*window-size*` is set to one day and `*sample-count*` to 12, resulting in a one-day
window ending at the current time, with one sample every two hours. If you change those two
variables, all the other values will change accordingly.
