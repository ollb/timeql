﻿# Data Sources

TimeQL supports querying several different data sources. Data is usually queried by calling a
function that will return a time series list. How exactly the data is translated depends a bit
on the data source in question.