﻿# Named Values

A _named value_ is similar to a time series in that it associates a value with a name and additional
metadata. It can be constructed using the `named-value` function:

```clojure
>> (named-value "test" 1.5)
<NamedValue "test" 1.50>
```

Metadata can also be attached:

```clojure
>> (def a (named-value "test" 1.5 {:test 5}))
<NamedValue "test" 1.50>
>> (meta a)
{:test 5}
```

Also similar to series lists, a _named value list_ groups several named values:

```clojure
>> (named-value-list (named-value "a" 1) (named-value "b" 2))
<NamedValueList ["a" 1.00] ["b" 2.00]>
```

## `fold-values` and `reduce-values`

Those named values are useful, when extracting single values from a time series list:

```clojure
>> (fold-values + poly-list)
<NamedValueList ["linear" 6.50] ["square" 4.51] ["cube" 3.52]>
```

There is also a generalization `reduce-values` of folding, which passes _all_ values of
each series into a function and expects a `double` result.

```clojure
>> (reduce-values #(fold + %) poly-list)
<NamedValueList ["linear" 6.50] ["square" 4.51] ["cube" 3.52]>
>> (reduce-values sum poly-list)
<NamedValueList ["linear" 6.50] ["square" 4.51] ["cube" 3.52]>
>> (reduce-values average poly-list)
<NamedValueList ["linear" 0.50] ["square" 0.35] ["cube" 0.27]>
```
## Arithmetics

Arithmetics between `double` values and named value lists are supported:

```clojure
>> (def a (reduce-values sum poly-list))
<NamedValueList ["linear" 6.50] ["square" 4.51] ["cube" 3.52]>
>> (def b (+ 10 a))
<NamedValueList ["linear" 16.50] ["square" 14.51] ["cube" 13.52]>
```

The same is true for arithmetics between time series lists and named value lists:

```clojure
>> (- poly-list (reduce-values average poly-list))
Timestamp             linear   square     cube
2022-03-16 20:00:00    -0.50    -0.35    -0.27
2022-03-16 22:00:00    -0.42    -0.34    -0.27
2022-03-17 00:00:00    -0.33    -0.32    -0.27
2022-03-17 02:00:00    -0.25    -0.28    -0.26
2022-03-17 04:00:00    -0.17    -0.24    -0.23
2022-03-17 06:00:00    -0.08    -0.17    -0.20
2022-03-17 08:00:00     0.00    -0.10    -0.15
2022-03-17 10:00:00     0.08    -0.01    -0.07
2022-03-17 12:00:00     0.17     0.10     0.03
2022-03-17 14:00:00     0.25     0.22     0.15
2022-03-17 16:00:00     0.33     0.35     0.31
2022-03-17 18:00:00     0.42     0.49     0.50
2022-03-17 20:00:00     0.50     0.65     0.73
```

## First and last values

Take the following series list as an example:

```clojure
>> (def a (series-list sin-series (filter-values #(>= 0.2 %) cos-series)))
Timestamp                sin      cos
2022-03-16 20:00:00     0.00
2022-03-16 22:00:00     0.50
2022-03-17 00:00:00     0.87
2022-03-17 02:00:00     1.00     0.00
2022-03-17 04:00:00     0.87    -0.50
2022-03-17 06:00:00     0.50    -0.87
2022-03-17 08:00:00     0.00    -1.00
2022-03-17 10:00:00    -0.50    -0.87
2022-03-17 12:00:00    -0.87    -0.50
2022-03-17 14:00:00    -1.00    -0.00
2022-03-17 16:00:00    -0.87
2022-03-17 18:00:00    -0.50
2022-03-17 20:00:00    -0.00
```

You can easily extract the first and last values from each series individually:

```clojure
>> (reduce-values first a)
<NamedValueList ["sin" 0.00] ["cos" 0.00]>
>> (reduce-values last a)
<NamedValueList ["sin" -0.00] ["cos" -0.00]>
```

Note that the values come from different timestamps, because of the filtering. If you want to
avoid this behavior, you can use the `first-value-set` and `last-value-set` functions, which will
return the first and last _set_ of values instead (corresponding to the first and last row in the
table):

```clojure
>> (first-value a)
<NamedValueList ["sin" 0.00]>
>> (last-value a)
<NamedValueList ["sin" -0.00]>
```
