﻿# Example Data

For the purpose of easy discoverability, TimeQL comes with some example data that can be loaded in.
This allows us to have a look at TimeQL and the concepts without needing to dive into querying different
data sources immediately.

Loading the example data is done using the `load-example-data` function:

```clojure
>> (load-example-data)
[sin-series cos-series rand-series zero-series one-series two-series three-series
linear-series square-series cube-series step-series trig-list const-list poly-list
example-list]
```

The function returns a list of all symbols that are defined by it. Let's have a look at the `example-list`
_time series list_, that contains all of the newly defined _time series_:

```clojure
>> example-list
Timestamp                sin      cos     rand     zero      one      two   linear   square     cube     step
2022-03-16 18:00:00     0.00     1.00     0.19     0.00     1.00     2.00     0.00     0.00     0.00     0.00
2022-03-16 20:00:00     0.50     0.87     0.52     0.00     1.00     2.00     0.08     0.01     0.00     0.00
2022-03-16 22:00:00     0.87     0.50     0.40     0.00     1.00     2.00     0.17     0.03     0.00     0.00
2022-03-17 00:00:00     1.00     0.00     0.31     0.00     1.00     2.00     0.25     0.06     0.02     0.00
2022-03-17 02:00:00     0.87    -0.50     0.55     0.00     1.00     2.00     0.33     0.11     0.04     0.00
2022-03-17 04:00:00     0.50    -0.87     0.91     0.00     1.00     2.00     0.42     0.17     0.07     0.00
2022-03-17 06:00:00     0.00    -1.00     1.00     0.00     1.00     2.00     0.50     0.25     0.13     0.00
2022-03-17 08:00:00    -0.50    -0.87     0.71     0.00     1.00     2.00     0.58     0.34     0.20     1.00
2022-03-17 10:00:00    -0.87    -0.50     0.81     0.00     1.00     2.00     0.67     0.44     0.30     1.00
2022-03-17 12:00:00    -1.00    -0.00     0.77     0.00     1.00     2.00     0.75     0.56     0.42     1.00
2022-03-17 14:00:00    -0.87     0.50     0.32     0.00     1.00     2.00     0.83     0.69     0.58     1.00
2022-03-17 16:00:00    -0.50     0.87     0.24     0.00     1.00     2.00     0.92     0.84     0.77     1.00
2022-03-17 18:00:00    -0.00     1.00     0.40     0.00     1.00     2.00     1.00     1.00     1.00     1.00
```

As you can see, the REPL will by default print out time series and time series lists as a table. If you
want to have a look at the visual representation, you can also run the following query in Grafana:

```clojure
(load-example-data)
example-list
```

The remaining documentation will assume, that the example data has already been loaded.
