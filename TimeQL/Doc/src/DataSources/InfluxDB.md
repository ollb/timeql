﻿# InfluxDB

```clojure
(influx-query measurement options...)
```

Queries data from the given measurement. This will select everything in the measurement, regardless
of the `_field` or tags. Usually you will want to pair this with the `:field` option to only select
data for the given field:

```clojure
(influx-query measurement :field field-name options...)
```

Data from the tags is mapped to metadata and can subsequently be used to filter or group the selected
series data. To keep with the datacenter example from the Graphite documentation, this could look
something like this:

```clojure
(def data (influx-query "servers" :field "load"))
```

Producing the following metadata:

| Key           | Value |
|:------------- |:----- |
| `:datacenter` | `"dc-lon-01"` |
| `:server`     | `"node-02"` |
| `:duration`   | `"1"` |

You can then select the data you actually need with `filter-meta`:

```clojure
(filter-meta [:and [:datacenter "dc-lon-01"] [:duration "1"]] data)
```

## Flux queries

There is currently no support for running more complicated Flux queries. Some support for filtering
by tags directly is planned, in order to reduce the amount of data that needs to be fetched from the
database.

## Options

The query supports the following options:

| Option                    | Description |
|:------------------------- |:----------- |
| `:shift interval`         | Time-shift the results by the given `interval`. |
| `:resolution interval`    | Overrides the time resolution. |
| `:window-start timestamp` | Overrides the start of the time window. |
| `:window-stop timestamp`  | Overrides the stop of the time window. |
| `:prefetch interval`      | Decreases the start of the time window by `interval`. |
| `:endpoint name`          | Selects a different endpoint from the configuration. |
| `:bucket name`            | Selects the bucket to query data from. |
| `:field name`             | Determines the field to select. |
| `:name expr`              | Sets the name of the series according to `expr` (like `set-name`). |

## Configuration

```json
{
	"name":           "influx",
	"uri":            "http://localhost:8086",
	"organization":   "...",
	"default-bucket": "...",
	"api-token":      "..."
}
```

Multiple endpoints need to have different names, so that they can be selected using the `:endpoint`
option. The `uri` specifies the address of the InfluxDB server. Please note that the organization
can currently not be overwritten by options. If you need multiple organizations, you can create
more than one endpoint instead.
