﻿# Prometheus

```clojure
(prometheus-query query options...)
```

The `query` supports any prometheus query. The labels that are contained in the result set will
be mapped to TimeQL metadata.

## Options

The query supports the following options:

| Option                     | Description |
|:-------------------------- |:----------- |
| `:shift interval`          | Time-shift the results by the given `interval`. |
| `:resolution interval`     | Overrides the time resolution. |
| `:max-resolution interval` | Overrides the maximum resolution. |
| `:window-start timestamp`  | Overrides the start of the time window. |
| `:window-stop timestamp`   | Overrides the stop of the time window. |
| `:prefetch interval`       | Decreases the start of the time window by `interval`. |
| `:endpoint name`           | Selects a different endpoint from the configuration. |
| `:name expr`               | Sets the name of the series according to `expr` (like `set-name`). |

## Resolution

Prometheus will provide data with the resolution that is being requested, regardless of how many data
points there actually are. This can be problematic, for example when deriving data, because it will
produce multiple data points with the same value.

This can be prevented by either specifying the `:max-resolution` option or by defining the
`*max-resolution*` symbol and setting it to the scrape interval for the metric in question. If done
properly, TimeQL will not request a higher resolution than the one that is actually available.

## Configuration

```json
{
	"name": "prometheus",
	"uri":  "http://localhost:9090"
}
```

This should be pretty self-explanatory. Multiple endpoints need to have different names, so that
they can be selected using the `:endpoint` option. The `uri` specifies the address of the Prometheus
server API.

## Examples

Node exporter CPU usage in percent, averaged by type.

```clojure
(assign! *max-resolution* "15s")
(def data-per-second (per-second (prometheus-query "node_cpu_seconds_total")))
(filter-meta [:not :mode "idle"] (group-join :mode average data-per-second))
```
