﻿# Graphite

```clojure
(graphite-query query options...)
```

For `query` you can provide the name of a series with optional wildcards. For example it could be
something like this:

- `dc-lon-01.servers.node-02.load.1_min`
- `dc-lon-0{1,2,3}.servers.node-02.load.1_min`
- `dc-lon-0{1,2,3}.servers.node-02.load.*`

The query will return a time series list containing all the selected series. By default, the
metadata contains one record for each _node_ in the series name. For example on the first query,
you will get:

| Key       | Value |
|:--------- |:----- |
| `:node-0` | `"dc-lon-01"` |
| `:node-1` | `"servers"` |
| `:node-2` | `"node-02"` |
| `:node-3` | `"load"` |
| `:node-4` | `"1_min"` |

## Schema

This can be adapted, by providing a _schema_ in the options. For example:

```clojure
(graphite-query "dc-lon-01.servers.node-02.load.1_min" :schema "datacenter._.server._.interval")
```

This will result in the following metadata:

| Key           | Value |
|:------------- |:----- |
| `:datacenter` | `"dc-lon-01"` |
| `:server`     | `"node-02"` |
| `:interval`   | `"1_min"` |

Any nodes that have a corresponding `_` in the schema are ignored. This will of course also work
with multiple series.

## Graphite queries

There is currently no support for runnig more complicated Graphite queries, because it would break
the metadata mapping.

## Graphite tags

Support for Graphite tags is still missing.

## Options

The query supports the following options:

| Option                    | Description |
|:------------------------- |:----------- |
| `:shift interval`         | Time-shift the results by the given `interval`. |
| `:resolution interval`    | Overrides the time resolution. |
| `:window-start timestamp` | Overrides the start of the time window. |
| `:window-stop timestamp`  | Overrides the stop of the time window. |
| `:prefetch interval`      | Decreases the start of the time window by `interval`. |
| `:endpoint name`          | Selects a different endpoint from the configuration. |
| `:schema schema`          | Determines how series names are mapped to metadata. |
| `:name expr`              | Sets the name of the series according to `expr` (like `set-name`). |

## Configuration

```json
{
	"name": "graphite",
	"uri":  "http://localhost:8542"
}
```

This should be pretty self-explanatory. Multiple endpoints need to have different names, so that
they can be selected using the `:endpoint` option. The `uri` specifies the address of the Graphite
server API.

## Examples

Average the 1-minute load of all servers, grouped by datacenter:

```clojure
(def data (graphite-query "dc-*.servers.*.load.1_min" :schema "datacenter._.server._.interval"))
(group-join :datacenter average data)
```

Take the 1-minute load in `dc-lon-01` and create a sliding average over 6 hours:

```clojure
(def data (graphite-query "dc-lon-01.servers.*.load.1_min" :schema "datacenter._.server._.interval"))
(slide-map "6h" average data)
```
