﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Influx
{
	public class InfluxFetchedEventArgs : EventArgs
	{
		public InfluxResult Result { get; init; }

		internal InfluxFetchedEventArgs(InfluxResult result)
		{
			Result = result;
		}
	}
}
