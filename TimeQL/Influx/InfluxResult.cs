﻿using Lispery.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Values;

namespace TimeQL.Influx
{
	public class InfluxResult
	{
		/// <summary>Time spent checking the cache.</summary>
		public TimeSpan CacheTime { get; init; }

		/// <summary>Time spent executing the query remotely.</summary>
		public TimeSpan ExecuteTime { get; init; }

		/// <summary>Time spent parsing the query results.</summary>
		public TimeSpan ParseTime { get; init; }

		/// <summary>The resulting time series list.</summary>
		public TimeSeriesList SeriesList { get; init; }

		/// <summary>
		/// Creates a new influx result.
		/// </summary>
		internal InfluxResult(TimeSpan cacheTime, TimeSpan execTime, TimeSpan parseTime, TimeSeriesList seriesList)
		{
			CacheTime = cacheTime;
			ExecuteTime = execTime;
			ParseTime = parseTime;
			SeriesList = seriesList;
		}
	}
}
