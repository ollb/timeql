﻿using Lispery;
using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Cloudwatch;
using TimeQL.Influx;
using TimeQL.Util;

namespace TimeQL.Influx
{
	public class InfluxLib
	{
		private readonly Keyword kwBucket;
		private readonly Keyword kwField;
		private readonly Keyword kwGroup;
		private readonly Keyword kwLabel;
		private readonly Keyword kwId;

		private readonly KnownIdentifiers ki;
		private readonly Lispery.Environment env;
		private readonly InfluxClient client;

		public event EventHandler<InfluxFetchedEventArgs>? Fetched;

		public InfluxLib(Config config, QueryCache queryCache, KnownIdentifiers ki, Lispery.Environment env)
		{
			this.ki = ki;
			this.env = env;
			this.client = new InfluxClient(config, queryCache);

			kwBucket = env.GetKeyword("bucket");
			kwField = env.GetKeyword("field");
			kwGroup = env.GetKeyword("group");
			kwLabel = env.GetKeyword("label");
			kwId = env.GetKeyword("id");

			env.DefineFunction<string, object>("influx-query", InfluxQuery!);

			env.DefineFunction("influx-endpoints", () => new Vector(
				config.InfluxEndpoints.Select(e => e.Name)));
		}

		private object? InfluxQuery(string measurement, object[] args)
		{
			var options = new Options(args);
			var commonOptions = new CommonQueryOptions(options, ki, env);

			string? bucket = options.GetNullable<string>(kwBucket, null);

			InfluxResult result = client.Fetch(
				env.KeywordTable,
				commonOptions,
				bucket,
				measurement,
				options.GetNullable<string>(kwField, null),
				options.GetNullable<string>(kwGroup, null));

			Fetched?.Invoke(this, new InfluxFetchedEventArgs(result));

			var list = commonOptions.Postprocess(result.SeriesList);

			if (commonOptions.Name == null)
			{
				// Try to guess the appropriate field for the name.
				var keys = list.First().Metadata.Keys;

				Keyword? nameKeyword = null;

				if (keys.Contains(ki.KwName)) nameKeyword = ki.KwName;
				else if (keys.Contains(kwLabel)) nameKeyword = kwLabel;
				else if (keys.Contains(kwId)) nameKeyword = kwId;

				if (nameKeyword != null)
				{
					list = list.SetName(nameKeyword);
				}
			}

			return list;
		}
	}
}
