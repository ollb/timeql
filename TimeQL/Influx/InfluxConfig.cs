﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TimeQL.Util;

namespace TimeQL.Influx
{
	public class InfluxConfig
	{
		private readonly JsonElement root;

		public InfluxConfig(JsonElement root)
		{
			this.root = root;
		}

		public string Name
		{
			get { return root.GetString("name"); }
		}

		public Uri Uri
		{
			get { return new Uri(root.GetString("uri", "http://localhost:8086")); }
		}

		public string Organization
		{
			get { return root.GetString("organization"); }
		}

		public string DefaultBucket
		{
			get { return root.GetString("default-bucket"); }
		}

		public string ApiToken
		{
			get { return root.GetString("api-token"); }
		}
	}
}
