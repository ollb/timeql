﻿using Lispery;
using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TimeQL.Util;
using TimeQL.Values;

namespace TimeQL.Influx
{
	internal class InfluxClient
	{
		private readonly Config config;
		private readonly HttpClient client;
		private readonly QueryCache cache;

		private class TimeSeriesBuilder
		{
			public ImmutableSortedDictionary<DateTime, double>.Builder DataPointBuilder;
			public ImmutableDictionary<Keyword, object?>.Builder MetadataBuilder;

			public TimeSeriesBuilder()
			{
				DataPointBuilder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
				MetadataBuilder = ImmutableDictionary.CreateBuilder<Keyword, object?>();
			}
		}

		public InfluxClient(Config config, QueryCache cache)
		{
			this.config = config;
			this.cache = cache;

			HttpClientHandler handler = new();
			handler.AutomaticDecompression = DecompressionMethods.All;

			client = new HttpClient(handler);
		}

		public InfluxResult Fetch(
			KeywordTable keywordTable,
			CommonQueryOptions options,
			string? bucket,
			string measurement,
			string? field,
			string? groupBy,
			string groupFn = "mean")
		{
			ArgumentNullException.ThrowIfNull(measurement);
			ArgumentNullException.ThrowIfNull(groupFn);

			if (!config.InfluxEndpoints.Any())
			{
				throw new InvalidOperationException("No influx endpoints configured.");
			}

			InfluxConfig endpointConfig = config.InfluxEndpoints.First();

			if (options.Endpoint != null)
			{
				endpointConfig = config.InfluxEndpoints.Where(d => d.Name == options.Endpoint).First();
			}

			string groupInstruction = "";

			if (groupBy != null)
			{
				groupBy = Regex.Replace(groupBy, @"[\[\]""]", "_");
				string columns = "\"" + string.Join("\",\"", groupBy.Split(',')) + "\"";
				groupInstruction = $"	|> group(columns: [{columns}])\n";
			}

			string fieldFilter = (field == null) ? "" : $" and r._field == \"{field}\"";
			string usedBucket = bucket ?? endpointConfig.DefaultBucket;

			string query =
				$"from(bucket: \"{usedBucket}\")\n" +
				$"	|> range(start: {options.WindowStart.ToRfc3339()}, stop: {options.WindowStop.ToRfc3339()})\n" +
				$"	|> filter(fn: (r) => r._measurement == \"{measurement}\"{fieldFilter})\n" +
				$"	|> aggregateWindow(every: {options.Resolution.ToInflux()}, fn: {groupFn})\n" +
				groupInstruction;

			var excludeFromKey = new HashSet<string>()
			{
				"_start",
				"_stop",
				"_measurement"
			};

			if (field != null)
			{
				// All series in the result set will have the same field, so
				// there is no need to include that information in the key.
				excludeFromKey.Add("_field");
			}

			return Fetch(options, keywordTable, endpointConfig, query, excludeFromKey);
		}

		private InfluxResult Fetch(
			CommonQueryOptions options,
			KeywordTable keywordTable,
			InfluxConfig endpointConfig,
			string query,
			HashSet<string> excludeFromKey)
		{
			Stopwatch sw = new();
			sw.Start();

			// Create a key for trying to look up the result in the cache.
			string cacheKey = CreateKey(endpointConfig.Name, query, excludeFromKey);

			if (options.Cache && cache.TryLookup(cacheKey, keywordTable, out TimeSeriesList? list))
			{
				return new InfluxResult(sw.Elapsed, TimeSpan.Zero, TimeSpan.Zero, list);
			}

			TimeSpan cacheTime = sw.GetElapsedAndRestart();

			string requestJson;

			using (MemoryStream memoryStream = new())
			using (Utf8JsonWriter writer = new(memoryStream))
			{
				writer.WriteStartObject();
				writer.WriteStartObject("dialect");
				writer.WriteStartArray("annotations");
				writer.WriteStringValue("datatype");
				writer.WriteStringValue("group");
				writer.WriteStringValue("default");
				writer.WriteEndArray();
				writer.WriteString("delimiter", ",");
				writer.WriteBoolean("header", true);
				writer.WriteEndObject();
				writer.WriteString("type", "flux");
				writer.WriteString("query", query);
				writer.WriteEndObject();
				writer.Flush();

				requestJson = Encoding.UTF8.GetString(memoryStream.ToArray());
			}

			StringContent content = new(requestJson);
			content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

			if (!endpointConfig.Uri.AbsolutePath.EndsWith('/'))
			{
				throw new InvalidOperationException("URI does not specify a path.");
			}

			UriBuilder uriBuilder = new();
			uriBuilder.Scheme = endpointConfig.Uri.Scheme;
			uriBuilder.Host = endpointConfig.Uri.Host;
			uriBuilder.Port = endpointConfig.Uri.Port;
			uriBuilder.Path = endpointConfig.Uri.AbsolutePath + "api/v2/query";
			uriBuilder.Query = $"org={endpointConfig.Organization}";

			HttpRequestMessage request = new();
			request.Method = HttpMethod.Post;
			request.RequestUri = uriBuilder.Uri;
			request.Headers.Add("Authorization", $"Token {endpointConfig.ApiToken}");
			request.Headers.Add("Accept", "application/csv");
			request.Headers.Add("Accept-Encoding", "gzip");
			request.Content = content;

			var response = client.Send(request);
			TimeSpan executeTime = sw.GetElapsedAndRestart();

			if ((int)response.StatusCode < 200 || (int)response.StatusCode >= 300)
			{
				// TODO: decode JSON
				throw new InvalidOperationException(response.ReasonPhrase);
			}

			List<int> groupColumns = new();
			List<int> keyColumns = new();
			List<string> columnNames = new();

			int tableColumn = -1;
			int timeColumn = -1;
			int valueColumn = -1;
			bool headerRow = true;

			var builders = new Dictionary<string, TimeSeriesBuilder>();

			using Stream stream = response.Content.ReadAsStream();
			using TextReader reader = new StreamReader(stream);
			CsvParser parser = new(reader);

			while (parser.Read())
			{
				var fields = parser.Record;

				if (fields.Count == 0)
				{
					continue;
				}

				string annotation = fields[0];

				if (annotation == "#group")
				{
					groupColumns.Clear();

					for (int i = 0; i < fields.Count; i++)
					{
						if (fields[i].Equals("true", StringComparison.InvariantCultureIgnoreCase))
						{
							groupColumns.Add(i);
						}
					}

					continue;
				}
				else if (!string.IsNullOrEmpty(annotation))
				{
					continue;
				}

				if (headerRow)
				{
					tableColumn = fields.IndexOf("table");
					timeColumn = fields.IndexOf("_time");
					valueColumn = fields.IndexOf("_value");
					columnNames = fields.ToList();

					keyColumns = groupColumns
						.Where(i => !excludeFromKey.Contains(fields[i]))
						.OrderBy(i => fields[i])
						.ToList();

					headerRow = false;
				}
				else
				{
					string valueStr = fields[valueColumn];
					string timeStr = fields[timeColumn];
					string table = fields[tableColumn];

					if (string.IsNullOrEmpty(valueStr) ||
						string.IsNullOrEmpty(timeStr) ||
						string.IsNullOrEmpty(table))
					{
						continue;
					}

					StringBuilder keyBuilder = new();

					foreach (var index in keyColumns)
					{
						if (keyBuilder.Length > 0)
						{
							keyBuilder.Append(',');
						}

						keyBuilder.Append(columnNames[index]);
						keyBuilder.Append('=');
						keyBuilder.Append(fields[index]);
					}

					string key = keyBuilder.ToString();

					if (!builders.TryGetValue(key, out var builder))
					{
						builder = new TimeSeriesBuilder();
						builders[key] = builder;
					}

					var time = DateTime.Parse(timeStr, CultureInfo.InvariantCulture).ToUniversalTime();
					var value = double.Parse(valueStr, CultureInfo.InvariantCulture);

					builder.DataPointBuilder[time] = value;

					foreach (int column in groupColumns)
					{
						Keyword keyword = keywordTable.Get(columnNames[column]);
						builder.MetadataBuilder[keyword] = fields[column];
					}
				}
			}

			var series = builders.Select(b => new TimeSeries(
				b.Key,
				b.Value.DataPointBuilder.ToImmutable(),
				b.Value.MetadataBuilder.ToImmutable()));

			var result = new TimeSeriesList(series);
			cache.Insert(cacheKey, result, options.Resolution);
			return new InfluxResult(cacheTime, executeTime, sw.Elapsed, result);
		}

		/// <summary>
		/// Creates a key string for the cache.
		/// </summary>
		private static string CreateKey(string database, string query, HashSet<string> excludeFromKey)
		{
			StringBuilder sb = new();
			sb.Append("influx|");
			sb.Append(database);
			sb.Append('|');
			sb.Append(query);
			sb.Append('|');

			foreach (var exclude in excludeFromKey.OrderBy(v => v))
			{
				sb.Append(exclude);
				sb.Append(',');
			}

			return sb.ToString();
		}
	}
}
