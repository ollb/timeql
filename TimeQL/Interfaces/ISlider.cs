﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Interfaces
{
	/// <summary>
	/// Interface for an algorithm that aggregates on a sliding window.
	/// </summary>
	public interface ISlider
	{
		/// <summary>Adds a new value to the window.</summary>
		void Add(double value);

		/// <summary>Removes a value that is already in the window.</summary>
		void Remove(double value);

		/// <summary>Retrieves the aggregated value.</summary>
		/// <param name="values">All values currently in the window.</param>
		double GetValue(IEnumerable<double> values);
	}
}
