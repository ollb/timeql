﻿using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Interfaces
{
	/// <summary>
	/// Value with annotated metadata and name.
	/// </summary>
	public interface IAnnotatedValue
	{
		string Name { get; }
		ImmutableDictionary<Keyword, object?> Metadata { get; }
	}
}
