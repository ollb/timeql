﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TimeQL.Util;

namespace TimeQL.Prometheus
{
	public class PrometheusConfig
	{
		private readonly JsonElement root;

		public PrometheusConfig(JsonElement root)
		{
			this.root = root;
		}

		public string Name
		{
			get { return root.GetString("name"); }
		}

		public Uri Uri
		{
			get { return new Uri(root.GetString("uri", "http://localhost:9090")); }
		}
	}
}
