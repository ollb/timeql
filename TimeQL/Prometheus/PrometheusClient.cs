﻿using Lispery;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TimeQL.Util;
using TimeQL.Values;

namespace TimeQL.Prometheus
{
	public class PrometheusClient
	{
		private readonly Config config;
		private readonly QueryCache cache;
		private readonly HttpClient client;

		public PrometheusClient(Config config, QueryCache cache)
		{
			this.config = config;
			this.cache = cache;

			HttpClientHandler handler = new();
			handler.AutomaticDecompression = DecompressionMethods.All;

			client = new HttpClient(handler);
		}

		public TimeSeriesList Fetch(
			KeywordTable keywordTable,
			CommonQueryOptions options,
			string query)
		{
			if (!config.PrometheusEndpoints.Any())
			{
				throw new InvalidOperationException("No prometheus endpoints configured.");
			}

			PrometheusConfig endpointConfig = config.PrometheusEndpoints.First();

			if (options.Endpoint != null)
			{
				endpointConfig = config.PrometheusEndpoints.Where(d => d.Name == options.Endpoint).First();
			}

			// Create a key for trying to look up the result in the cache.
			string cacheKey = CreateKey(options, query);

			if (options.Cache && cache.TryLookup(cacheKey, keywordTable, out TimeSeriesList? list))
			{
				return list;
			}

			long startTimestamp = new DateTimeOffset(options.WindowStart).ToUnixTimeSeconds();
			long stopTimestamp = new DateTimeOffset(options.WindowStop).ToUnixTimeSeconds();

			StringBuilder sb = new();
			sb.Append("query=");
			sb.Append(Uri.EscapeDataString(query));
			sb.Append("&start=");
			sb.Append(startTimestamp);
			sb.Append("&end=");
			sb.Append(stopTimestamp);
			sb.Append("&step=");
			sb.Append(options.Resolution.TotalSeconds);

			UriBuilder uriBuilder = new();
			uriBuilder.Scheme = endpointConfig.Uri.Scheme;
			uriBuilder.Host = endpointConfig.Uri.Host;
			uriBuilder.Port = endpointConfig.Uri.Port;
			uriBuilder.Path = endpointConfig.Uri.AbsolutePath + "api/v1/query_range";
			uriBuilder.Query = sb.ToString();

			HttpRequestMessage request = new();
			request.Method = HttpMethod.Get;
			request.RequestUri = uriBuilder.Uri;
			request.Headers.Add("Accept", "application/json");
			request.Headers.Add("Accept-Encoding", "gzip");

			var response = client.Send(request);

			if ((int)response.StatusCode < 200 || (int)response.StatusCode >= 300)
			{
				throw new InvalidOperationException(response.ReasonPhrase);
			}

			using Stream stream = response.Content.ReadAsStream();
			JsonDocument doc = JsonDocument.Parse(stream);
			JsonElement root = doc.RootElement;

			if (root.GetString("status") != "success")
			{
				throw new InvalidOperationException("Query not successful.");
			}

			JsonElement data = root.GetProperty("data");
			string resultType = data.GetString("resultType");

			if (resultType != "matrix")
			{
				throw new InvalidOperationException($"Unsupported result type '{resultType}'.");
			}

			var builder = ImmutableList.CreateBuilder<TimeSeries>();

			foreach (var result in data.GetProperty("result").EnumerateArray())
			{
				var metadataBuilder = ImmutableDictionary.CreateBuilder<Keyword, object?>();
				var datapointBuilder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
				string name = "unknown";

				JsonElement labels = result.GetProperty("metric");

				foreach (var label in labels.EnumerateObject())
				{
					metadataBuilder[keywordTable.Get(label.Name)] = label.Value.GetString();

					if (label.Name == "__name__")
					{
						name = label.Value.GetString()!;
					}
				}

				JsonElement datapoints = result.GetProperty("values");

				foreach (var datapoint in datapoints.EnumerateArray())
				{
					var array = datapoint.EnumerateArray().ToArray();

					if (array.Length != 2)
					{
						throw new InvalidOperationException("Invalid datapoint");
					}

					double value = double.Parse(array[1].GetString()!, CultureInfo.InvariantCulture);

					var datetimeOffset = DateTimeOffset.FromUnixTimeSeconds(array[0].GetInt64());
					DateTime time = datetimeOffset.UtcDateTime;

					datapointBuilder[time] = value;
				}

				builder.Add(new TimeSeries(
					name,
					datapointBuilder.ToImmutable(),
					metadataBuilder.ToImmutable()));
			}

			var resultList = new TimeSeriesList(builder.ToImmutable());
			cache.Insert(cacheKey, resultList, options.Resolution);
			return resultList;
		}

		/// <summary>
		/// Creates a key string for the cache.
		/// </summary>
		private static string CreateKey(
			CommonQueryOptions options,
			string query)
		{
			StringBuilder sb = new();
			sb.Append("prometheus|");
			sb.Append(options.Endpoint);
			sb.Append('|');
			sb.Append(options.WindowStart.Ticks);
			sb.Append('|');
			sb.Append(options.WindowStop.Ticks);
			sb.Append('|');
			sb.Append(options.Resolution.Ticks);
			sb.Append('|');
			sb.Append(query);
			return sb.ToString();
		}
	}
}
