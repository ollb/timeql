﻿using Lispery;
using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Graphite;
using TimeQL.Util;

namespace TimeQL.Prometheus
{
	public class PrometheusLib
	{
		private readonly KnownIdentifiers ki;
		private readonly Lispery.Environment env;
		private readonly PrometheusClient client;

		public PrometheusLib(Config config, QueryCache queryCache, KnownIdentifiers ki, Lispery.Environment env)
		{
			this.ki = ki;
			this.env = env;
			this.client = new PrometheusClient(config, queryCache);

			env.DefineFunction<string, object>("prometheus-query", PrometheusQuery!);

			env.DefineFunction("prometheus-endpoints", () => new Vector(
				config.PrometheusEndpoints.Select(e => e.Name)));
		}

		private object? PrometheusQuery(string query, object[] args)
		{
			var options = new Options(args);
			var commonOptions = new CommonQueryOptions(options, ki, env);

			return commonOptions.Postprocess(client.Fetch(
				env.KeywordTable,
				commonOptions,
				query));
		}
	}
}
