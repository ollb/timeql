﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TimeQL.Cloudwatch;
using TimeQL.Graphite;
using TimeQL.Influx;
using TimeQL.Prometheus;
using TimeQL.Util;

namespace TimeQL
{
	public class Config
	{
		private JsonElement root;

		private readonly List<InfluxConfig> influxEndpoints = new();
		private readonly List<GraphiteConfig> graphiteEndpoints = new();
		private readonly List<CloudwatchConfig> cloudwatchEndpoints = new();
		private readonly List<PrometheusConfig> prometheusEndpoints = new();
		private readonly List<string> includes = new();

		public string? Filename { get; set; }

		public Config()
		{
			if (Environment.OSVersion.Platform == PlatformID.Win32NT)
			{
				if (!TryLoad(Path.Combine(
					Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
					"TimeQL",
					"config.json")))
				{
					if (!TryLoad(Path.Combine(
						Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
						"TimeQL",
						"config.json")))
					{
						root = JsonDocument.Parse("{}").RootElement;
					}
				}
			}
			else
			{
				if (!TryLoad(GetHomePath(".timeql/config.json")))
				{
					if (!TryLoad("/etc/timeql/config.json"))
					{
						root = JsonDocument.Parse("{}").RootElement;
					}
				}
			}
		}

		public Config(JsonDocument document)
		{
			this.root = document.RootElement;
		}

		private static string? GetHomePath(string path)
		{
			string? home = Environment.GetEnvironmentVariable("HOME");

			if (home != null)
			{
				return Path.Combine(home, path);
			}

			return null;
		}

		private bool TryLoad(string? filename)
		{
			if (filename == null || !File.Exists(filename))
			{
				return false;
			}

			JsonDocumentOptions options = new();
			options.CommentHandling = JsonCommentHandling.Skip;

			using Stream stream = File.OpenRead(filename);
			root = JsonDocument.Parse(stream, options).RootElement;

			if (root.TryGetProperty("influx-endpoints", out var influxEndpointsRoot))
			{
				foreach (var endpoint in influxEndpointsRoot.EnumerateArray())
				{
					influxEndpoints.Add(new InfluxConfig(endpoint));
				}
			}

			if (root.TryGetProperty("graphite-endpoints", out var graphiteEndpointsRoot))
			{
				foreach (var endpoint in graphiteEndpointsRoot.EnumerateArray())
				{
					graphiteEndpoints.Add(new GraphiteConfig(endpoint));
				}
			}

			if (root.TryGetProperty("cloudwatch-endpoints", out var cloudwatchEndpointsRoot))
			{
				foreach (var endpoint in cloudwatchEndpointsRoot.EnumerateArray())
				{
					cloudwatchEndpoints.Add(new CloudwatchConfig(endpoint));
				}
			}

			if (root.TryGetProperty("prometheus-endpoints", out var prometheusEndpointsRoot))
			{
				foreach (var endpoint in prometheusEndpointsRoot.EnumerateArray())
				{
					prometheusEndpoints.Add(new PrometheusConfig(endpoint));
				}
			}

			if (root.TryGetProperty("includes", out var includesRoot))
			{
				foreach (var include in includesRoot.EnumerateArray())
				{
					string? file = include.GetString();

					if (file == null)
					{
						throw new InvalidOperationException("Invalid include file.");
					}

					includes.Add(file);
				}
			}

			Filename = filename;
			return true;
		}

		/// <summary>
		/// Returns the host address for the server.
		/// </summary>
		public string Host => root.GetString("host", "localhost");

		/// <summary>
		/// Returns the listen port for the server.
		/// </summary>
		public int Port => root.GetInt32("port", 5000);

		/// <summary>
		/// Returns the limit for the number of evaluation steps.
		/// </summary>
		public long EvaluationStepLimit => root.GetInt64("evaluation-step-limit", 1000000);

		/// <summary>
		/// Returns the compression method used for the cache.
		/// </summary>
		public string CacheCompression => root.GetString("cache-compression", "zstd");

		/// <summary>
		/// Returns the compression level used for the cache.
		/// </summary>
		public int CacheCompressionLevel => root.GetInt32("cache-compression-level", 1);

		/// <summary>
		/// Returns the maximum cache TTL in minutes.
		/// </summary>
		public int CacheMaxTtlInterval => root.GetInt32("cache-max-ttl-interval", 60);

		/// <summary>
		/// Returns the cache cleanup interval in minutes.
		/// </summary>
		public int CacheCleanupInterval => root.GetInt32("cache-cleanup-interval", 15);

		/// <summary>
		/// Returns the compression method used for HTTP responses.
		/// </summary>
		public string ResponseCompression => root.GetString("response-compression", "gzip");

		/// <summary>
		/// Returns the compression level used for HTTP responses.
		/// </summary>
		public int ResponseCompressionLevel => root.GetInt32("response-compression-level", 1);

		/// <summary>
		/// The maximum number of concurrent workers.
		/// </summary>
		public int WorkerLimit => root.GetInt32("worker-limit", 4);

		/// <summary>
		/// The maximum number of pending requests.
		/// </summary>
		public int PendingRequestLimit => root.GetInt32("pending-request-limit", 50);

		/// <summary>
		/// Returns all include files.
		/// </summary>
		public IEnumerable<string> Includes => includes;

		/// <summary>
		/// Returns the influx endpoint configurations.
		/// </summary>
		public IEnumerable<InfluxConfig> InfluxEndpoints => influxEndpoints;

		/// <summary>
		/// Returns the graphite endpoint configurations.
		/// </summary>
		public IEnumerable<GraphiteConfig> GraphiteEndpoints => graphiteEndpoints;

		/// <summary>
		/// Returns the cloudwatch endpoint configurations.
		/// </summary>
		public IEnumerable<CloudwatchConfig> CloudwatchEndpoints => cloudwatchEndpoints;

		/// <summary>
		/// Returns the prometheus endpoint configurations.
		/// </summary>
		public IEnumerable<PrometheusConfig> PrometheusEndpoints => prometheusEndpoints;
	}
}
