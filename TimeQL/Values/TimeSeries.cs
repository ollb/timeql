﻿using Lispery;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Serialization;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TimeQL.Interfaces;
using TimeQL.Util;

namespace TimeQL.Values
{
	public class TimeSeries : ISequence, IFormatable, IAnnotatedValue, IComparable
	{
		/// <summary>
		/// The name of the time series.
		/// </summary>
		public string Name { get; init; }

		/// <summary>
		/// Datapoints that are contained in the time series.
		/// </summary>
		/// 
		public ImmutableSortedDictionary<DateTime, double> DataPoints { get; init; }

		/// <summary>
		/// Metadata associated with the time series.
		/// </summary>
		public ImmutableDictionary<Keyword, object?> Metadata { get; init; }

		/// <summary>
		/// The current resolution of the time series. May be zero when unaligned.
		/// </summary>
		public TimeSpan? Resolution { get; init; }

		/// <summary>
		/// Creates a new time series.
		/// </summary>
		public TimeSeries(string name, ImmutableSortedDictionary<DateTime, double> dataPoints) :
			this(name, dataPoints, ImmutableDictionary<Keyword, object?>.Empty, TimeSpan.Zero)
		{
		}

		/// <summary>
		/// Creates a new time series.
		/// </summary>
		public TimeSeries(
			string name,
			ImmutableSortedDictionary<DateTime, double> dataPoints,
			ImmutableDictionary<Keyword, object?> metadata) :
			this(name, dataPoints, metadata, null)
		{
		}

		/// <summary>
		/// Creates a new time series.
		/// </summary>
		public TimeSeries(
			string name,
			ImmutableSortedDictionary<DateTime, double> dataPoints,
			ImmutableDictionary<Keyword, object?> metadata,
			TimeSpan? resolution)
		{
			Name = name;
			DataPoints = dataPoints;
			Metadata = metadata;
			Resolution = resolution;
		}

		/// <summary>
		/// Retrieves the first value from each series individually.
		/// </summary>
		public double GetFirstValue()
		{
			return DataPoints.First().Value;
		}

		/// <summary>
		/// Retrieves the last value from each series individually.
		/// </summary>
		public double GetLastValue()
		{
			return DataPoints.Last().Value;
		}

		/// <summary>
		/// Returns a new time series with the given name.
		/// </summary>
		public TimeSeries SetName(string name)
		{
			return new TimeSeries(name, DataPoints, Metadata, Resolution);
		}

		/// <summary>
		/// Returns a new time series with a name selected from the metadata.
		/// </summary>
		public TimeSeries SetName(Keyword key)
		{
			return SetName(Metadata[key]?.ToString() ?? "nil");
		}

		/// <summary>
		/// Returns a new time series with a name selected from combined metadata.
		/// </summary>
		public TimeSeries SetName(ISequence seq)
		{
			return SetName(Util.Metadata.Select(Metadata, seq));
		}

		/// <summary>
		/// Returns a new time series with the given metadata set.
		/// </summary>
		public TimeSeries SetMeta(Keyword key, object? value)
		{
			return new TimeSeries(Name, DataPoints, Metadata.SetItem(key, value), Resolution);
		}

		/// <summary>
		/// Apply a map function to the values.
		/// </summary>
		public TimeSeries MapValues(Func<double, double> fun)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();

			foreach (var entry in DataPoints)
			{
				builder[entry.Key] = fun(entry.Value);
			}

			return new TimeSeries(Name, builder.ToImmutable(), Metadata, Resolution);
		}

		/// <summary>
		/// Apply a fold function to the values. Records every accumulator value.
		/// </summary>
		public TimeSeries ScanValues(Func<double, double, double> fun)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();

			double acc = 0;
			bool first = true;

			foreach (var entry in DataPoints)
			{
				acc = first ? entry.Value : fun(entry.Value, acc);
				builder[entry.Key] = acc;
				first = false;
			}

			return new TimeSeries(Name, builder.ToImmutable(), Metadata, Resolution);
		}

		/// <summary>
		/// Apply a fold function to the values. Records every accumulator value.
		/// </summary>
		public TimeSeries ScanValues(Func<double, double, double> fun, double seed)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			double acc = seed;

			foreach (var entry in DataPoints)
			{
				acc = fun(entry.Value, acc);
				builder[entry.Key] = acc;
			}

			return new TimeSeries(Name, builder.ToImmutable(), Metadata, Resolution);
		}

		/// <summary>
		/// Slides a window through the series. Applies the function on each window.
		/// </summary>
		public TimeSeries SlideMap(int count, ISlider aggregator)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			var queue = new Queue<double>();

			foreach (var entry in DataPoints)
			{
				if (queue.Count == count)
				{
					aggregator.Remove(queue.Dequeue());
				}

				aggregator.Add(entry.Value);
				queue.Enqueue(entry.Value);

				builder[entry.Key] = aggregator.GetValue(queue);
			}

			return new TimeSeries(Name, builder.ToImmutable(), Metadata, Resolution);
		}

		/// <summary>
		/// Slides a window through the series. Applies the function on each window.
		/// </summary>
		public TimeSeries SlideMap(TimeSpan interval, ISlider aggregator)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			var queue = new Queue<KeyValuePair<DateTime, double>>();

			foreach (var entry in DataPoints)
			{
				// Drop entries from the queue that have left the window.
				while (queue.Count > 0 && queue.First().Key < entry.Key - interval)
				{
					aggregator.Remove(queue.Dequeue().Value);
				}

				aggregator.Add(entry.Value);
				queue.Enqueue(entry);

				builder[entry.Key] = aggregator.GetValue(queue.Select(e => e.Value));
			}

			return new TimeSeries(Name, builder.ToImmutable(), Metadata, Resolution);
		}

		/// <summary>
		/// Groups values into windows. Applies the function on each window.
		/// </summary>
		public TimeSeries WindowMap(TimeSpan interval, Func<IEnumerable<double>, double> fun)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			var groups = DataPoints.GroupBy(d => d.Key.AlignDown(interval));

			foreach (var group in groups)
			{
				builder[group.Key] = fun(group.Select(d => d.Value));
			}

			return new TimeSeries(Name, builder.ToImmutable(), Metadata, interval);
		}

		/// <summary>
		/// Apply a filter function to the values.
		/// </summary>
		public TimeSeries FilterValues(Func<double, bool> fun)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();

			foreach (var entry in DataPoints)
			{
				if (fun(entry.Value))
				{
					builder[entry.Key] = entry.Value;
				}
			}

			return new TimeSeries(Name, builder.ToImmutable(), Metadata, Resolution);
		}

		/// <summary>
		/// Apply a fold function to the values.
		/// </summary>
		public double FoldValues(Func<double, double, double> fun)
		{
			return DataPoints.Values.Aggregate(fun);
		}

		/// <summary>
		/// Apply a fold function to the values.
		/// </summary>
		public double FoldValues(Func<double, double, double> fun, double seed)
		{
			return DataPoints.Values.Aggregate(seed, fun);
		}

		/// <summary>
		/// Time-shifts the series.
		/// </summary>
		public TimeSeries TimeShift(TimeSpan delta)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();

			foreach (var entry in DataPoints)
			{
				builder[entry.Key + delta] = entry.Value;
			}

			var series = new TimeSeries(Name, builder.ToImmutable(), Metadata);

			if (Resolution.HasValue)
			{
				return series.Align(Resolution.Value);
			}

			return series;
		}

		/// <summary>
		/// Performs an inner join on one or more series.
		/// </summary>
		public static TimeSeries Join(Func<double[], double> fun, params TimeSeries[] list)
		{
			return Join(fun, (IEnumerable<TimeSeries>)list);
		}

		/// <summary>
		/// Performs an inner join on one or more series.
		/// </summary>
		public static TimeSeries Join(Func<double[], double> fun, IEnumerable<TimeSeries> list)
		{
			if (!list.Any())
			{
				throw new InvalidOperationException("No series to join.");
			}

			// Make sure that all series have the same resolution.
			TimeSpan? resolution = null;

			if (list.IsSameForAll(s => s.Resolution))
			{
				resolution = list.First().Resolution;
			}

			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			double[] values = new double[list.Count()];

			foreach (var key in list.First().DataPoints.Keys)
			{
				bool complete = true;

				// Collect all input values.
				int index = 0;

				foreach (var series in list)
				{
					if (!series.DataPoints.TryGetValue(key, out values[index++]))
					{
						complete = false;
						break;
					}
				}

				// If one value is missing, drop the key.
				if (complete)
				{
					builder[key] = fun(values);
				}
			}

			string name = list.IsSameForAll(s => s.Name) ?
				list.First().Name :
				"result";

			return new TimeSeries(
				name,
				builder.ToImmutable(),
				Util.Metadata.Join(list),
				resolution);
		}

		/// <summary>
		/// Performs an outer join on one or more series.
		/// </summary>
		public static TimeSeries Join(Func<double[], double> fun, double fallback, params TimeSeries[] list)
		{
			return Join(fun, fallback, (IEnumerable<TimeSeries>)list);
		}

		/// <summary>
		/// Performs an outer join on one or more series.
		/// </summary>
		public static TimeSeries Join(Func<double[], double> fun, double fallback, IEnumerable<TimeSeries> list)
		{
			if (!list.Any())
			{
				throw new InvalidOperationException("No series to join.");
			}

			TimeSpan? resolution = null;

			if (list.IsSameForAll(s => s.Resolution))
			{
				resolution = list.First().Resolution;
			}

			// Determine all the keys.
			HashSet<DateTime> keys = new();

			foreach (var series in list)
			{
				keys.AddRange(series.DataPoints.Keys);
			}

			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			double[] values = new double[list.Count()];

			foreach (var key in keys)
			{
				int index = 0;

				foreach (var series in list)
				{
					values[index++] = series.DataPoints.GetValueOrDefault(key, fallback);
				}

				builder[key] = fun(values);
			}

			string name = list.IsSameForAll(s => s.Name) ?
				list.First().Name :
				"result";

			return new TimeSeries(
				name,
				builder.ToImmutable(),
				Util.Metadata.Join(list),
				resolution);
		}

		/// <summary>
		/// Randomize the times.
		/// </summary>
		public TimeSeries RandomizeTimes(Random random, TimeSpan interval)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();

			foreach (var datapoint in DataPoints)
			{
				TimeSpan randInterval = random.NextDouble() * interval;

				var newKey = datapoint.Key + randInterval - interval / 2.0;
				builder[newKey] = datapoint.Value;
			}

			return new TimeSeries(
				Name,
				builder.ToImmutable(),
				Metadata,
				null);
		}

		/// <summary>
		/// Derives the time series.
		/// </summary>
		public TimeSeries Derive()
		{
			if (Resolution == null)
			{
				throw new InvalidOperationException("Series is not aligned.");
			}

			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();

			KeyValuePair<DateTime, double>? last = null;

			foreach (var next in DataPoints)
			{
				if (last != null)
				{
					if (next.Key - last.Value.Key > Resolution.Value)
					{
						last = null;
					}
					else
					{
						double value = (next.Value - last.Value.Value) / Resolution.Value.TotalHours;
						builder[next.Key] = value;
					}
				}

				last = next;
			}

			return new TimeSeries(
				Name,
				builder.ToImmutable(),
				Metadata,
				Resolution);
		}

		/// <summary>
		/// Integrates the time series.
		/// </summary>
		public TimeSeries Integrate()
		{
			if (Resolution == null)
			{
				throw new InvalidOperationException("Series is not aligned.");
			}

			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			DateTime lastTime = DataPoints.Keys.First() - Resolution.Value;
			double accumulator = 0;
			builder[lastTime] = 0;

			foreach (var datapoint in DataPoints)
			{
				if (datapoint.Key > lastTime + Resolution.Value)
				{
					lastTime = datapoint.Key - Resolution.Value;
					accumulator = 0;
					builder[lastTime] = 0;
				}

				accumulator += datapoint.Value * Resolution.Value.TotalHours;
				builder[datapoint.Key] = accumulator;
				lastTime = datapoint.Key;
			}

			return new TimeSeries(
				Name,
				builder.ToImmutable(),
				Metadata,
				Resolution);
		}

		/// <summary>
		/// Aligns the time series to the given interval.
		/// </summary>
		public TimeSeries Align(TimeSpan interval)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();

			foreach (var entry in DataPoints)
			{
				builder[entry.Key.AlignDown(interval)] = entry.Value;
			}

			return new TimeSeries(Name, builder.ToImmutable(), Metadata, interval);
		}

		/// <summary>
		/// Concatenates the given metadata.
		/// </summary>
		public TimeSeries ConcatMeta(Keyword key, string separator, IEnumerable<Keyword> keys)
		{
			string newValue = string.Join(separator, keys.Select(k => Metadata[k]?.ToString() ?? "nil"));
			return SetMeta(key, newValue);
		}

		/// <summary>
		/// Serializes the time series with metadata.
		/// </summary>
		public void Serialize(BinaryWriter writer)
		{
			writer.Write(Name);
			writer.Write(Resolution.HasValue ? Resolution.Value.Ticks : (long)0);

			writer.Write((int)DataPoints.Count);

			foreach (var dataPoint in DataPoints)
			{
				Debug.Assert(dataPoint.Key.Kind == DateTimeKind.Utc);
				writer.Write((long)dataPoint.Key.Ticks);
				writer.Write(dataPoint.Value);
			}

			BinarySerializer serializer = new();
			writer.Write((int)Metadata.Count);

			foreach (var entry in Metadata)
			{
				writer.Write(entry.Key.Name);
				serializer.Serialize(entry.Value, writer);
			}
		}

		/// <summary>
		/// Deserializes a series from the given binary writer.
		/// </summary>
		public static TimeSeries Deserialize(KeywordTable keywordTable, BinaryReader reader)
		{
			string name = reader.ReadString();
			long resolution = reader.ReadInt64();

			var dataPointBuilder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			int dataPointCount = reader.ReadInt32();

			for (uint i = 0; i < dataPointCount; i++)
			{
				long ticks = reader.ReadInt64();
				double value = reader.ReadDouble();
				dataPointBuilder[new DateTime(ticks, DateTimeKind.Utc)] = value;
			}

			var metadataBuilder = ImmutableDictionary.CreateBuilder<Keyword, object?>();
			int metadataCount = reader.ReadInt32();

			BinarySerializer serializer = new();

			for (uint i = 0; i < metadataCount; i++)
			{
				string keywordName = reader.ReadString();
				var keyword = keywordTable.Get(keywordName);
				var value = serializer.Deserialize(reader);
				metadataBuilder[keyword] = value;
			}

			return new TimeSeries(
				name,
				dataPointBuilder.ToImmutable(),
				metadataBuilder.ToImmutable(),
				resolution == 0 ? null : TimeSpan.FromTicks(resolution));
		}

		/// <summary>
		/// Serializes the time series *without* metadata to JSON.
		/// </summary>
		public void SerializeJson(Utf8JsonWriter writer)
		{
			writer.WriteStartObject();
			writer.WriteString("label", Name);
			writer.WriteStartArray("data-points");

			foreach (var dataPoint in DataPoints)
			{
				writer.WriteStartObject();
				writer.WriteNumber("time", new DateTimeOffset(dataPoint.Key).ToUnixTimeMilliseconds());
				writer.WriteNumber("value", dataPoint.Value);
				writer.WriteEndObject();
			}

			writer.WriteEndArray();
			writer.WriteEndObject();
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		public override string ToString()
		{
			return $"Series \"{Name}\"";
		}

		public IEnumerable<object?> GetEntries()
		{
			return DataPoints
				.Select(e => KeyValuePair.Create<object, object?>(e.Key, e.Value))
				.Cast<object>();
		}

		public int Count()
		{
			return DataPoints.Count;
		}

		public void Format(TextWriter writer, Formatter formatter)
		{
			writer.Write($"<{ToString()}>");
		}

		public int CompareTo(TimeSeries? other)
		{
			if (other == null)
			{
				return 1;
			}

			return Name.CompareTo(other.Name);
		}

		public int CompareTo(object? obj)
		{
			if (obj is not TimeSeries series)
			{
				return 1;
			}

			return Name.CompareTo(series.Name);
		}

		public static TimeSeries operator +(TimeSeries lhs, TimeSeries rhs)
		{
			return Join((v) => v[0] + v[1], lhs, rhs);
		}

		public static TimeSeries operator +(TimeSeries lhs, double rhs)
		{
			return lhs.MapValues(v => v + rhs);
		}

		public static TimeSeries operator +(double lhs, TimeSeries rhs)
		{
			return rhs.MapValues(v => lhs + v);
		}

		public static TimeSeries operator -(TimeSeries lhs)
		{
			return lhs.MapValues(v => -v);
		}

		public static TimeSeries operator -(TimeSeries lhs, TimeSeries rhs)
		{
			return Join((v) => v[0] - v[1], lhs, rhs);
		}

		public static TimeSeries operator -(TimeSeries lhs, double rhs)
		{
			return lhs.MapValues(v => v - rhs);
		}

		public static TimeSeries operator -(double lhs, TimeSeries rhs)
		{
			return rhs.MapValues(v => lhs - v);
		}

		public static TimeSeries operator *(TimeSeries lhs, TimeSeries rhs)
		{
			return Join((v) => v[0] * v[1], lhs, rhs);
		}

		public static TimeSeries operator *(TimeSeries lhs, double rhs)
		{
			return lhs.MapValues(v => v * rhs);
		}

		public static TimeSeries operator *(double lhs, TimeSeries rhs)
		{
			return rhs.MapValues(v => lhs * v);
		}

		public static TimeSeries operator /(TimeSeries lhs, TimeSeries rhs)
		{
			return Join((v) => v[0] / v[1], lhs, rhs);
		}

		public static TimeSeries operator /(TimeSeries lhs, double rhs)
		{
			return lhs.MapValues(v => v / rhs);
		}

		public static TimeSeries operator /(double lhs, TimeSeries rhs)
		{
			return rhs.MapValues(v => lhs / v);
		}
	}
}
