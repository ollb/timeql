﻿using Lispery;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Values;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Values
{
	public class TimeSeriesList : ISequence, IFormatable, IEnumerable<TimeSeries>
	{
		public ImmutableList<TimeSeries> Entries { get; init; }

		public TimeSeriesList(params TimeSeries[] entries) :
			this(ImmutableList.CreateRange(entries))
		{
		}

		public TimeSeriesList(IEnumerable<TimeSeries> entries) :
			this(ImmutableList.CreateRange(entries))
		{
		}

		public TimeSeriesList(ImmutableList<TimeSeries> entries)
		{
			Entries = entries;
		}

		/// <summary>
		/// Returns the resolution of the series. If not unique, null is returned.
		/// </summary>
		public TimeSpan? Resolution
		{
			get
			{
				var distinct = Entries.Select(s => s.Resolution).Distinct().ToArray();
				return distinct.Length != 1 ? null : distinct.First();
			}
		}

		/// <summary>
		/// Retrieves the first value from each series individually.
		/// </summary>
		public NamedValueList GetFirstValue()
		{
			return Map(s => new NamedValue(s.Name, s.GetFirstValue()));
		}

		/// <summary>
		/// Retrieves the last value from each series individually.
		/// </summary>
		public NamedValueList GetLastValue()
		{
			return Map(s => new NamedValue(s.Name, s.GetLastValue()));
		}

		/// <summary>
		/// Retrieves the first consistent value set.
		/// </summary>
		public NamedValueList GetFirstValueSet()
		{
			DateTime time = Entries.Min(s => s.DataPoints.First().Key);
			var builder = ImmutableList.CreateBuilder<NamedValue>();

			foreach (var series in Entries)
			{
				if (series.DataPoints.TryGetValue(time, out double value))
				{
					builder.Add(new NamedValue(series.Name, value));
				}
			}

			return new NamedValueList(builder.ToImmutable());
		}

		/// <summary>
		/// Retrieves the last consistent value set.
		/// </summary>
		public NamedValueList GetLastValueSet()
		{
			DateTime time = Entries.Max(s => s.DataPoints.Last().Key);
			var builder = ImmutableList.CreateBuilder<NamedValue>();

			foreach (var series in Entries)
			{
				if (series.DataPoints.TryGetValue(time, out double value))
				{
					builder.Add(new NamedValue(series.Name, value));
				}
			}

			return new NamedValueList(builder.ToImmutable());
		}

		/// <summary>
		/// Returns a new time series list with the given name.
		/// </summary>
		public TimeSeriesList SetName(string name)
		{
			return Map(s => s.SetName(name)).Sort();
		}

		/// <summary>
		/// Returns a new time series list with a name selected from the metadata.
		/// </summary>
		public TimeSeriesList SetName(Keyword key)
		{
			return Map(s => s.SetName(key)).Sort();
		}

		/// <summary>
		/// Returns a new time series list with a name selected from combined metadata.
		/// </summary>
		public TimeSeriesList SetName(ISequence seq)
		{
			return Map(s => s.SetName(seq)).Sort();
		}

		/// <summary>
		/// Returns a new time series list with the given metadata set.
		/// </summary>
		public TimeSeriesList SetMeta(Keyword key, object? value)
		{
			return Map(s => s.SetMeta(key, value));
		}

		public TimeSeriesList Map(Func<TimeSeries, TimeSeries> fun)
		{
			var builder = ImmutableList.CreateBuilder<TimeSeries>();

			foreach (var series in Entries)
			{
				builder.Add(fun(series));
			}

			return new TimeSeriesList(builder.ToImmutable());
		}

		public NamedValueList Map(Func<TimeSeries, NamedValue> fun)
		{
			var builder = ImmutableList.CreateBuilder<NamedValue>();

			foreach (var series in Entries)
			{
				builder.Add(fun(series));
			}

			return new NamedValueList(builder.ToImmutable());
		}

		public TimeSeriesList Match(Func<TimeSeries, TimeSeries, TimeSeries> fun, TimeSeriesList rhs)
		{
			var lhsDict = CreateNameDictionary(this);
			var rhsDict = CreateNameDictionary(rhs);
			var builder = ImmutableList.CreateBuilder<TimeSeries>();

			foreach (var entry in lhsDict)
			{
				if (rhsDict.TryGetValue(entry.Key, out TimeSeries? rhsSeries))
				{
					builder.Add(fun(entry.Value, rhsSeries));
				}
			}

			return new TimeSeriesList(builder.ToImmutable());
		}

		public TimeSeriesList Match(Func<TimeSeries, NamedValue, TimeSeries> fun, NamedValueList rhs)
		{
			var lhsDict = CreateNameDictionary(this);
			var rhsDict = CreateNameDictionary(rhs);
			var builder = ImmutableList.CreateBuilder<TimeSeries>();

			foreach (var entry in lhsDict)
			{
				if (rhsDict.TryGetValue(entry.Key, out NamedValue? rhsValue))
				{
					builder.Add(fun(entry.Value, rhsValue));
				}
			}

			return new TimeSeriesList(builder.ToImmutable());
		}

		public TimeSeriesList Filter(Func<TimeSeries, bool> predicate)
		{
			var builder = ImmutableList.CreateBuilder<TimeSeries>();

			foreach (var entry in Entries)
			{
				if (predicate(entry))
				{
					builder.Add(entry);
				}
			}

			return new TimeSeriesList(builder.ToImmutable());
		}

		/// <summary>
		/// Serializes the time series list with metadata.
		/// </summary>
		public void Serialize(BinaryWriter writer)
		{
			writer.Write((int)Entries.Count);

			foreach (var entry in Entries)
			{
				entry.Serialize(writer);
			}
		}

		/// <summary>
		/// Serializes the time series list with metadata.
		/// </summary>
		public byte[] Serialize()
		{
			using MemoryStream stream = new();
			using BinaryWriter writer = new(stream);
			Serialize(writer);
			writer.Flush();
			return stream.GetBuffer()[..(int)stream.Length];
		}

		/// <summary>
		/// Desrializes the time series list with metadata.
		/// </summary>
		public static TimeSeriesList Deserialize(KeywordTable keywordTable, BinaryReader reader)
		{
			var builder = ImmutableList.CreateBuilder<TimeSeries>();
			int count = reader.ReadInt32();

			for (int i = 0; i < count; i++)
			{
				builder.Add(TimeSeries.Deserialize(keywordTable, reader));
			}

			return new TimeSeriesList(builder.ToImmutable());
		}

		/// <summary>
		/// Desrializes the time series list with metadata.
		/// </summary>
		public static TimeSeriesList Deserialize(KeywordTable keywordTable, byte[] data)
		{
			using MemoryStream stream = new(data);
			using BinaryReader reader = new(stream);
			return Deserialize(keywordTable, reader);
		}

		/// <summary>
		/// Derives the time series list.
		/// </summary>
		public TimeSeriesList Derive()
		{
			return Map(s => s.Derive());
		}

		/// <summary>
		/// Integrates the time series list.
		/// </summary>
		public TimeSeriesList Integrate()
		{
			return Map(s => s.Integrate());
		}

		/// <summary>
		/// Time-shifts the series list.
		/// </summary>
		public TimeSeriesList TimeShift(TimeSpan delta)
		{
			return Map(s => s.TimeShift(delta));
		}

		public TimeSeriesList Align(TimeSpan interval)
		{
			return Map(s => s.Align(interval));
		}

		public TimeSeriesList ConcatMeta(Keyword key, string separator, IEnumerable<Keyword> keys)
		{
			return Map(s => s.ConcatMeta(key, separator, keys));
		}

		public IEnumerable<object?> GetEntries()
		{
			return Entries;
		}

		public int Count()
		{
			return Entries.Count;
		}

		public void Format(TextWriter writer, Formatter formatter)
		{
			writer.Write("<SeriesList");

			foreach (var series in Entries)
			{
				writer.Write(" \"");
				writer.Write(series.Name);
				writer.Write("\"");
			}

			writer.Write('>');
		}

		public IEnumerator<TimeSeries> GetEnumerator()
		{
			return Entries.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public TimeSeriesList Sort(SortOrder order = SortOrder.Ascending)
		{
			if (order == SortOrder.Descending)
			{
				return new TimeSeriesList(Entries.OrderByDescending(s => s.Name));
			}

			return new TimeSeriesList(Entries.OrderBy(s => s.Name));
		}

		object? ISupportSort.Sort(SortOrder order)
		{
			return Sort(order);
		}

		private static Dictionary<string, TimeSeries> CreateNameDictionary(TimeSeriesList list)
		{
			Dictionary<string, TimeSeries> series = new();

			foreach (var item in list)
			{
				series.Add(item.Name, item);
			}

			return series;
		}

		private static Dictionary<string, NamedValue> CreateNameDictionary(NamedValueList list)
		{
			Dictionary<string, NamedValue> series = new();

			foreach (var item in list)
			{
				series.Add(item.Name, item);
			}

			return series;
		}

		public static TimeSeriesList operator +(TimeSeriesList lhs, TimeSeriesList rhs)
		{
			return lhs.Match((a, b) => a + b, rhs);
		}

		public static TimeSeriesList operator +(TimeSeriesList lhs, NamedValueList rhs)
		{
			return lhs.Match((a, b) => a + b.Value, rhs);
		}

		public static TimeSeriesList operator +(TimeSeriesList lhs, TimeSeries rhs)
		{
			return lhs.Map(s => s + rhs);
		}

		public static TimeSeriesList operator +(TimeSeriesList lhs, double rhs)
		{
			return lhs.Map(s => s + rhs);
		}

		public static TimeSeriesList operator +(NamedValueList lhs, TimeSeriesList rhs)
		{
			return rhs.Match((a, b) => b.Value + a, lhs);
		}

		public static TimeSeriesList operator +(TimeSeries lhs, TimeSeriesList rhs)
		{
			return rhs.Map(s => lhs + s);
		}

		public static TimeSeriesList operator +(double lhs, TimeSeriesList rhs)
		{
			return rhs.Map(s => lhs + s);
		}

		public static TimeSeriesList operator -(TimeSeriesList lhs)
		{
			return lhs.Map(s => -s);
		}

		public static TimeSeriesList operator -(TimeSeriesList lhs, TimeSeriesList rhs)
		{
			return lhs.Match((a, b) => a - b, rhs);
		}

		public static TimeSeriesList operator -(TimeSeriesList lhs, NamedValueList rhs)
		{
			return lhs.Match((a, b) => a - b.Value, rhs);
		}

		public static TimeSeriesList operator -(TimeSeriesList lhs, TimeSeries rhs)
		{
			return lhs.Map(s => s - rhs);
		}

		public static TimeSeriesList operator -(TimeSeriesList lhs, double rhs)
		{
			return lhs.Map(s => s - rhs);
		}

		public static TimeSeriesList operator -(NamedValueList lhs, TimeSeriesList rhs)
		{
			return rhs.Match((a, b) => b.Value - a, lhs);
		}

		public static TimeSeriesList operator -(TimeSeries lhs, TimeSeriesList rhs)
		{
			return rhs.Map(s => lhs - s);
		}

		public static TimeSeriesList operator -(double lhs, TimeSeriesList rhs)
		{
			return rhs.Map(s => lhs - s);
		}

		public static TimeSeriesList operator *(TimeSeriesList lhs, TimeSeriesList rhs)
		{
			return lhs.Match((a, b) => a * b, rhs);
		}

		public static TimeSeriesList operator *(TimeSeriesList lhs, NamedValueList rhs)
		{
			return lhs.Match((a, b) => a * b.Value, rhs);
		}

		public static TimeSeriesList operator *(TimeSeriesList lhs, TimeSeries rhs)
		{
			return lhs.Map(s => s * rhs);
		}

		public static TimeSeriesList operator *(TimeSeriesList lhs, double rhs)
		{
			return lhs.Map(s => s * rhs);
		}

		public static TimeSeriesList operator *(NamedValueList lhs, TimeSeriesList rhs)
		{
			return rhs.Match((a, b) => b.Value * a, lhs);
		}

		public static TimeSeriesList operator *(TimeSeries lhs, TimeSeriesList rhs)
		{
			return rhs.Map(s => lhs * s);
		}

		public static TimeSeriesList operator *(double lhs, TimeSeriesList rhs)
		{
			return rhs.Map(s => lhs * s);
		}

		public static TimeSeriesList operator /(TimeSeriesList lhs, TimeSeriesList rhs)
		{
			return lhs.Match((a, b) => a / b, rhs);
		}

		public static TimeSeriesList operator /(TimeSeriesList lhs, NamedValueList rhs)
		{
			return lhs.Match((a, b) => a / b.Value, rhs);
		}

		public static TimeSeriesList operator /(TimeSeriesList lhs, TimeSeries rhs)
		{
			return lhs.Map(s => s / rhs);
		}

		public static TimeSeriesList operator /(NamedValueList lhs, TimeSeriesList rhs)
		{
			return rhs.Match((a, b) => b.Value / a, lhs);
		}

		public static TimeSeriesList operator /(TimeSeriesList lhs, double rhs)
		{
			return lhs.Map(s => s / rhs);
		}

		public static TimeSeriesList operator /(TimeSeries lhs, TimeSeriesList rhs)
		{
			return rhs.Map(s => lhs / s);
		}

		public static TimeSeriesList operator /(double lhs, TimeSeriesList rhs)
		{
			return rhs.Map(s => lhs / s);
		}
	}
}
