﻿using Lispery.Interfaces;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Interfaces;

namespace TimeQL.Values
{
	public class NamedValue : IAnnotatedValue, IComparable
	{
		/// <summary>
		/// The name of the value.
		/// </summary>
		public string Name { get; init; }

		/// <summary>
		/// The value itself.
		/// </summary>
		public double Value { get; init; }

		/// <summary>
		/// Metadata associated with the value.
		/// </summary>
		public ImmutableDictionary<Keyword, object?> Metadata { get; init; }

		/// <summary>
		/// Creates a new named value.
		/// </summary>
		public NamedValue(string name, double value) :
			this(name, value, ImmutableDictionary < Keyword, object?>.Empty)
		{
		}

		/// <summary>
		/// Creates a new named value.
		/// </summary>
		public NamedValue(string name, double value, ImmutableDictionary<Keyword, object?> metadata)
		{
			Name = name;
			Value = value;
			Metadata = metadata; 
		}

		/// <summary>
		/// Returns a new time series with the given name.
		/// </summary>
		public NamedValue SetName(string name)
		{
			return new NamedValue(name, Value, Metadata);
		}

		/// <summary>
		/// Returns a new time series with a name selected from the metadata.
		/// </summary>
		public NamedValue SetName(Keyword key)
		{
			return SetName(Metadata[key]?.ToString() ?? "nil");
		}

		/// <summary>
		/// Returns a new time series with a name selected from combined metadata.
		/// </summary>
		public NamedValue SetName(ISequence seq)
		{
			return SetName(Util.Metadata.Select(Metadata, seq));
		}

		/// <summary>
		/// Returns a new named value with the given metadata set.
		/// </summary>
		public NamedValue SetMeta(Keyword key, object? value)
		{
			return new NamedValue(Name, Value, Metadata.SetItem(key, value));
		}

		/// <summary>
		/// Combines two named values.
		/// </summary>
		public NamedValue Combine(Func<double, double, double> fun, NamedValue rhs)
		{
			return new NamedValue(
				Name == rhs.Name ? Name : "result",
				fun(Value, rhs.Value),
				Util.Metadata.Join(this, rhs));
		}

		public override string ToString()
		{
			return $"NamedValue \"{Name}\" {Value.ToString("0.00", CultureInfo.InvariantCulture)}";
		}

		public int CompareTo(object? obj)
		{
			if (obj is not NamedValue value)
			{
				return 1;
			}

			return Name.CompareTo(value.Name);
		}

		public static NamedValue operator +(NamedValue lhs, NamedValue rhs)
		{
			return lhs.Combine((a, b) => a + b, rhs);
		}

		public static NamedValue operator +(NamedValue lhs, double rhs)
		{
			return new NamedValue(lhs.Name, lhs.Value + rhs, lhs.Metadata);
		}

		public static NamedValue operator +(double lhs, NamedValue rhs)
		{
			return new NamedValue(rhs.Name, lhs + rhs.Value, rhs.Metadata);
		}

		public static NamedValue operator -(NamedValue lhs, NamedValue rhs)
		{
			return lhs.Combine((a, b) => a - b, rhs);
		}

		public static NamedValue operator -(NamedValue lhs, double rhs)
		{
			return new NamedValue(lhs.Name, lhs.Value - rhs, lhs.Metadata);
		}

		public static NamedValue operator -(double lhs, NamedValue rhs)
		{
			return new NamedValue(rhs.Name, lhs - rhs.Value, rhs.Metadata);
		}

		public static NamedValue operator *(NamedValue lhs, NamedValue rhs)
		{
			return lhs.Combine((a, b) => a * b, rhs);
		}

		public static NamedValue operator *(NamedValue lhs, double rhs)
		{
			return new NamedValue(lhs.Name, lhs.Value * rhs, lhs.Metadata);
		}

		public static NamedValue operator *(double lhs, NamedValue rhs)
		{
			return new NamedValue(rhs.Name, lhs * rhs.Value, rhs.Metadata);
		}

		public static NamedValue operator /(NamedValue lhs, NamedValue rhs)
		{
			return lhs.Combine((a, b) => a / b, rhs);
		}

		public static NamedValue operator /(NamedValue lhs, double rhs)
		{
			return new NamedValue(lhs.Name, lhs.Value / rhs, lhs.Metadata);
		}

		public static NamedValue operator /(double lhs, NamedValue rhs)
		{
			return new NamedValue(rhs.Name, lhs / rhs.Value, rhs.Metadata);
		}
	}
}
