﻿using Lispery;
using Lispery.Interfaces;
using Lispery.Values;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQL.Values
{
	public class NamedValueList : ISequence, IFormatable, IEnumerable<NamedValue>
	{
		public ImmutableList<NamedValue> Entries { get; init; }

		public NamedValueList(params NamedValue[] entries) :
			this(ImmutableList.CreateRange(entries))
		{
		}

		public NamedValueList(IEnumerable<NamedValue> entries) :
			this(ImmutableList.CreateRange(entries))
		{
		}

		public NamedValueList(ImmutableList<NamedValue> entries)
		{
			Entries = entries;
		}

		/// <summary>
		/// Returns a new time series list with the given name.
		/// </summary>
		public NamedValueList SetName(string name)
		{
			return Map(n => n.SetName(name)).Sort();
		}

		/// <summary>
		/// Returns a new time series list with a name selected from the metadata.
		/// </summary>
		public NamedValueList SetName(Keyword key)
		{
			return Map(n => n.SetName(key)).Sort();
		}

		/// <summary>
		/// Returns a new time series list with a name selected from combined metadata.
		/// </summary>
		public NamedValueList SetName(ISequence seq)
		{
			return Map(n => n.SetName(seq)).Sort();
		}

		/// <summary>
		/// Returns a new time series list with the given metadata set.
		/// </summary>
		public NamedValueList SetMeta(Keyword key, object? value)
		{
			return Map(n => n.SetMeta(key, value));
		}

		public NamedValueList Map(Func<NamedValue, NamedValue> fun)
		{
			var builder = ImmutableList.CreateBuilder<NamedValue>();

			foreach (var entry in Entries)
			{
				builder.Add(fun(entry));
			}

			return new NamedValueList(builder.ToImmutable());
		}

		public IEnumerable<object?> GetEntries()
		{
			return Entries;
		}

		public int Count()
		{
			return Entries.Count;
		}

		public void Format(TextWriter writer, Formatter formatter)
		{
			writer.Write("<NamedValueList");

			foreach (var item in Entries)
			{
				writer.Write(" [\"");
				writer.Write(item.Name);
				writer.Write("\" ");
				writer.Write(item.Value.ToString("0.00", CultureInfo.InvariantCulture));
				writer.Write(']');
			}

			writer.Write('>');
		}

		public NamedValueList Match(Func<NamedValue, NamedValue, NamedValue> fun, NamedValueList rhs)
		{
			var lhsDict = CreateNameDictionary(this);
			var rhsDict = CreateNameDictionary(rhs);
			var builder = ImmutableList.CreateBuilder<NamedValue>();

			foreach (var entry in lhsDict)
			{
				if (rhsDict.TryGetValue(entry.Key, out NamedValue? rhsValue))
				{
					builder.Add(fun(entry.Value, rhsValue));
				}
			}

			return new NamedValueList(builder.ToImmutable());
		}

		public NamedValueList Filter(Func<NamedValue, bool> predicate)
		{
			var builder = ImmutableList.CreateBuilder<NamedValue>();

			foreach (var entry in Entries)
			{
				if (predicate(entry))
				{
					builder.Add(entry);
				}
			}

			return new NamedValueList(builder.ToImmutable());
		}

		private static Dictionary<string, NamedValue> CreateNameDictionary(NamedValueList list)
		{
			Dictionary<string, NamedValue> series = new();

			foreach (var item in list)
			{
				series.Add(item.Name, item);
			}

			return series;
		}

		public IEnumerator<NamedValue> GetEnumerator()
		{
			return Entries.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public NamedValueList Sort(SortOrder order = SortOrder.Ascending)
		{
			if (order == SortOrder.Descending)
			{
				return new NamedValueList(Entries.OrderByDescending(s => s.Name));
			}

			return new NamedValueList(Entries.OrderBy(s => s.Name));
		}

		object? ISupportSort.Sort(SortOrder order)
		{
			return Sort(order);
		}

		public static NamedValueList operator +(NamedValueList lhs, NamedValueList rhs)
		{
			return lhs.Match((a, b) => a + b, rhs);
		}

		public static NamedValueList operator +(NamedValueList lhs, NamedValue rhs)
		{
			return lhs.Map(s => s + rhs);
		}

		public static NamedValueList operator +(NamedValueList lhs, double rhs)
		{
			return lhs.Map(s => s + rhs);
		}

		public static NamedValueList operator +(NamedValue lhs, NamedValueList rhs)
		{
			return rhs.Map(s => lhs + s);
		}

		public static NamedValueList operator +(double lhs, NamedValueList rhs)
		{
			return rhs.Map(s => lhs + s);
		}

		public static NamedValueList operator -(NamedValueList lhs, NamedValueList rhs)
		{
			return lhs.Match((a, b) => a - b, rhs);
		}

		public static NamedValueList operator -(NamedValueList lhs, NamedValue rhs)
		{
			return lhs.Map(s => s - rhs);
		}

		public static NamedValueList operator -(NamedValueList lhs, double rhs)
		{
			return lhs.Map(s => s - rhs);
		}

		public static NamedValueList operator -(NamedValue lhs, NamedValueList rhs)
		{
			return rhs.Map(s => lhs - s);
		}

		public static NamedValueList operator -(double lhs, NamedValueList rhs)
		{
			return rhs.Map(s => lhs - s);
		}

		public static NamedValueList operator *(NamedValueList lhs, NamedValueList rhs)
		{
			return lhs.Match((a, b) => a * b, rhs);
		}

		public static NamedValueList operator *(NamedValueList lhs, NamedValue rhs)
		{
			return lhs.Map(s => s * rhs);
		}

		public static NamedValueList operator *(NamedValueList lhs, double rhs)
		{
			return lhs.Map(s => s * rhs);
		}

		public static NamedValueList operator *(NamedValue lhs, NamedValueList rhs)
		{
			return rhs.Map(s => lhs * s);
		}

		public static NamedValueList operator *(double lhs, NamedValueList rhs)
		{
			return rhs.Map(s => lhs * s);
		}

		public static NamedValueList operator /(NamedValueList lhs, NamedValueList rhs)
		{
			return lhs.Match((a, b) => a / b, rhs);
		}

		public static NamedValueList operator /(NamedValueList lhs, NamedValue rhs)
		{
			return lhs.Map(s => s / rhs);
		}

		public static NamedValueList operator /(NamedValueList lhs, double rhs)
		{
			return lhs.Map(s => s / rhs);
		}

		public static NamedValueList operator /(NamedValue lhs, NamedValueList rhs)
		{
			return rhs.Map(s => lhs / s);
		}

		public static NamedValueList operator /(double lhs, NamedValueList rhs)
		{
			return rhs.Map(s => lhs / s);
		}
	}
}
