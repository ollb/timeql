﻿using Lispery;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TimeQL.Interfaces;
using TimeQL.Values;

namespace TimeQL
{
	/// <summary>
	/// Parses filter expressions.
	/// </summary>
	public class FilterExpressionParser
	{
		private readonly Keyword kwNot;
		private readonly Keyword kwAnd;
		private readonly Keyword kwOr;
		private readonly Keyword kwMatch;

		/// <summary>
		/// Creates a new filter expression parser.
		/// </summary>
		public FilterExpressionParser(KeywordTable keywordTable)
		{
			kwNot = keywordTable.Get("not");
			kwAnd = keywordTable.Get("and");
			kwOr = keywordTable.Get("or");
			kwMatch = keywordTable.Get("match");
		}

		/// <summary>
		/// Parses the given filter expression.
		/// </summary>
		public Func<IAnnotatedValue, bool> Parse(object expression)
		{
			return Parse(expression, null);
		}

		/// <summary>
		/// Parses the given filter expression.
		/// </summary>
		private Func<IAnnotatedValue, bool> Parse(object expression, Keyword? metaKey)
		{
			ArgumentNullException.ThrowIfNull(expression);

			switch (expression)
			{
				case Regex rex: // Check if the regex matches.
					return s => rex.IsMatch(GetMetaOrName(s, metaKey)?.ToString() ?? "nil");

				case ISequence seq:
					return Parse(seq, metaKey);

				default:
					// Try to convert to a function and call it with the series
					// or the metadata, depending on the context.
					if (Conversion.ToCallableOrNull(expression) is IFunction fun)
					{
						return s => Conversion.To<bool>(fun.Call(metaKey != null ? s.Metadata[metaKey] : s));
					}

					return s => EqualityComparer<object?>.Default.Equals(expression, GetMetaOrName(s, metaKey));
			}
		}

		/// <summary>
		/// Parses the given filter expression sequence.
		/// </summary>
		private Func<IAnnotatedValue, bool> Parse(ISequence seq, Keyword? metaKey)
		{
			int count = seq.Count();

			if (count < 1)
			{
				throw new ArgumentException("Invalid filter expression");
			}

			var key = Conversion.To<Keyword>(seq.First());

			if (key == kwNot)
			{
				if (count > 2)
				{
					var newSeq = new Vector(seq.Rest().GetEntries());
					var pred = Parse(newSeq, metaKey);
					return v => !pred(v);
				}
				else
				{
					var expr = seq.Nth(1);
					var pred = Parse(expr!, metaKey);
					return v => !pred(v);
				}
			}
			else if (key == kwAnd)
			{
				return v =>
				{
					foreach (var expr in seq.Rest().GetEntries())
					{
						if (!Parse(expr!, metaKey)(v))
							return false;
					}

					return true;
				};
			}
			else if (key == kwOr)
			{
				return v =>
				{
					foreach (var expr in seq.Rest().GetEntries())
					{
						if (Parse(expr!, metaKey)(v))
							return true;
					}

					return false;
				};
			}
			else if (key == kwMatch)
			{
				if (count != 3)
				{
					throw new ArgumentException("Invalid filter expression");
				}

				var keyword = Conversion.To<Keyword>(seq.GetEntries().ElementAt(1));
				return Parse(seq.Nth(2)!, keyword);
			}
			else
			{
				if (count != 2)
				{
					throw new ArgumentException("Invalid filter expression");
				}

				var keyword = Conversion.To<Keyword>(key);
				return Parse(seq.Nth(1)!, keyword);
			}
		}

		/// <summary>
		/// Retrieves the name or metadata, depending on <c>metaKey</c>.
		/// </summary>
		private static object? GetMetaOrName(IAnnotatedValue value, Keyword? metaKey)
		{
			return metaKey == null ? value.Name : value.Metadata[metaKey];
		}
	}
}
