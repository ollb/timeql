﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TimeQL.Util;

namespace TimeQL.Graphite
{
	public class GraphiteConfig
	{
		private readonly JsonElement root;

		public GraphiteConfig(JsonElement root)
		{
			this.root = root;
		}

		public string Name
		{
			get { return root.GetString("name"); }
		}

		public Uri Uri
		{
			get { return new Uri(root.GetString("uri", "http://localhost:8542")); }
		}
	}
}
