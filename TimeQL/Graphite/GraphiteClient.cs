﻿using Lispery;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TimeQL.Util;
using TimeQL.Values;

namespace TimeQL.Graphite
{
	internal class GraphiteClient
	{
		private readonly Config config;
		private readonly QueryCache cache;
		private readonly HttpClient client;

		private class TimeSeriesBuilder
		{
			public ImmutableSortedDictionary<DateTime, double>.Builder DataPointBuilder;
			public ImmutableDictionary<Keyword, object?>.Builder MetadataBuilder;

			public TimeSeriesBuilder()
			{
				DataPointBuilder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
				MetadataBuilder = ImmutableDictionary.CreateBuilder<Keyword, object?>();
			}
		}

		public GraphiteClient(Config config, QueryCache cache)
		{
			this.config = config;
			this.cache = cache;

			HttpClientHandler handler = new();
			handler.AutomaticDecompression = DecompressionMethods.All;

			client = new HttpClient(handler);
		}

		public TimeSeriesList Fetch(
			KeywordTable keywordTable,
			CommonQueryOptions options,
			string query,
			string schema,
			string groupFn)
		{
			if (!config.GraphiteEndpoints.Any())
			{
				throw new InvalidOperationException("No graphite endpoints configured.");
			}

			GraphiteConfig endpointConfig = config.GraphiteEndpoints.First();

			if (options.Endpoint != null)
			{
				endpointConfig = config.GraphiteEndpoints.Where(d => d.Name == options.Endpoint).First();
			}

			// Create a key for trying to look up the result in the cache.
			string cacheKey = CreateKey(options, query, schema, groupFn);

			if (options.Cache && cache.TryLookup(cacheKey, keywordTable, out TimeSeriesList? list))
			{
				return list;
			}

			long startTimestamp = new DateTimeOffset(options.WindowStart).ToUnixTimeSeconds();
			long stopTimestamp = new DateTimeOffset(options.WindowStop).ToUnixTimeSeconds();

			query = Regex.Replace(query, @"[()]", "_");
			query = $"consolidateBy({query}, '{groupFn}')";

			StringBuilder sb = new();
			sb.Append("format=json&target=");
			sb.Append(Uri.EscapeDataString(query));
			sb.Append("&from=");
			sb.Append(startTimestamp);
			sb.Append("&until=");
			sb.Append(stopTimestamp);
			sb.Append("&maxDataPoints=");
			sb.Append(options.SampleCount);

			if (!endpointConfig.Uri.AbsolutePath.EndsWith('/'))
			{
				throw new InvalidOperationException("URI does not specify a path.");
			}

			UriBuilder uriBuilder = new();
			uriBuilder.Scheme = endpointConfig.Uri.Scheme;
			uriBuilder.Host = endpointConfig.Uri.Host;
			uriBuilder.Port = endpointConfig.Uri.Port;
			uriBuilder.Path = endpointConfig.Uri.AbsolutePath + "render";
			uriBuilder.Query = sb.ToString();

			HttpRequestMessage request = new();
			request.Method = HttpMethod.Get;
			request.RequestUri = uriBuilder.Uri;
			request.Headers.Add("Accept", "application/json");
			request.Headers.Add("Accept-Encoding", "gzip");

			var response = client.Send(request);

			if ((int)response.StatusCode < 200 || (int)response.StatusCode >= 300)
			{
				throw new InvalidOperationException(response.ReasonPhrase);
			}

			using Stream stream = response.Content.ReadAsStream();
			JsonDocument doc = JsonDocument.Parse(stream);

			var kwName = keywordTable.Get("name");
			string[] schemaParts = schema.Length == 0 ? Array.Empty<string>() : schema.Split('.');
			var builder = ImmutableList.CreateBuilder<TimeSeries>();
			Regex queryRegex = new(@"^consolidateBy\((?<name>.*),[^,]+\)$");

			foreach (var seriesElement in doc.RootElement.EnumerateArray())
			{
				var metadataBuilder = ImmutableDictionary.CreateBuilder<Keyword, object?>();
				string name = seriesElement.GetString("target");

				// Try to extract the actual name and split it according to the schema.
				Match match = queryRegex.Match(name);

				if (match.Success)
				{
					name = match.Groups["name"].Value;
					string[] parts = name.Split('.');

					for (int i = 0; i < parts.Length; i++)
					{
						string schemaPart = "_";

						if (i < schemaParts.Length)
						{
							schemaPart = schemaParts[i];
						}

						var kwNode = keywordTable.Get($"node-{i}");
						metadataBuilder[kwNode] = parts[i];

						if (schemaPart != "_")
						{
							metadataBuilder[keywordTable.Get(schemaPart)] = parts[i];
						}
					}
				}

				// Read the datapoints.
				var datapointBuilder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();

				foreach (var datapointElement in seriesElement.GetProperty("datapoints").EnumerateArray())
				{
					var array = datapointElement.EnumerateArray().ToArray();

					if (array.Length != 2)
					{
						throw new FormatException("Invalid datapoint format.");
					}

					if (array[0].ValueKind != JsonValueKind.Null)
					{
						double value = array[0].GetDouble();

						var datetimeOffset = DateTimeOffset.FromUnixTimeSeconds(array[1].GetInt64());
						DateTime time = datetimeOffset.UtcDateTime;

						datapointBuilder[time] = value;
					}
				}

				builder.Add(new TimeSeries(
					name,
					datapointBuilder.ToImmutable(),
					metadataBuilder.ToImmutable()));
			}

			var resultList = new TimeSeriesList(builder.ToImmutable());
			cache.Insert(cacheKey, resultList, options.Resolution);
			return resultList;
		}

		/// <summary>
		/// Creates a key string for the cache.
		/// </summary>
		private static string CreateKey(
			CommonQueryOptions options,
			string query,
			string schema,
			string groupFn)
		{
			StringBuilder sb = new();
			sb.Append("graphite|");
			sb.Append(options.Endpoint);
			sb.Append('|');
			sb.Append(options.WindowStart.Ticks);
			sb.Append('|');
			sb.Append(options.WindowStop.Ticks);
			sb.Append('|');
			sb.Append(options.Resolution.Ticks);
			sb.Append('|');
			sb.Append(query);
			sb.Append('|');
			sb.Append(schema);
			sb.Append('|');
			sb.Append(groupFn);
			return sb.ToString();
		}
	}
}
