﻿using Lispery;
using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Util;

namespace TimeQL.Graphite
{
	public class GraphiteLib
	{
		private readonly Keyword kwSchema;

		private readonly KnownIdentifiers ki;
		private readonly Lispery.Environment env;
		private readonly GraphiteClient client;

		public GraphiteLib(Config config, QueryCache queryCache, KnownIdentifiers ki, Lispery.Environment env)
		{
			this.ki = ki;
			this.env = env;
			this.client = new GraphiteClient(config, queryCache);

			kwSchema = env.GetKeyword("schema");

			env.DefineFunction<string, object>("graphite-query", GraphiteQuery!);

			env.DefineFunction("graphite-endpoints", () => new Vector(
				config.GraphiteEndpoints.Select(e => e.Name)));
		}

		private object? GraphiteQuery(string query, object[] args)
		{
			var options = new Options(args);
			var commonOptions = new CommonQueryOptions(options, ki, env);
			string schema = options.Get<string>(kwSchema, string.Empty);

			// TODO: make consolidation configurable
			return commonOptions.Postprocess(client.Fetch(
				env.KeywordTable,
				commonOptions,
				query,
				schema,
				"average"));
		}
	}
}
