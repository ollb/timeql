﻿using Lispery;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Values;

namespace TimeQL
{
	public class QueryCache
	{
		private readonly Dictionary<string, QueryCacheEntry> entries = new();
		private readonly object lockObj = new();
		private readonly Stopwatch sw = new();

		private readonly Config config;
		private readonly TimeSpan maxTtlInterval;
		private readonly TimeSpan cleanupInterval;
		private TimeSpan lastCleanup;

		public int EntryCount
		{
			get { lock (lockObj) { return entries.Count; } }
		}

		public long Size
		{
			get { lock (lockObj) { return entries.Sum(e => e.Value.Size); } }
		}

		public long UncompressedSize
		{
			get { lock (lockObj) { return entries.Sum(e => e.Value.UncompressedSize); } }
		}

		public double AverageEntrySize
		{
			get { return EntryCount > 0 ? Size / EntryCount : 0; }
		}

		/// <summary>
		/// Returns the compression ratio.
		/// </summary>
		public double CompressionRatio
		{
			get { return (double)Size / UncompressedSize; }
		}

		public QueryCache(Config config)
		{
			this.config = config;

			maxTtlInterval = TimeSpan.FromMinutes(config.CacheMaxTtlInterval);
			cleanupInterval = TimeSpan.FromMinutes(config.CacheCleanupInterval);

			sw.Start();
		}

		public bool TryLookup(string key, KeywordTable keywordTable, [NotNullWhen(true)] out TimeSeriesList? result)
		{
			lock (lockObj)
			{
				Cleanup();

				if (!entries.TryGetValue(key, out QueryCacheEntry? entry))
				{
					result = null;
					return false;
				}

				if (sw.Elapsed >= entry.ExpireTime)
				{
					entries.Remove(key);
					result = null;
					return false;
				}

				result = entry.Load(keywordTable);
				return true;
			}
		}

		public void Insert(string key, TimeSeriesList list, TimeSpan ttl)
		{
			if (ttl > maxTtlInterval)
			{
				ttl = maxTtlInterval;
			}

			lock (lockObj)
			{
				QueryCacheEntry entry = new(sw.Elapsed + ttl);
				entry.Save(list, config);
				entries[key] = entry;
			}
		}

		private void Cleanup()
		{
			if (sw.Elapsed - lastCleanup < cleanupInterval)
			{
				return;
			}

			lock (lockObj)
			{
				HashSet<string> removeKeys = new();

				foreach (var entry in entries)
				{
					if (entry.Value.ExpireTime >= sw.Elapsed)
					{
						removeKeys.Add(entry.Key);
					}
				}

				foreach (var key in removeKeys)
				{
					entries.Remove(key);
				}

				lastCleanup = sw.Elapsed;
				Console.WriteLine("Cleanup up {0} cache entries.", removeKeys.Count);
			}
		}

		public void Clear()
		{
			lock (lockObj)
			{
				entries.Clear();
			}
		}
	}
}
