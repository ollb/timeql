# v0.1.1 (2022-04-01)
- Added support for Prometheus.
- Added `per-hour`, `per-minute` and `per-second`.
- Renamed `sample-interval` to `resolution`.
- Added `max-resolution` options.
- Added common query options class to simplify data sources.
- Fixed query options that affect the query window.
- Added some initial unit tests.

# v0.1.0 (2022-03-23)
- Initial release.
