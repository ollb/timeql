﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TimeQLTest
{
	public class ArithmeticsTest : BaseTest
	{
		[Fact]
		public void TestAddSeriesDouble()
		{
			int[] values = Quantize(Evaluate("(values (+ 5 sin-series))"));
			Assert.Equal(500, values[0]);
			Assert.Equal(600, values[2]);
			Assert.Equal(500, values[4]);
			Assert.Equal(400, values[6]);
			Assert.Equal(500, values[8]);

			values = Quantize(Evaluate("(values (+ sin-series 5))"));
			Assert.Equal(500, values[0]);
			Assert.Equal(600, values[2]);
			Assert.Equal(500, values[4]);
			Assert.Equal(400, values[6]);
			Assert.Equal(500, values[8]);
		}

		[Fact]
		public void TestSubtractSeriesDouble()
		{
			int[] values = Quantize(Evaluate("(values (- 5 sin-series))"));
			Assert.Equal(500, values[0]);
			Assert.Equal(400, values[2]);
			Assert.Equal(500, values[4]);
			Assert.Equal(600, values[6]);
			Assert.Equal(500, values[8]);

			values = Quantize(Evaluate("(values (- sin-series 5))"));
			Assert.Equal(-500, values[0]);
			Assert.Equal(-400, values[2]);
			Assert.Equal(-500, values[4]);
			Assert.Equal(-600, values[6]);
			Assert.Equal(-500, values[8]);
		}

		[Fact]
		public void TestMultiplySeriesDouble()
		{
			int[] values = Quantize(Evaluate("(values (* 3 sin-series))"));
			Assert.Equal(0, values[0]);
			Assert.Equal(300, values[2]);
			Assert.Equal(0, values[4]);
			Assert.Equal(-300, values[6]);
			Assert.Equal(0, values[8]);

			values = Quantize(Evaluate("(values (* sin-series 3))"));
			Assert.Equal(0, values[0]);
			Assert.Equal(300, values[2]);
			Assert.Equal(0, values[4]);
			Assert.Equal(-300, values[6]);
			Assert.Equal(0, values[8]);
		}

		[Fact]
		public void TestDivideSeriesDouble()
		{
			int[] values = Quantize(Evaluate("(values (/ 3 (+ 2 sin-series)))"));
			Assert.Equal(150, values[0]);
			Assert.Equal(100, values[2]);
			Assert.Equal(150, values[4]);
			Assert.Equal(300, values[6]);
			Assert.Equal(150, values[8]);

			values = Quantize(Evaluate("(values (/ (+ sin-series 2) 4))"));
			Assert.Equal(50, values[0]);
			Assert.Equal(75, values[2]);
			Assert.Equal(50, values[4]);
			Assert.Equal(25, values[6]);
			Assert.Equal(50, values[8]);
		}
	}
}
