using Lispery;
using Lispery.Interfaces;
using System;
using System.Linq;
using System.Text.Json;
using TimeQL;
using TimeQL.Libraries;
using Xunit;

namespace TimeQLTest
{
	public class ExampleDataTest : BaseTest
	{
		[Fact]
		public void TestExampleDataOne()
		{
			int[] values = Quantize(Evaluate("(values one-series)"));
			Assert.True(values.All(v => v == 100));
		}

		[Fact]
		public void TestExampleDataLinear()
		{
			int[] values = Quantize(Evaluate("(values linear-series)"));
			Assert.Equal(0, values[0]);
			Assert.Equal(25, values[2]);
			Assert.Equal(50, values[4]);
			Assert.Equal(75, values[6]);
			Assert.Equal(100, values[8]);
		}

		[Fact]
		public void TestExampleDataSin()
		{
			int[] values = Quantize(Evaluate("(values sin-series)"));
			Assert.Equal(0, values[0]);
			Assert.Equal(71, values[1]);
			Assert.Equal(100, values[2]);
			Assert.Equal(71, values[3]);
			Assert.Equal(0, values[4]);
			Assert.Equal(-71, values[5]);
			Assert.Equal(-100, values[6]);
			Assert.Equal(-71, values[7]);
			Assert.Equal(0, values[8]);
		}

		[Fact]
		public void TestExampleDataCos()
		{
			int[] values = Quantize(Evaluate("(values cos-series)"));
			Assert.Equal(100, values[0]);
			Assert.Equal(71, values[1]);
			Assert.Equal(0, values[2]);
			Assert.Equal(-71, values[3]);
			Assert.Equal(-100, values[4]);
			Assert.Equal(-71, values[5]);
			Assert.Equal(0, values[6]);
			Assert.Equal(71, values[7]);
			Assert.Equal(100, values[8]);
		}
	}
}