﻿using Lispery;
using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TimeQL;
using TimeQL.Libraries;
using Xunit;

namespace TimeQLTest
{
	public class BaseTest
	{
		protected readonly Interpreter interpreter;
		protected readonly Lispery.Environment env;
		protected readonly Config config;
		protected readonly QueryCache cache;
		protected readonly TimeQLLib library;

		public BaseTest()
		{
			interpreter = new();
			config = new(JsonDocument.Parse("{}"));
			cache = new(config);
			library = new(config, cache, interpreter.Environment);
			env = interpreter.Environment;

			env.Define("*resolution*", TimeSpan.FromHours(3));
			env.Define("*max-resolution*", null);
			env.Define("*window-start*", new DateTime(2020, 1, 1, 12, 0, 0, DateTimeKind.Utc));
			env.Define("*window-stop*", new DateTime(2020, 1, 2, 12, 0, 0, DateTimeKind.Utc));

			interpreter.Evaluate("(load-example-data)");
		}

		protected static int[] Quantize(object? value)
		{
			var seq = Assert.IsAssignableFrom<ISequence>(value);
			Assert.True(seq.GetEntries().All(v => v is double));
			return seq.GetEntries().Select(v => (int)Math.Round((double)v! * 100, 0)).ToArray();
		}

		protected object? Evaluate(string query)
		{
			return interpreter.Evaluate(query);
		}
	}
}
