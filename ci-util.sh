#!/bin/sh
mdbook_version=0.4.15

die() {
	echo "$@" >&2
	exit 1
}

try() {
	"$@" || die "failed command: $*"
}

print_usage() {
	echo "`basename $0` COMMAND"
	echo
	echo "Commands:"
	echo "  mdbook                  Creates the mdbook."
	echo "  tarball        PROJECT  Creates a tarball."
	echo "  docker         PROJECT  Creates a docker image."
	echo "  tarball_upload PROJECT  Creates a tarball and uploads it."
	echo "  docker_upload  PROJECT  Creates a docker image and uploads it."
	exit 1
}

has_command() {
	command -v $1 >/dev/null 2>&1
}

pkg_install() {
	if has_command apt-get; then
		try apt-get -y update
		try apt-get -y install $1
	elif has_command apk; then
		try apk add $1
	else
		die "Could not install package $1."
	fi
}

pkg_ensure() {
	pkg=$2
	[ -z ${pkg} ] && pkg=$1

	if ! has_command $1; then
		pkg_install "${pkg}"
	fi
}

load_version() {
	pkg_ensure xmlstarlet
	version=`xmlstarlet sel -t -v "/Project/PropertyGroup/Version" ${project}/${project}.csproj`

	if [ $? -ne 0 ] || [ -z ${version} ]; then
		die "Could not determine version."
	fi
}

load_mdbook() {
	pkg_ensure curl
	md_url=https://github.com/rust-lang/mdBook/releases/download
	md_arch=x86_64-unknown-linux-gnu
	try mkdir -p mdbook
	try cd mdbook
	try curl -fsL "${md_url}/v${mdbook_version}/mdbook-v${mdbook_version}-${md_arch}.tar.gz" -o mdbook.tar.gz
	try tar xzf mdbook.tar.gz
	export PATH="`pwd`:$PATH"
	try cd ..
}

cmd_tarball() {
	[ $# -eq 1 ] || print_usage

	project=$1
	[ -d ${project} ] || die "Project does not exist."

	load_version

	echo "project: ${project}"
	echo "version: ${version}"

	outdir=`realpath "${project}/bin/Release"`
	tarname="${project}-${version}.tar.gz"
	tarpath="${outdir}/${tarname}"

	echo "tarname: ${tarname}"
	echo "tarpath: ${tarpath}"

	try cp CHANGELOG.md LICENSE README.md THIRD-PARTY-NOTICES.md "${outdir}/net6.0/"
	try tar -C "${outdir}" -czf "${tarpath}" "net6.0" --transform="s/^net6.0/${project}-${version}/"
}

cmd_tarball_upload() {
	[ $# -eq 1 ] || print_usage
	project=$1

	[ -z $CI_PROJECT_ID ] && die "CI_PROJECT_ID undefined."
	[ -z $CI_API_V4_URL ] && die "CI_API_V4_URL undefined."
	[ -z $CI_JOB_TOKEN ] && die "CI_JOB_TOKEN undefined."

	cmd_tarball "${project}"

	echo "project_id: ${CI_PROJECT_ID}"
	echo "api_url: ${CI_API_V4_URL}"

	pkg_ensure curl
	try curl -fs --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${tarpath}" \
		"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${project}/${version}/${tarname}"
}

cmd_docker() {
	[ $# -eq 1 ] || print_usage

	project=$1
	registry_image=$2

	case "${project}" in
		TimeQLREPL) docker_tag="timeql-repl" ;;
		TimeQLServer) docker_tag="timeql-server" ;;
		*) die "Could not determine docker tag."
	esac

	load_version
	docker_tag="${docker_tag}:${version}"

	if [ ! -z ${CI_REGISTRY_IMAGE} ]; then
		docker_tag="${CI_REGISTRY_IMAGE}/${docker_tag}"
	fi

	echo "docker_tag: ${docker_tag}"

	try docker build -t "${docker_tag}" "${project}/"
}

cmd_docker_upload() {
	[ $# -eq 1 ] || print_usage
	project=$1

	[ -z $CI_REGISTRY ] && die "CI_REGISTRY undefined."
	[ -z $CI_REGISTRY_IMAGE ] && die "CI_REGISTRY_IMAGE undefined."
	[ -z $CI_REGISTRY_USER ] && die "CI_REGISTRY_USER undefined."
	[ -z $CI_REGISTRY_PASSWORD ] && die "CI_REGISTRY_PASSWORD undefined."

	cmd_docker "${project}"

	echo "registry: ${CI_REGISTRY}"
	echo "registry_image: ${CI_REGISTRY_IMAGE}"

	try docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
	try docker push "${docker_tag}"
}

cmd_mdbook() {
	[ $# -eq 0 ] || print_usage

	if ! has_command mdbook; then
		load_mdbook
	fi

	project=TimeQL
	load_version

	try mdbook build TimeQL/Doc -d "../../public"
	try find public/ -type f -name "*.html" -exec sed -i -e "s/\[\[:VERSION:\]\]/${version}/g" {} +
}

cmd_mdbook_upload() {
	[ $# -eq 0 ] || print_usage

	[ -z $CI_PROJECT_ID ] && die "CI_PROJECT_ID undefined."
	[ -z $CI_API_V4_URL ] && die "CI_API_V4_URL undefined."
	[ -z $CI_JOB_TOKEN ] && die "CI_JOB_TOKEN undefined."

	cmd_mdbook

	echo "project_id: ${CI_PROJECT_ID}"
	echo "api_url: ${CI_API_V4_URL}"

	tarname="${project}-${version}-doc.tar.gz"
	try tar -czf "${tarname}" public

	pkg_ensure curl
	try curl -fs --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${tarname}" \
		"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${project}/${version}/${tarname}"
}

cmd=$1
shift

case "${cmd}" in
	tarball|docker|mdbook|tarball_upload|docker_upload|mdbook_upload)
		cmd_"${cmd}" "$@"
		;;

	*)
		print_usage "$@"
		;;
esac

