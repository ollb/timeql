﻿using Lispery.Interfaces;
using Lispery.Values;
using LisperyREPL;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TimeQL;
using TimeQL.Libraries;
using TimeQL.Values;

namespace TimeQLREPL
{
	[SuppressMessage("CodeQuality", "IDE0052")]
	internal class REPL : LisperyREPL.REPL
	{
		private readonly Config config = new();
		private QueryCache? queryCache;
		private TimeQLLib? lib;

		public REPL()
		{
			var assemblyName = Assembly.GetCallingAssembly().GetName();
			Terminal.WriteLine($"{assemblyName.Name} {assemblyName.Version}");
			Terminal.WriteLine($"Config: {config.Filename ?? "none"}.");
			Terminal.WriteLine();
		}

		public override void Reset()
		{
			base.Reset();

			queryCache = new QueryCache(config);
			lib = new TimeQLLib(config, queryCache, Environment);

			Environment.Define("*window-size*", TimeSpan.FromHours(24));
			Environment.Define("*sample-count*", (long)12);

			Environment.DefineFunction<TimeSeries>("plot", PlotSeries!);

			Environment.DefineFunction("clear-cache", () => {
				queryCache.Clear();
				return new EmptyResult();
			});
		}

		protected override void PrepareQueryEnvironment()
		{
			DateTime now = DateTime.Now;
			TimeSpan timeWindow = (TimeSpan)Environment.Resolve("*window-size*")!;
			long sampleCount = (long)Environment.Resolve("*sample-count*")!;
			TimeSpan resolution = timeWindow / sampleCount;

			Environment.Define("*window-start*", now - timeWindow);
			Environment.Define("*window-stop*", now);
			Environment.Define("*resolution*", resolution);
			Environment.Define("*max-resolution*", null);

			foreach (string filename in config.Includes)
			{
				string code = File.ReadAllText(filename);
				Interpreter.Evaluate(code);
			}
		}

		protected override void PrintResult(object? result)
		{
			switch (result)
			{
				case TimeSeries series: PrintResult(series); break;
				case TimeSeriesList list: PrintResult(list); break;
				case ISequence seq: PrintResult(seq); break;
				default: base.PrintResult(result); break;
			}			
		}

		private void PrintResult(ISequence seq)
		{
			if (seq.Count() > 0 && seq.GetEntries().All(e => e is TimeSeries))
			{
				PrintResult(new TimeSeriesList(seq.GetEntries().Cast<TimeSeries>()));
			}
			else
			{
				base.PrintResult(seq);
			}
		}

		private static void PrintResult(TimeSeries series)
		{
			PrintResult(new TimeSeriesList(series));
		}

		private static void PrintResult(TimeSeriesList list)
		{
			TimeTable table = new();

			foreach (var series in list)
			{
				table.AddColumn(series.Name);
			}

			var keys = list.SelectMany(l => l.DataPoints.Keys).Distinct().OrderBy(v => v).ToArray();

			foreach (var key in keys)
			{
				table.AddValues(key, GetRowValues(key, list));
			}

			table.Print();
		}

		private static IEnumerable<string> GetRowValues(DateTime time, IEnumerable<TimeSeries> list)
		{
			ArgumentNullException.ThrowIfNull(list);

			foreach (var series in list)
			{
				if (series.DataPoints.TryGetValue(time, out var value))
				{
					yield return value.ToString("0.00", CultureInfo.InvariantCulture);
				}
				else
				{
					yield return string.Empty;
				}
			}
		}

		/// <summary>
		/// Quantizes the given value between two extremes.
		/// </summary>
		private static int Quantize(double min, double max, double value, int steps)
		{
			double f = (value - min) / (max - min);
			return (int)(f * steps);
		}

		/// <summary>
		/// Writes a plot of the series to the terminal.
		/// </summary>
		private object? PlotSeries(TimeSeries series)
		{
			double minValue = double.MaxValue;
			double maxValue = double.MinValue;

			foreach (var dataPoint in series!.DataPoints)
			{
				minValue = Math.Min(minValue, dataPoint.Value);
				maxValue = Math.Max(maxValue, dataPoint.Value);
			}

			DateTime startTime = series.DataPoints.First().Key;
			DateTime stopTime = series.DataPoints.Last().Key;

			const int lines = 10;
			const int steps = 5;

			int[] dataArray = new int[Terminal.WindowWidth];

			for (int i = 0; i < dataArray.Length; i++)
			{
				dataArray[i] = -1;
			}

			foreach (var dataPoint in series.DataPoints)
			{
				int index = Quantize(
					startTime.Ticks,
					stopTime.Ticks,
					dataPoint.Key.Ticks,
					dataArray.Length - 1);

				int value = Quantize(
					minValue,
					maxValue,
					dataPoint.Value,
					lines * steps - 1);

				dataArray[index] = value;
			}

			Terminal.SetUnderline(true);
			Terminal.WriteLine(new string(' ', Terminal.WindowWidth));
			Terminal.SetBgColor(0x333333);
			Terminal.SetUnderline(true);
			Terminal.Write(series.Name);
			Terminal.Write(new string(' ', Math.Max(0, Terminal.WindowWidth - series.Name.Length)));
			Terminal.ResetBgColor();
			Terminal.SetUnderline(false);
			Terminal.WriteLine();
			Terminal.SetBgColor(0x222222);

			for (int line = 0; line < lines; line++)
			{
				if (line == lines - 1)
				{
					Terminal.SetUnderline(true);
				}

				for (int x = 0; x < dataArray.Length; x++)
				{
					if (dataArray[x] >= 0 && dataArray[x] / steps == lines - line - 1)
					{
						switch (dataArray[x] % steps)
						{
							case 0: Terminal.Write(','); break;
							case 1: Terminal.Write('.'); break;
							case 2: Terminal.Write('-'); break;
							case 3: Terminal.Write('\''); break;
							case 4: Terminal.Write('´'); break;
						}
					}
					else
					{
						Terminal.Write(' ');
					}
				}

				if (line == lines - 1)
				{
					Terminal.SetUnderline(false);
				}

				Terminal.WriteLine();
			}

			Terminal.ResetColors();

			return new EmptyResult();
		}
	}
}
