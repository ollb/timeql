﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LisperyREPL;

namespace TimeQLREPL
{
	internal class TimeTable
	{
		private record Row(DateTime Time, string[] Cells);

		private const string timeFormat = "yyyy-MM-dd HH:mm:ss";
		private readonly int timeWidth = timeFormat.Length;
		private readonly List<string> columns = new();
		private readonly List<Row> rows = new();

		public void AddColumn(string title)
		{
			columns.Add(title);
		}

		public void AddValues(DateTime time, IEnumerable<string> values)
		{
			if (values.Count() != columns.Count)
			{
				throw new ArgumentException("Invalid number of values provided.");
			}

			rows.Add(new Row(time, values.ToArray()));
		}

		/// <summary>
		/// Calculates the widths of the columns including one separator column.
		/// </summary>
		private ImmutableList<int> GetColumnWidths(bool shortTitles)
		{
			var widths = ImmutableList.CreateBuilder<int>();

			for (int i = 0; i < columns.Count; i++)
			{
				int minWidth = Math.Max(8, shortTitles ? 0 : columns[i].Length);
				int valueWidth = rows.Count > 0 ? rows.Select(r => r.Cells[i].Length).Max() : 0;
				widths.Add(Math.Max(minWidth, valueWidth) + 1);
			}

			return widths.ToImmutable();
		}

		public void Print()
		{
			int freeWidth = Terminal.WindowWidth - timeWidth;
			var widths = GetColumnWidths(false);

			if (widths.Sum() <= freeWidth)
			{
				Print(widths, false);
				return;
			}

			widths = GetColumnWidths(true);

			if (widths.Sum() <= freeWidth)
			{
				widths = ExtendToTotalWidth(widths, freeWidth);
				Print(widths, false);
				return;
			}

			freeWidth -= 3;
			widths = TakeWhile(widths, freeWidth);
			widths = ExtendToTotalWidth(widths, freeWidth);
			Print(widths, true);
		}

		private static ImmutableList<int> TakeWhile(ImmutableList<int> widths, int sum)
		{
			var builder = ImmutableList.CreateBuilder<int>();
			int rest = sum;

			for (int i = 0; i < widths.Count; i++)
			{
				int width = widths[i];

				if (rest < width)
				{
					break;
				}

				builder.Add(width);
				rest -= width;
			}

			return builder.ToImmutable();
		}

		private static ImmutableList<int> ExtendToTotalWidth(ImmutableList<int> widths, int sum)
		{
			while (widths.Sum() < sum)
			{
				widths = ExtendMinimumWidth(widths);
			}

			return widths;
		}

		private static ImmutableList<int> ExtendMinimumWidth(ImmutableList<int> widths)
		{
			int minValue = widths.Min();

			for (int i = 0; i < widths.Count; i++)
			{
				int width = widths[i];

				if (width == minValue)
				{
					return widths.SetItem(i, width + 1);
				}
			}

			throw new InvalidOperationException();
		}

		public void Print(ImmutableList<int> widths, bool shortened)
		{
			int columnCount = widths.Count;

			Terminal.SetUnderline(true);
			Terminal.Write("Timestamp");
			Terminal.SetUnderline(false);

			int offset = timeWidth;

			for (int i = 0; i < columnCount; i++)
			{
				offset += widths[i];
				string title = Shorten(columns[i], widths[i] - 1);

				GotoColumn(offset - title.Length);
				Terminal.SetUnderline(true);
				Terminal.Write(title);
				Terminal.SetUnderline(false);
			}

			if (shortened)
			{
				GotoColumn(offset + 1);
				Terminal.SetUnderline(true);
				Terminal.Write("..");
				Terminal.SetUnderline(false);
			}

			Terminal.WriteLine();

			foreach (var row in rows)
			{
				Terminal.Write(row.Time.ToLocalTime().ToString(timeFormat, CultureInfo.InvariantCulture));

				offset = timeWidth;

				for (int i = 0; i < columnCount; i++)
				{
					offset += widths[i];
					string text = row.Cells[i];

					GotoColumn(offset - text.Length);
					Terminal.Write(text);
				}

				if (shortened)
				{
					GotoColumn(offset + 1);
					Terminal.Write("..");
				}

				Terminal.WriteLine();
			}
		}

		private static void GotoColumn(int column)
		{
			var pos = Terminal.GetAbsolutePosition();
			Terminal.SetPosition(pos with { X = column });
		}

		private static string Shorten(string str, int len)
		{
			if (str.Length <= len)
			{
				return str;
			}

			if (len < 2)
			{
				return new string('.', len);
			}

			int x = len - 2;
			int j = x / 2;
			int i = x - j;

			return $"{str[..i]}..{str[^j..]}";
		}
	}
}
