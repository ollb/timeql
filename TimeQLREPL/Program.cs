﻿using LisperyREPL;
using System.Globalization;
using System.Reflection;
using System.Threading.Tasks.Dataflow;
using TimeQLREPL;

CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;
CultureInfo.CurrentUICulture = CultureInfo.InvariantCulture;

TimeQLREPL.REPL repl = new();

repl.HistoryPath = Path.Combine(
	Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
	".timeql.history");

repl.ImportFiles(args);
repl.Run();
