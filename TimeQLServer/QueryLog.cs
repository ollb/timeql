﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeQLServer
{
	/// <summary>
	/// Logs output / errors etc. for one query.
	/// </summary>
	internal class QueryLog
	{
		private readonly StringBuilder sb = new();

		/// <summary>
		/// Writes an empty line.
		/// </summary>
		public void WriteLine()
		{
			sb.AppendLine();
		}

		/// <summary>
		/// Writes a log message.
		/// </summary>
		public void WriteLine(string line)
		{
			sb.AppendLine(line);
		}

		/// <summary>
		/// Writes a log message.
		/// </summary>
		public void WriteLine(string format, params object?[] args)
		{
			sb.AppendFormat(format, args);
			sb.AppendLine();
		}

		/// <summary>
		/// Outputs the complete log to the console.
		/// </summary>
		public void WriteToConsole()
		{
			Console.Write(
				"-------------------" +
				Environment.NewLine +
				sb.ToString());
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		public override string ToString()
		{
			return sb.ToString();
		}
	}
}
