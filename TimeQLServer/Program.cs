﻿using Lispery;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Globalization;
using System.IO.Compression;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks.Dataflow;
using TimeQL;
using TimeQL.Libraries;
using TimeQL.Util;
using TimeQL.Values;
using TimeQLServer;

CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;
CultureInfo.CurrentUICulture = CultureInfo.InvariantCulture;

Config config = new();
QueryCache queryCache = new(config);

var assemblyName = Assembly.GetCallingAssembly().GetName();
Console.WriteLine($"{assemblyName.Name} {assemblyName.Version}");
Console.WriteLine($"Config: {config.Filename ?? "none"}.");
Console.WriteLine();

void ProcessRequest(HttpListenerContext context)
{
	try
	{
		Query query = new(config, queryCache);
		QueryLog log = new();
		HttpListenerRequest request = context.Request;
		HttpListenerResponse response = context.Response;

		response.ContentType = "application/json";
		response.ContentEncoding = Encoding.UTF8;

		// Check the request URL and type.
		if (request.Url?.AbsolutePath != "/query" || request.HttpMethod != "POST")
		{
			query.Log.WriteLine("Invalid request to {0} received.", request.Url);
			query.Log.WriteToConsole();

			response.StatusCode = 400;
			response.AddHeader("X-Platform-Error-Code", "invalid");

			using (Utf8JsonWriter writer = new(response.OutputStream))
			{
				writer.WriteStartObject();
				writer.WriteString("error", "Invalid request");
				writer.WriteEndObject();
			}

			response.Close();
			return;
		}

		using JsonDocument document = JsonDocument.Parse(request.InputStream);
		JsonElement root = document.RootElement;
		int version = root.GetInt32("version", -1);

		// Check if we support the version.
		if (version != 1)
		{
			query.Log.WriteLine("Invalid request with version {0} received.", version);
			query.Log.WriteToConsole();

			response.StatusCode = 400;
			response.AddHeader("X-Platform-Error-Code", "invalid");

			using (Utf8JsonWriter writer = new(response.OutputStream))
			{
				writer.WriteStartObject();
				writer.WriteString("error", "Unsupported version");
				writer.WriteEndObject();
			}

			response.Close();
			return;
		}

		try
		{
			string queryText = root.GetString("query-text");
			log.WriteLine(queryText);
			log.WriteLine();

			if (root.HasProperty("resolution"))
			{
				query.Resolution = TimeSpan.FromTicks(root.GetInt64("resolution") * 10000);
			}
			else
			{
				// TODO: remove at some point.
				query.Resolution = TimeSpan.FromTicks(root.GetInt64("sample-interval") * 10000);
			}

			query.WindowStart = DateTimeOffset.FromUnixTimeMilliseconds(root.GetInt64("window-start")).UtcDateTime;
			query.WindowStop = DateTimeOffset.FromUnixTimeMilliseconds(root.GetInt64("window-stop")).UtcDateTime;

			object? result = query.Run(queryText);
			TimeSeriesList list = TimeSeriesConverter.Convert(result, query.WindowStop);

			var algo = Enum.Parse<CompressionAlgorithm>(config.ResponseCompression, true);

			if (algo != CompressionAlgorithm.None)
			{
				response.AddHeader("Content-Encoding", algo.ToString().ToLowerInvariant());
			}

			using CompressionStream compressionStream = new(
				response.OutputStream,
				algo,
				config.ResponseCompressionLevel);

			using Utf8JsonWriter writer = new(compressionStream);
			writer.WriteStartObject();
			writer.WriteStartArray("series");

			foreach (var series in list)
			{
				series.SerializeJson(writer);
			}

			writer.WriteEndArray();
			writer.WriteEndObject();
		}
		catch (Exception ex)
		{
			query.Log.WriteLine(ex.Message);
			query.Log.WriteLine();

			response.StatusCode = 400;
			response.AddHeader("X-Platform-Error-Code", "invalid");

			using Utf8JsonWriter writer = new(response.OutputStream);
			writer.WriteStartObject();
			writer.WriteString("code", "invalid");
			writer.WriteString("message", ex.Message);
			writer.WriteEndObject();
		}

		query.PrintReport();
		response.Close();
	}
	catch (Exception ex)
	{
		Console.WriteLine(ex.Message);
	}
}

HttpListener listener = new();
listener.Prefixes.Add($"http://{config.Host}:{config.Port}/");
listener.Start();

ExecutionDataflowBlockOptions options = new();
options.MaxDegreeOfParallelism = config.WorkerLimit;
options.BoundedCapacity = config.PendingRequestLimit;
ActionBlock<HttpListenerContext> block = new(ProcessRequest, options);

while (true)
{
	HttpListenerContext context = listener.GetContext();
	block.SendAsync(context).Wait();
}
