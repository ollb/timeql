﻿using Lispery;
using Lispery.Interfaces;
using Lispery.Util;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL.Values;

namespace TimeQLServer
{
	using KVPair = KeyValuePair<object, object?>;

	internal static class TimeSeriesConverter
	{
		public static TimeSeriesList Convert(object? result, DateTime stopTime)
		{
			if (result == null)
			{
				return new TimeSeriesList();
			}

			return result switch
			{
				TimeSeriesList list => list,
				TimeSeries series   => new TimeSeriesList(series),
				NamedValue value    => Convert(value, stopTime),
				ISequence seq       => Convert(seq, stopTime),
				KVPair kv           => Convert(kv, stopTime),
				long value          => Convert(new NamedValue("result", value), stopTime),
				double value        => Convert(new NamedValue("result", value), stopTime),
				_                   => throw GetConvertException(result)
			};
		}

		private static Exception GetConvertException(object result)
		{
			return new InvalidOperationException(
				$"Type '{result.GetType().Name}' can not be converted to TimeSeriesList");
		}

		private static TimeSeriesList Convert(NamedValue value, DateTime stopTime)
		{
			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			builder.Add(stopTime, value.Value);
			return new TimeSeriesList(new TimeSeries(value.Name, builder.ToImmutable(), value.Metadata));
		}

		private static TimeSeriesList Convert(KVPair pair, DateTime stopTime)
		{
			string name = pair.Key?.ToString() ?? "nil";
			double value = Conversion.To<double>(pair.Value);

			var builder = ImmutableSortedDictionary.CreateBuilder<DateTime, double>();
			builder.Add(stopTime, value);
			return new TimeSeriesList(new TimeSeries(name, builder.ToImmutable()));
		}

		private static TimeSeriesList Convert(ISequence seq, DateTime stopTime)
		{
			var builder = ImmutableList.CreateBuilder<TimeSeries>();

			foreach (var value in seq.GetEntries())
			{
				builder.AddRange(Convert(value, stopTime));
			}

			return new TimeSeriesList(builder.ToImmutable());
		}
	}
}
