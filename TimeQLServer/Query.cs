﻿using Lispery;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeQL;
using TimeQL.Influx;
using TimeQL.Libraries;
using TimeQL.Util;

namespace TimeQLServer
{
	internal class Query
	{
		private readonly Config config;
		private readonly QueryCache queryCache;

		/// <summary>The log used for the query.</summary>
		public QueryLog Log { get; init; } = new();

		/// <summary>Gets or sets the sample interval for the query.</summary>
		public TimeSpan Resolution { get; set; }

		/// <summary>Gets or sets the start time for the query window.</summary>
		public DateTime WindowStart { get; set; }

		/// <summary>Gets or sets the stop time for the query window.</summary>
		public DateTime WindowStop { get; set; }

		/// <summary>The time spent setting up the environment.</summary>
		public TimeSpan SetupTime { get; private set; }

		/// <summary>The time spent parsing the query.</summary>
		public TimeSpan ParseTime { get; private set; }

		/// <summary>The time spent checking the cache on fetch requests.</summary>
		public TimeSpan FetchCacheTime { get; private set; }

		/// <summary>The time spent executing fetch requests.</summary>
		public TimeSpan FetchExecuteTime { get; private set; }

		/// <summary>The time spent parsing the results of fetch requests.</summary>
		public TimeSpan FetchParseTime { get; private set; }

		/// <summary>The time spent executing code (excluding fetching data).</summary>
		public TimeSpan ExecuteTime { get; private set; }

		/// <summary>The number of evaluation steps.</summary>
		public long EvaluationSteps { get; private set; }

		/// <summary>
		/// Creates a new query.
		/// </summary>
		public Query(Config config, QueryCache queryCache)
		{
			this.config = config;
			this.queryCache	= queryCache;
		}

		/// <summary>
		/// Runs the given query.
		/// </summary>
		public object? Run(string queryText)
		{
			if (Resolution == TimeSpan.Zero)
			{
				throw new InvalidOperationException("Sample interval not set.");
			}

			Log.WriteLine(queryText);
			Log.WriteLine();

			Stopwatch sw = new();
			sw.Start();

			// Setup the interpreter and environment.
			Interpreter interpreter = new();
			interpreter.EvaluationStepLimit = config.EvaluationStepLimit;
			var env = interpreter.Environment;
			interpreter.Print += Interpreter_Print;

			TimeQLLib lib = new(config, queryCache, env);
			env.Define("*window-start*", WindowStart);
			env.Define("*window-stop*", WindowStop);
			env.Define("*window-size*", WindowStop - WindowStart);
			env.Define("*resolution*", Resolution);
			env.Define("*max-resolution*", null);
			lib.InfluxLib.Fetched += InfluxLib_Fetched;
			SetupTime = sw.GetElapsedAndRestart();

			foreach (string filename in config.Includes)
			{
				string code = File.ReadAllText(filename);
				interpreter.Evaluate(code);
			}

			// Parse the query.
			var program = interpreter.Parse(queryText);
			ParseTime = sw.GetElapsedAndRestart();

			// Run the query.
			object? result = interpreter.Evaluate(program);

			// Update statistics.
			EvaluationSteps = interpreter.EvaluationSteps;

			ExecuteTime = interpreter.EvaluationTime -
				FetchExecuteTime -
				FetchParseTime;

			return result;
		}

		/// <summary>
		/// Prints the query log and statistics.
		/// </summary>
		public void PrintReport()
		{
			Log.WriteLine("Setup:        {0} ms", (int)SetupTime.TotalMilliseconds);
			Log.WriteLine("Parse:        {0} ms", (int)ParseTime.TotalMilliseconds);
			Log.WriteLine("FetchCache:   {0} ms", (int)FetchCacheTime.TotalMilliseconds);
			Log.WriteLine("FetchExecute: {0} ms", (int)FetchExecuteTime.TotalMilliseconds);
			Log.WriteLine("FetchParse:   {0} ms", (int)FetchParseTime.TotalMilliseconds);
			Log.WriteLine("Execute:      {0} ms", (int)ExecuteTime.TotalMilliseconds);
			Log.WriteLine("Steps:        {0}", EvaluationSteps);
			Log.WriteToConsole();
		}

		/// <summary>
		/// Handles print events.
		/// </summary>
		private void Interpreter_Print(object? sender, PrintEventArgs e)
		{
			Log.WriteLine(e.Output);
		}

		/// <summary>
		/// Handles influx fetched events.
		/// </summary>
		private void InfluxLib_Fetched(object? sender, InfluxFetchedEventArgs e)
		{
			FetchCacheTime += e.Result.CacheTime;
			FetchExecuteTime += e.Result.ExecuteTime;
			FetchParseTime += e.Result.ParseTime;
		}
	}
}
