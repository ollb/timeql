# TimeQL

TimeQL is a query language for time-series, based on [Lispery](https://gitlab.com/ollb/lispery).
It currently supports the following features:

- Extensible LISP-based programming language.
- Easy arithmetics and combination of time series.
- Support for grouping / joining / filtering time series by metadata.
- Support for the following data sources:
  - InfluxDB
  - Graphite
  - Prometheus
  - Cloudwatch
- [Grafana Plugin](https://gitlab.com/ollb/timeql-grafana-plugin)
- Built-in caching mechanism

## Documentation

The documentation can be found at https://ollb.gitlab.io/timeql/. It also contains instructions
on how to install and run TimeQL.

## Query Examples

Assuming a Graphite database with the following series names:

- `[datacenter].[server].[service].[metric]`
- `dc-lon-01.node-01.cpu.usage`
- `dc-fra-02.node-01.disk.sda.free`
- `dc-fra-02.node-01.net.bandwidth`
- `dc-fra-02.node-01.http.connections`

### Bandwidth usage of servers in `dc-fra-02`

```clojure
(graphite-query "dc-fra-02.*.net.bandwidth" :schema "dc.server" :name :server)
```

### Average bandwidth usage of servers in `dc-fra-02` over 6 hours

```clojure
(def data (graphite-query "dc-fra-02.*.net.bandwidth"
                          :schema "dc.server" :name :server :prefetch "6h"))
(slide-map "6h" average data)
```

### Maximum bandwidth usage of servers in `dc-fra-02` in 4 hour windows

```clojure
(def data (graphite-query "dc-fra-02.*.net.bandwidth"
                          :schema "dc.server" :name :server))
(window-map "4h" maximum data)
```

### Average bandwidth usage per datacenter

```clojure
(def data (graphite-query "*.*.net.bandwidth" :schema "dc"))
(group-join :dc average 0 data)
```

### Total free space for each server (across all disks)

```clojure
(def data (graphite-query "*.*.disk.*.free" :schema "dc.server"))
(group-join [:dc "-" :server] sum 0 data)
```

### Average bandwidth per HTTP connection for each server in `dc-fra-02`

```clojure
(def bandwidth (graphite-query "dc-fra-02.*.net.bandwidth" :schema "dc.server" :name :server))
(def connections (graphite-query "dc-fra-02.*.http.connections" :schema "dc.server" :name :server))
(/ bandwidth connections)
```